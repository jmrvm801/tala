var marssoft = '';
var parts = window.location.search.substr(1).split("&");
var $_GET = {};
for (var i = 0; i < parts.length; i++) {
  var temp = parts[i].split("=");
  $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
}
$(document).ready(() => {
  moment.locale('es');
  marssoft = new Marssoft();
  if ($_GET.admin != undefined) {
    marssoft.logged();
  } else {
    //Sandbox
    // OpenPay.setId('mcgd7w1k2znwkmczfjqq');
    // OpenPay.setApiKey('pk_2bb46c903b9f46a1b5fefe90ffa11042');
    //Production
    OpenPay.setId('mlqyplwkyovxwiitj7y2');
    OpenPay.setApiKey('pk_51af35d77f1846728871da7c4bce9d3c');
    OpenPay.setSandboxMode(false);
    marssoft.in.public.run();
  }
});

class Marssoft {
  constructor() {
    this.data = {
      title: 'Talabartería la Guadalupana'
    };
    this.ui = new ui();
    this.asyn = new Ax('services/');
    this.fecha = new Fecha();
    this.out = {
      login: new Login()
    };
    this.in = {
      principal: new Mainframe(),
      public: new Public()
    }
    this.sidenavLoaded = false;
    this.clickEvents();
  }
  logged() {
    marssoft.asyn.post({ class: 'login', method: 'logged' }, (r) => {
      if (!r.response)
        this.out.login.run();
      else
        this.in.principal.run();
    });
  }
  clickEvents() {
    u.keyup('input, textarea', this.removeValidated);
    u.blur('input, textarea', this.setValidations);
    u.keyup('.username, .password', this.checkEnter);
  }
  checkEnter(v, e) {
    if (e.keyCode == 13)
      $('.login_action').click();
  }

  removeValidated(v) {
    /** Start of length validation */
    let length = v.attr('data-length');
    if (length != undefined) {
      length = length.toSimpleNumber();
      if (v.val().length > length) {
        let text = v.val().substring(0, length);
        v.val(text);
      }
    }
    /** end of length validation */
    v.removeClass('required');
  }
  setValidations(v, e) {
    switch (e.keyCode) {
      case 37:
      case 38:
      case 39:
      case 40:
      case 8:

        break;
      default:
        marssoft.removeValidated(v);
        let val = v.val();
        if (v.hasClass('currency_format')) {
          val = val.toSimpleNumber();
          if (isNaN(val))
            val = '$0.00';
          v.val(val.formatMoney());
        }
        break;
    }
  }
}

class Mainframe {
  constructor() {
    this.clickEvents();
    this.activeClass = () => { };
    this.subs = {
      user: new User(),
      client: new Client(),
      supplier: new Supplier(),
      family: new Family(),
      subfamily: new Subfamily(),
      product: new Product(),
      order: new Order(),
      slider: new Slider(),
    };
    this.DOMAction = '';
    this.DOMAction2 = '';
    this.DOMAction3 = '';
  }
  clickEvents() {
    u.click('.action_menu', this.action_menu);
    u.click('.refresh_module', this.refresh_module);
    u.click('.project_selectable', this.project_selectable);
    u.click('.open_popup', this.open_popup);
    u.click('html', this.targeret);
    u.click('.choose', this.choose);
  }
  choose(v) {
    let cls = v.attr('class').split(' ');
    cls = cls[cls.length - 1]
    switch (cls) {
      case 'close_session':
        marssoft.in.principal.exit_session();
        break;
    }
    $('.window-popup').fadeOut(300);
  }
  targeret(v, e) {
    if (!$(e.target).hasClass('not_hide')) {
      $('.window-popup').fadeOut(300);
    }
  }
  open_popup() {
    $('.window-popup').fadeIn(300);
  }
  project_selectable(v) {
    let target = v.attr('data-target');
    let name = v.attr('data-name');
    $('.action_menu').removeClass('scale-in');
    setTimeout(() => {
      switch (target) {
        case '0':
          $('.changable').html(marssoft.in.principal.buildSidebar());
          $.cookie('workspace', 0);
          $('.action_menu[data-target="dashboard"]').click();
          break;
        default:
          $('.changable').html(marssoft.in.principal.buildSidebar('5'));
          $.cookie('workspace', target);
          $('.action_menu[data-target="sdashboard"]').click();
          break;
      }
      $('li.action_menu').addClass('scale-in');
    }, 201);
  }
  exit_session() {
    marssoft.asyn.post({ class: 'login', method: 'logout' }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast("Cerrando sesion...", "green");
          setTimeout(() => {
            location.reload();
          }, 1000);
          break;
        default:
          Core.toast("Error inesperado, intente más tarde.", "red");
          break;
      }
    });
  }
  animate(fn) {
    $('.content').removeClass('scale-in');
    setTimeout(() => {
      $('.content').addClass('scale-in');
      fn();
    }, 201);
  }
  refresh_module() {
    marssoft.in.principal.animate(() => {
      marssoft.in.principal.activeClass();
    });
  }
  buildBreadcrum(options = []) {
    let m = $('.titlesHeader');
    let type = "breadcrumb white-text";
    if (options.length == 0) {
      m.html(`<a href="#!" class="${type}">Dashboard</a>`);
    } else {
      m.html(``);
      let i = 0;
      let suma = 0;
      $.each(options, (i, e) => {
        suma += (e[1].length * 10.5);
        if (i > 0)
          suma += 30;
        let cls = z.clsid();
        m.append(`<a href="#!" class="${cls} ${type}">${e[1]}</a>`);
        u.click('.' + cls, () => {
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.activeClass = e[0];
            e[0]();
          });
        });
        i++;
      });
      m.css('width', suma);
      m.parent().animate({ scrollLeft: suma }, 800);
    }
  }
  action_menu(v, e) {
    if (marssoft.sidenavLoaded)
      if ($(window).width() < 850)
        $('.sidenav').sidenav('close');
    let target = v.attr('data-target');
    if (target == 'null') {
      let guid = v.attr('data-assoc');
      let visible = $('.submenu[data-assoc="' + guid + '"]').is(':visible');
      $('.submenu').slideUp(300);
      if (!visible)
        $('.submenu[data-assoc="' + guid + '"]').slideDown(300);
      return false;
    }
    $('.action_menu').removeClass('active');
    v.addClass('active');
    let openboard = () => { };
    switch (target) {
      case 'dashboard':
        openboard = marssoft.in.principal.get_dashboard;
        break;
      case 'users':
        openboard = marssoft.in.principal.subs.user.run;
        break;
      case 'clients':
        openboard = marssoft.in.principal.subs.user.run_2;
        break;
      case 'suppliers':
        openboard = marssoft.in.principal.subs.supplier.run;
        break;
      case 'families':
        openboard = marssoft.in.principal.subs.family.run;
        break;
      case 'subfamilies':
        openboard = marssoft.in.principal.subs.subfamily.run;
        break;
      case 'products':
        openboard = marssoft.in.principal.subs.product.run;
        break;
      case 'orders':
        openboard = marssoft.in.principal.subs.order.run;
        break;
      case 'sliders':
        openboard = marssoft.in.principal.subs.slider.run;
        break;
    }
    marssoft.in.principal.activeClass = openboard;
    marssoft.in.principal.animate(() => {
      openboard();
    });
  }
  get_dashboard() {
    $('.content').html(`
      Dashboard
    `);
  }
  getMenu(option) {
    let menu = '';
    switch (option) {
      case '1':
      case '2':
      case '3':
      case '4':
        /*
          ['suppliers', 'Proveedores', 'assignment_ind', [
            ['subfamilies', 'Sub familias 1', 'collections'],
            ['subfamilies', 'Sub familias 2', 'collections'],
            ['subfamilies', 'Sub familias 3', 'collections'],
          ]],
         */
        menu = [
          ['orders', 'Órdenes', 'assignment'],
          ['sliders', 'Sliders', 'landscape'],
          // ['clients', 'Clientes', 'group'],
          // ['suppliers', 'Proveedores', 'assignment_ind'],
          ['families', 'Familias', 'collections'],
          ['subfamilies', 'Sub familias', 'collections'],
          ['products', 'Productos', 'style'],
          ['users', 'Usuarios', 'person'],
          ['clients', 'Clientes de la tienda', 'person'],
        ];
        break;
      case '5':
        menu = [
          ['sdashboard', 'Dashboard', 'dashboard interno'],
        ];
        break;
    }
    return menu;
  }
  buildSidebar(option = $.cookie('use_profile')) {
    let actions = this.getMenu(option);
    let cont = '';
    $.each(actions, (i, e) => {
      if (e[3] == undefined) {
        cont += `
          <li class="action_menu scale-transition scale-out" data-target="${e[0]}">
            <a href="#!">
              <i class="material-icons primary_color">${e[2]}</i>
              ${e[1]}
            </a>
          </li>
        `;
      } else {
        let guid = M.guid();
        let base = '';
        $.each(e[3], (j, k) => {
          base += `
            <li class="action_menu scale-transition scale-out" data-target="${k[0]}">
              <a href="#!">
                <i class="material-icons primary_color">${k[2]}</i>
                ${k[1]}
              </a>
            </li>
          `;
        });
        cont += `
          <li class="action_menu scale-transition scale-out" data-target="null" data-assoc="${guid}">
            <a href="#!">
              <i class="material-icons primary_color">${e[2]}</i>
              ${e[1]} <i class="material-icons right">arrow_drop_down</i>
            </a>
          </li>
          <li>
            <ul class="submenu" data-assoc="${guid}">
              ${base}
            </ul>
          </li>
        `;
      }
    });
    return cont;
  }
  run() {
    /*
      1. admin
      2. Directivo
      3. Supervisor general
      4. Supervisor región
      5. Administrador local
     */
    let menuspecial = '';
    let cont = this.buildSidebar();

    let actions = this.getMenu();
    switch ($.cookie('use_profile')) {
      case '1':
        menuspecial = `
          <li><a class="primary_color" href="#!">Eventos</a></li>
        `;
        break;
    }
    $('title').html(marssoft.data.title);
    $('.app').html(`
      <ul id="dropdown1" class="dropdown-content">
        ${menuspecial}
        <li><a class="primary_color" href="#!">Perfil</a></li>
        <li class="divider"></li>
        <li><a class="primary_color exit_session" href="#!">Cerrar sesión</a></li>
      </ul>
      <ul id="dropdown3" class="dropdown-content">
        ${menuspecial}
        <li><a class="primary_color" href="#!">Proyectos activos</a></li>
        <li><a class="primary_color" href="#!">Perfil</a></li>
        <li class="divider"></li>
        <li><a class="primary_color exit_session" href="#!">Cerrar sesión</a></li>
      </ul>
      <ul id="dropdown2" class="dropdown-content projects">
        <li><a class="primary_color" href="#!">Sin proyectos</a></li>
      </ul>
      <div class="header z-depth-1 primary_background white-text">
        <div class="container rsmenu">
          <div class="left-container lc1">
            <div class="icon-base hide-on-med-and-up">
              <a href="#" data-target="slide-out" class="sidenav-trigger">
                <i class="material-icons rsmenu white-text">menu</i>
              </a>
            </div>
            <div class="img-base">
              <img src="assets/img/smartplanning_logo_w.png" height="30" />
            </div>
          </div>
          <div class="left-container lc2">
            <div class="titlesHeader white-text"></div>
          </div>
          <div class="right-container not_hide">
            <div class="icon-base open_popup not_hide">
              <i class="material-icons rsmenu not_hide">apps</i>
            </div>
          </div>
          <div class="not_hide window-popup row z-depth-3 grey-text darken-2-text center-align">
            <div class="not_hide col s12">
              <h6 class="not_hide">Opciones de cuenta</h6>
            </div>
            <div class="not_hide col s6 m4 choose">
              <i class="not_hide material-icons small">settings</i><br>
              Herramientas
            </div>
            <div class="not_hide col s6 m4 choose">
              <i class="not_hide material-icons small">assignment</i><br>
              Registro
            </div>
            <div class="not_hide col s6 m4 choose close_session">
              <i class="not_hide material-icons small">lock</i><br>
              Salir
            </div>
          </div>
        </div>
      </div>
      <div class="workforce">
        <ul id="slide-out" class="spbottom sidenav sidenav-fixed z-depth-0 transparent">
          <div class="changable">
            ${cont}
          </div>
        </ul>
        <div class="workspace">
          <div class="content scale-transition scale-out row"></div>
          <div class="copyright-item center-align">Desarrollado en Grupo Marssoft, &copy; ${(new Date()).getFullYear()}</div>
        </div>
      </div>
    `);
    $('.action_menu[data-target="orders"]').click();
    $(".dropdown-trigger").dropdown({
      constrainWidth: false
    });
    $('.sidenav').sidenav();
    marssoft.sidenavLoaded = true;
    setTimeout(() => {
      $('.content').addClass('scale-in');
    }, 150);
    marssoft.in.principal.buildBreadcrum();
    $('.action_menu').addClass('scale-in');
  }
  dashboard() {
    $('.content').html(`
      Dashboard
    `);
  }
}

class Util {
  constructor() {

  }
  selectTable(selector, active, after = (selector) => { }) {
    let target = selector.parent().attr('class').split(' ');
    target = target[target.length - 1];
    if (!selector[0].hasAttribute("data-target"))
      return false;
    $('.' + target + ' > tr').removeClass('active');
    selector.addClass('active');
    let attr = selector.attr('data-target');
    $(active).attr('data-target', attr).removeClass('disabled');
    after(selector);
  }

  static rightAlign(array) {
    $.each(array, (i, e) => {
      $('td[data-attr="' + e + '"]').addClass('right-align');
      $('th[data-id="' + e + '"]').addClass('right-align');
    });
  }
  static buildselect(array, a, b) {
    let query = [];
    $.each(array, (i, e) => {
      let s_a = Table.keyValue(e, a.split('.'), 0);
      let s_b = '';
      if (Array.isArray(b)) {
        $.each(b, (j, k) => {
          s_b += Table.keyValue(e, k.split('.'), 0) + ' ';
        });
      } else {
        s_b = Table.keyValue(e, b.split('.'), 0);
      }
      query.push([s_a, s_b]);
    });
    return query;
  }

  static getFile(query) {
    // if ($(query)[0].files.length > 0){
    if ($(query + '-images > .card > .iterbium').length == 1 && $('.sec_imagen')[0].files.length > 0) {
      return [{
        document: $(query + '-images > .card > .iterbium').attr('data'),
        doc_type: $(query)[0].files[0].name.split('.').pop()
      }];
    } else {
      let items = [];
      $(query + '-images > .card > .iterbium').each(function () {
        let data = $(this).attr('data');
        let title = $(this).children('.card-title').html().split('.');
        items.push({
          document: data,
          doc_type: title[title.length - 1]
        });
      });
      return items;
    }
    // } else if ($(query+'-images > .card > .iterbium').length > 0){
    //   let source = $(query+'-images > .card > .iterbium').attr('src').split('.');
    //   return {
    //     document : $(query+'-images > .card > .iterbium').attr('data'),
    //     doc_type : source[source.length - 1]
    //   }
    // } else {
    //   return {};
    // }
  }
  static fillImage(target, url, sid = '') {
    let id = M.guid();
    $(target + '-images').append(`
      <div class="card">
        <div class="iterbium card-content" data="${sid}" src="${url}">
          <div class="img">
            <img class="iterbium" src="${url}" width="100%"/>
          </div>
          <div class="card-title">Imagen</div>
        </div>
      </div>
    `);
    /* 
    <div class="card">
        <img id="s-${id}" class="iterbium" data="" src="${url}" width="100%" />
      </div>
    */
    Util.getBase64Image(url, (base64) => {
      $('#s-' + id).attr('data', base64);
    });
  }

  static getBase64Image(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      var reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      }
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }
}

/* Business Classes */

class Slider extends Util {
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents() {
    u.click('.new_slider', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_slider();
    });
    u.click('.edit_slider', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_slider();
    });
    u.click('.slider_create', (v) => {
      this.slider_create(true);
    });
    u.click('.slider_edit', (v) => {
      this.slider_create(false);
    });
    u.click('.sliders_table > tr', (v) => {
      this.selectTable(v, '.delete_slider, .edit_slider');
    });
    u.click('.delete_slider', this.delete_slider);
    u.keyup('.sli_title, .sli_descr', () => {
      clearTimeout(marssoft.in.principal.subs.slider.timeout);
      marssoft.in.principal.subs.slider.timeout = setTimeout(marssoft.in.principal.subs.slider.create_slider_preview, 800);
    });
    u.change('.sli_alignment', this.create_slider_preview);
    u.change('.sli_goto', this.sli_goto);
  }

  delete_slider(v) {
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción', 'Está seguro de eliminar el slider <b>' + $('tr.active > td:nth-child(1)').html() + '</b>? Esta eliminación será permanente e irreversible', 'Eliminar', () => {
      marssoft.asyn.post({ class: 'sliders', method: 'delete', id: id }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("Slider eliminado con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => { });
  }

  slider_create(v) {
    let vars = ['sli_sub_id', 'sli_pro_id', 'sli_alignment', 'sli_title', 'sli_descr', 'sli_start', 'sli_end', 'sli_goto'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, ['sli_alignment', 'sli_start', 'sli_end', 'sli_goto'], true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    if (v) {
      data.images = Util.getFile('.sec_imagen');
      if (Object.keys(data.images).length == 0) {
        Core.toast("Es necesario agregar al menos 1 fotografía para el slider.", "red");
        return false;
      }
    } else {
      if ($('.sec_imagen').val() != '') {
        data.images = Util.getFile('.sec_imagen');
        if (Object.keys(data.images).length == 0) {
          Core.toast("Es necesario agregar al menos 1 fotografía para el slider.", "red");
          return false;
        }
      }
    }

    marssoft.asyn.post({ class: 'sliders', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast("Slider creado con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.slider.run;
            marssoft.in.principal.activeClass();
          });
          break;
        case 'minor':
          Core.toast("La fecha de fin es menor que la fecha de inicio", "red");
          break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
          break;
      }
    });
  }

  sli_goto(v) {
    let id = v.val();
    let clase = '', method = '';
    if (id == '1') {
      clase = 'subfamilies';
      method = 'getSubfamiliesByFamId';
    } else {
      clase = 'products';
      method = 'get';
    }
    let items = [];
    marssoft.asyn.post({ class: clase, method: method, id: '1' }, (r) => {
      if (id == '1') {
        $.each(r, (i, e) => {
          items.push([e.sub_id, e.sub_name]);
        });
        $('.div_goto_response').html(Form.select('sli_sub_id', 'Categoría', items, '', '', 'searchable="true"'));
        $('.sli_sub_id').formSelect();
      } else {
        $.each(r, (i, e) => {
          items.push([e.pro_id, e.pro_code + ' - ' + e.pro_name]);
        });
        $('.div_goto_response').html(Form.select('sli_pro_id', 'Producto', items, '', 'multiple', 'searchable="true"'));
        $('.sli_pro_id').formSelect();
      }
    });
  }

  create_slider_preview() {
    let title = $('.sli_title').val();
    let descr = $('.sli_descr').val();

    let align = $('select.sli_alignment').val();
    switch (align) {
      case '2':
        align = 'left-align';
        break;
      case '3':
        align = 'right-align';
        break;
      default:
        align = 'center-align';
        break;
    }
    let url = ($('img.iterbium').length > 0) ? $('img.iterbium').attr('src') : 'assets/img/a.jpg';
    $('.slider_preview').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="${url}">
            <div class="caption ${align}">
              <h3>${title}</h3>
              <h5 class="light grey-text text-lighten-3">${descr}</h5>
            </div>
          </li>
        </ul>
      </div>
    `);
    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  management_slider() {
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_slider'));
    let action = ['', '', ''];
    let slider = '';
    if (is_new) {
      action[0] = 'Guardar cambios';
      action[1] = 'slider_create';
      action[2] = 'Nuevo slider';
    } else {
      slider = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Guardar cambios';
      action[1] = 'slider_edit';
      action[2] = 'Editar slider';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.slider.management_slider;
    marssoft.in.principal.animate(() => {
      let families = []
      marssoft.asyn.post({ class: 'subfamilies', method: 'getSubfamiliesByFamId', id: '1' }, (r) => {
        $.each(r, (i, e) => {
          families.push([e.sub_id, e.sub_name]);
        });
        marssoft.in.principal.buildBreadcrum([
          [marssoft.in.principal.subs.slider.run, 'Sliders'],
          [marssoft.in.principal.subs.slider.management_slider, action[2]],
        ]);
        $('.content').html(`
          <div class="col s12">
            <div class="card-panel">
              <div class="row">
                <div class="col s12">
                  <h6>Datos del slider</h6>
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.input('sli_title', 'text', 'Nombre del slider (opcional)', '', '', '', 100)}
                </div>
                <div class="col s12 m8 input-field">
                  ${Form.input('sli_descr', 'text', 'Descripción del slider (opcional)', '', '', '', 100)}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.select('sli_alignment', 'Alienación del slider', [[1, 'Centrado'], [2, 'Izquierda'], [3, 'Derecha']], '', '', '')}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.select('sli_goto', 'Tipo de slider', [[1, 'Categoría'], [2, 'Producto']], '', '', '')}
                </div>
                <div class="col s12 m4 input-field div_goto_response"></div>
                <div class="col s12">
                  <h6>Duración del slider</h6>
                </div>
                <div class="col s12 m6 input-field">
                  ${Form.input('sli_start', 'text', 'Fecha de inicio', '', '', '', 100)}
                </div>
                <div class="col s12 m6 input-field">
                  ${Form.input('sli_end', 'text', 'Fecha de fin', '', '', '', 100)}
                </div>

                <div class="col s12">
                  <h6>Vista previa del slider</h6>
                </div>
                <div class="col s12 slider_preview">
                  <h6 class="center-align">No hay vista previa disponible<h6>
                </div>

                <div class="col s12">
                  <h6>Imagen del slider</h6>
                </div>
                <div class="col s12 input-field">
                  ${Form.file('sec_imagen', 'Cargar imágenes del producto', 'Se admiten archivos: jpg y png', false)}
                </div>
              </div>
            </div>
          </div>
          <div class="col s12 offset-m8 m4 input-field btn_action">
            ${Form.btn(false, 'waves_effect white_text primary_background', action[1] + ' btn100 ', action[0], 'save')}
          </div>
        `);
        if (!is_new) {
          marssoft.asyn.post({ class: 'sliders', method: 'single', id: slider }, (r) => {
            Form.fnimage('sec_imagen', 5242880, ['png', 'jpg'], (type, data) => {
              marssoft.in.principal.subs.slider.create_slider_preview();
              $('div.iterbium').hide(0);
            });
            marssoft.fecha.Dtpicker('.sli_start');
            marssoft.fecha.Dtpicker('.sli_end');
            $('.sli_title').val(r.sli_title);
            $('.sli_descr').val(r.sli_descr);
            $('.sli_alignment').val(r.sli_alignment);
            $('.sli_goto').val(r.sli_goto);
            $('.sli_start').val(r.sli_start);
            $('.sli_end').val(r.sli_end);
            $('.sli_alignment').formSelect();
            $('.sli_goto').formSelect();
            Util.fillImage('.sec_imagen', 'resources/sliders/' + r.sli_id + '.' + r.sli_extension, r.sli_id);
            M.updateTextFields();
            marssoft.in.principal.subs.slider.create_slider_preview();
            $('div.iterbium').hide(0);
          });
        } else {
          Form.fnimage('sec_imagen', 5242880, ['png', 'jpg'], (type, data) => {
            marssoft.in.principal.subs.slider.create_slider_preview();
            $('div.iterbium').hide(0);
          });
          marssoft.fecha.Dtpicker('.sli_start');
          marssoft.fecha.Dtpicker('.sli_end');
          $('.sli_alignment').formSelect();
          $('.sli_goto').formSelect();
        }
      });
    });
  }

  run() {
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.slider.run, 'Sliders']
    ]);
    $('.content').html(`
      <div class="col s12 offset-m6 m2">
        ${Form.btn(false, 'waves_effect white_text red', 'delete_slider btn100 disabled', 'Borrar', 'remove')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_slider btn100 disabled', 'Editar', 'edit')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'new_slider btn100 ', 'Nuevo', 'add')}
      </div>
      <div class="col s12 ">
        <div class="card">
          <div class="card-content table_place"></div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'sliders', method: 'get' }, (r) => {
      $.each(r, (i, e) => {
        r[i].sli_start = moment(r[i].sli_start).format('DD MMMM YYYY');
        r[i].sli_end = moment(r[i].sli_end).format('DD MMMM YYYY');
        r[i].destiny = (r[i].sli_goto == '1') ? r[i].subfamily.sub_name : r[i].product.pro_name;
      });
      Table.table('sliders_table', 'sli_id', '.table_place', {
        'sli_title': 'Nombre',
        'sli_start': 'Fecha de inicio',
        'sli_end': 'Fecha de fin',
        'goto': 'Tipo',
        'destiny': 'Destino',
        'status': 'Status',
      }, r);
    });
  }
}

class Public extends Util {
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents() {
    u.click('.public.brand-logo', this.home);
    u.click('.go_category', (v) => {
      let id = v.attr('data-target');
      this.open_category(id);
    });
    u.click('.go_brand', (v) => {
      let id = v.attr('data-target');
      this.open_brand(id);
    });
    u.click('.go_product', (v) => {
      let id = v.attr('data-target');
      this.open_product(id);
    });
    u.change('.pro_alpha, .pro_economic', this.filter);
    u.change('select.pro_toprice', this.pro_toprice);
    u.click('.buy_now', this.buy_now);
    u.click('.go_cart', this.go_cart);
    u.click('.create_account', () => {
      this.create_account(true);
    });
    u.click('.save_ships', this.save_ships);
    u.click('.update_client', () => {
      this.create_account(false);
    });
    u.click('.send_email', this.send_email);
    u.keyup('.code_sent', this.code_sent);
    u.click('.login_now', this.login_now);
    u.click('.delete_item', this.delete_item);
    u.click('.remove_item', this.remove_item);
    u.click('.add_item', this.add_item);
    u.change('.ord_financing', this.pay_differed);
    u.click('.confirm_pedido', this.confirm_pedido);
    u.click('.client_orders_table > tr', (v) => {
      let status = v.children('td:nth-child(7)').html();
      status = (status == 'Listo para pagar' || status == 'Listo con detalles') ? ', .pay_order' : '';
      this.selectTable(v, '.client_order_details ' + status);
    });
    u.click('.client_order_details', this.client_order_details);
    u.click('.go_terms', this.go_terms);
    u.click('.go_privacy', this.go_privacy);
    u.click('.go_shipping', this.go_shipping);
    u.click('.go_call', this.go_call);
    u.click('.close_client_session', this.close_client_session);
    u.click('.pay_order', this.pay_order);

    //Openpay buttons
    u.keyup('.holder_name, .card_number, .expiration_month, .expiration_year, .cvv2', this.removeRequiredField);

		u.keyup('.card_number', this.card_number);
		u.keyup('.expiration_month', this.expiration_month);
		u.keyup('.expiration_year', this.expiration_year);
    u.keyup('.cvv2', this.cvv2);
    u.pClick('#pay-button', this.pay_button);
    //End of openpay buttons
    u.click('.action_print_area', this.action_print_area);
    u.click('.go_print_payment', this.go_print_payment);
    u.click('.go_sent', this.go_sent);
    u.click('.return_to_order', this.return_to_order);
    u.click('.send_message', this.send_message);
    u.keyup('.pro_search', this.pro_search);
    u.keyup('.password.client', this.accessEnter);
    u.click('.before_image', this.changeImage);
    u.click('.after_image', this.changeImage);
  }

  accessEnter(v, e){
    if (e.keyCode == 13){
      $('.login_now').click();
    }
  }

  pay_button(){
    let flag = true;
		let fields = ['holder_name', 'card_number', 'expiration_month', 'expiration_year', 'cvv2'];
		$.each(fields, (i, e) => {
			let value = $('.'+e).val();
			if (value == ''){
				flag = false;
				$('.'+e).addClass('required');
			}
    });
    if (flag){
      $("#pay-button").prop( "disabled", true);
      OpenPay.token.extractFormAndCreate('payment-form', (response) => {
				var token_id = response.data.id;
				$('#token_id').val(token_id);
				let data = {
					name : $('.holder_name').val(),
					source_id : token_id,
					pay_id: $('#ord_id').val(),
					device_session_id : $('#deviceIdHiddenFieldName').val()
        }
				marssoft.asyn.post({class: 'login', method: 'onlinePayment', data: data}, r => {
					$("#pay-button").prop("disabled", false);
					switch(r.response){
						case 'success':
							if (r.status == 'charge_pending'){
								location.href = r.redirect.payment_method.url;
							} else {
								$('.openpay_form').html(`
									<div class="row center-align">
										<div class="card-panel row">
											<div class="col s12">
												<i class="material-icons large green-text">check_circle</i>
											</div>
											<div class="col s12">
												<h4>Pago aplicado con éxito</h4>
											</div>
											<div class="col s12 m6 offset-m3">
												<div class="col s12 m5">Autorización</div>
												<div class="col s12 m7 bolder">${r.authorization}</div>
												<div class="col s12 m5">Tipo</div>
												<div class="col s12 m7 bolder">${r.credit_type} ${r.brand} ${r.bank}</div>
												<div class="col s12 m5">Monto cobrado</div>
												<div class="col s12 m7 bolder">$ ${r.paid.toFixed(2)}</div>
											</div>
											<div class="col s12">
												<h6>En un lapso de 24 horas se verá reflejada el pago de su colegiatura</h6>
											</div>
											<div class="col s12">
												${Form.btn(false, 'green', 'admin_goto btn100', 'Regresar al inicio', 'check', 'data-target="onlinePayment"')}
											</div>
										</div>
									</div>
								`);
							}
						break;
						case 'failed':
							$('.openpay_form').html(`
								<div class="row center-align">
									<div class="card-panel row">
										<div class="col s12">
											<i class="material-icons large red-text">remove_circle</i>
										</div>
										<div class="col s12">
											<h4>El cargo no pudo ser aplicado: ${r.charge_data.amount}</h4>
										</div>
										<div class="col s12">
											<h5 class="response_code"></h5>
										</div>
										<div class="col s12">
											<h6>Si el cargo se realizó a su tarjeta pero el sistema no lo aplicó a su colegiatura, favor de enviar un correo a fernandoc@grupomarssoft.com con asunto: <b>pago declinado falso positivo</b> y en <b>contenido</b> el siguiente código: ${token_id}</h6>
										</div>
									</div>
								</div>
							`);
							switch(r.error_code){
								case 1003:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3001:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3002:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3003:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3004:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3005:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3006:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3007:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3008:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3009:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3010:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3011:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 3012:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								case 5001:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
								default:
									$('.response_code').html('Se presentó un error con la transacción de su pago.');
								break;
							}
						break;
					}
				});
			}, (response) => {
				var desc = response.data.description != undefined ? response.data.description : response.message;
					alert("ERROR [" + response.status + "] " + desc);
					$("#pay-button").prop("disabled", false);
			});
    }
  }

  card_number(v){
		let cont = true;
		for(let i = 0; i < $('.card_number').val().length; i++){
			let test = parseInt($('.card_number').val()[i]);
			if (isNaN(test))
				cont = false;
		}
		if (!cont){
			v.val('');
			return false;
		}
		$('.card_type').val('')
		let val = v.val().length;
		if (val > 14){
			let result = OpenPay.card.validateCardNumber(v.val());
			if (!result){
				Core.toast('Tarjeta no válida, intente nuevamente', 'red');
				if (val == 16){
					$('.card_number').addClass('required').val('');
				}else {
          console.log('etnra aqui perro');
					$('.card_number').addClass('required');
				}
				return false;
			} else {
				$('.card_type').html(OpenPay.card.cardType(v.val()));
				$('.card_number').addClass('success')
				$('.expiration_month').focus();
			}
		}
  }
  
  cvv2(v){
		let val = v.val()
		if (val.length > 2){
			if (!OpenPay.card.validateCVC(val,$('.card_number').val())){
				Core.toast('El código de seguridad de la tarjeta es incorrecto.', 'red');
				v.addClass('required');
			} else {
				v.addClass('success');
			}
		}
  }
  
  expiration_month(v){
		let val = v.val();
		let length = val.length;
		if (val.length > 2){
			v.val('');
			Core.toast('El mes de expiración debe tener 2 dígitos', 'red')
		} else {
			val = parseInt(val);
			if (val < 0 || val > 12){
				v.val('');
				Core.toast('Únicamente se admiten meses del 01 al 12', 'red');
			} else {
				if (length == 2){
					if (val == 0){
						v.val('');
					Core.toast('Únicamente se admiten meses del 01 al 12', 'red');
					} else {
						$('.expiration_month').addClass('success');
						$('.expiration_year').focus();
					}
				}
			}
		}
  }

  expiration_year(v){
		let val = v.val().length;
		if (val > 2){
			v.val('');
			Core.toast('El año de expiración debe tener 2 dígitos (2020 es 20)', 'red')
		} else if (val == 2){
			let month = $('.expiration_month');
			let year = $('.expiration_year');
			if (!OpenPay.card.validateExpiry(month.val(), year.val())){
				Core.toast('La vigencia de la tarjeta no es válida, intente nuevamente', 'red');
				month.val('');
				year.val('');
				$('.expiration_month').removeClass('success').addClass('required');
				$('.expiration_year').removeClass('success').addClass('required');
				$('.expiration_month').focus();
			} else{
				$('.expiration_year').addClass('success');
				$('.cvv2').focus();
			}
		}
  }
  
  removeRequiredField(v){
		v.removeClass('required success');
	}

  pay_order(v) {
    let id = v.attr('data-target');
    marssoft.asyn.post({ class: 'login', method: 'get_single_order', public: 'true', id: id }, (r) => {
      switch (r.response) {
        case 'true':
          marssoft.in.public.codi_platform(r);
          break;
        default:
          Core.toast('Se presentó un problema al encontrar la orden.', 'red');
          break;
      }
    });
  }

  codi_platform(r) {
    var deviceSessionId = OpenPay.deviceData.setup("payment-form", "deviceIdHiddenFieldName");

    window.scrollTo({ top: 0, behavior: 'smooth' });
    $('.main').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="assets/img/a.jpg">
            <div class="caption center-align">
              <h3>Pago de la orden de compra No. ${r.ord_id}</h3>
              <h5 class="light grey-text text-lighten-3">Su transacción será procesada por OpenPay</h5>
            </div>
          </li>
        </ul>
      </div>
      <div class="container">
        <div class="row openpay_form">
          <div class="col s12">
            <div class="card-panel row">
              <div class="col s12 center-align">
                <form action="#" method="POST" id="payment-form">
                  <input type="hidden" name="token_id" id="token_id">
                  <input type="hidden" name="pay_id" id="ord_id" value="${r.ord_id}">
                  <input type="hidden" name="deviceIdHiddenFieldName" id="deviceIdHiddenFieldName" value="${deviceSessionId}">
                  <input type="hidden" name="use_card_points" id="use_card_points" value="false">
                  <h5>Realizar pago con tarjeta</h5>
									<div class="col s12">
										<h6>Monto a cobrar: <span class="monto">${r.ord_total.formatMoney()} a ${r.ord_financing} MSI</span></h6>
									</div>
									<div class="col s12 m5">
										<h6>Tarjetas de crédito</h6>
										<img src="assets/img/cards1.png" />
									</div>
									<div class="col s12 m7">
										<h6>Tarjetas de débito</h6>
										<img src="assets/img/cards2.png" />
									</div>
									<div class="col s12 input-field">
										<input class="ginput holder_name" placeholder="Nombre del titular" type="text" autocomplete="off" data-openpay-card="holder_name" />
									</div>
									<div class="col s12 input-field">
										<span class="card_type"></span>
										<input class="ginput card_number" placeholder="Número de tarjeta" type="text" autocomplete="off" data-openpay-card="card_number" />
									</div>
									<div class="col s12">
										<h6>Fecha de expiración y CVV</h6>
									</div>
									<div class="col s12 m3 input-field">
										<input class="ginput expiration_month" placeholder="Mes" type="number" autocomplete="off" data-openpay-card="expiration_month" />
									</div>
									<div class="col s12 m3 input-field">
										<input class="ginput expiration_year" placeholder="Año" type="number" autocomplete="off" data-openpay-card="expiration_year" />
									</div>
									<div class="col s12 m2 input-field">
										<input class="ginput cvv2" placeholder="Código CVV" type="number" autocomplete="off" data-openpay-card="cvv2" />
									</div>
									<div class="col s12 m4 cvv_attach">
										<img src="assets/img/cvv.png" />
									</div>
									<div class="col s12 m6">
										Tus datos financieros están protegidos por:<br>
										<img src="assets/img/openpay.png" />
									</div>
									<div class="col s12 m6">
										Tus pagos se realizan de forma segura con encriptación de 256 bits<br>
										<img src="assets/img/security.png" />
									</div>
									<div class="col s12">
										<button class="waves-effect waves-light btn-large green btn100" type="submit" id="pay-button">Pagar</button>
									</div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  close_client_session() {
    marssoft.asyn.post({ class: 'login', method: 'logout' }, (r) => {
      switch (r.response) {
        case 'true':
          $('.go_cart').click();
          Core.toast('Cerrando sesión, gracias por estar en Talabartería la Guadalupana.', 'green');
          break;
        default:
          Core.toast('Se presentó un problema al encontrar la orden.', 'red');
          break;
      }
    });
  }

  go_call() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    $('.main').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="assets/img/a.jpg">
            <div class="caption center-align">
              <h3>¿Tiene preguntas o inquietudes?</h3>
              <h5 class="light grey-text text-lighten-3">No hay problema, ponemos nuestros datos a su disposición</h5>
            </div>
          </li>
        </ul>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12">
            <div class="card-panel row">
              <div class="col s12 center-align">
                <img src="assets/img/whats.png" width="30" />
                <a href="https://api.whatsapp.com/send?phone=5212293703438&text=Quiero%20solicitar%20informes%20sobre%20su%20tienda%20Talabartería%20la%20Guadalupana" target="_blank">
                  <h5>229 370 3438</h5>
                </a>
                <h6>¡Llámenos o envíenos un whatsapp y con gusto le atenderemos!</h6>
                <br>
                <h6>O si lo prefiere, puede enviarnos un correo electrónico a betocastro18@hotmail.com donde le daremos atención a su solicitud</h6>
                <br><br>
              </div>
              <div class="col s12 center-align">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1679.6932824737735!2d-97.04702782395744!3d19.07324350996718!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85c4de3139d4fdc9%3A0x36d51386c612ce3c!2sTalabarteria%20La%20Guadalupana!5e0!3m2!1ses-419!2smx!4v1598503244776!5m2!1ses-419!2smx" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  go_shipping() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    $('.main').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="assets/img/a.jpg">
            <div class="caption center-align">
              <h3>Políticas de envío</h3>
              <h5 class="light grey-text text-lighten-3">Cuidamos su compra hasta la puerta de su casa</h5>
            </div>
          </li>
        </ul>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12">
            <div class="card-panel row">
              <div class="col s12 info_data">
                <h5>Cómpra</h5>
                <p>Una vez realizada la compra no se aceptan cambios o modificaciones en su pedido de ningún tipo; por ejemplo, no hay cambios en tamaño, color, piel, adorno, personalización en monturas, entre otros.</p>
                <h5>Cambios y devoluciones</h5>
                <p>Dadas las características artesanales de nuestros productos no podemos ofrecer por el momento cambios ni devoluciones en las munturas. Sin embargo, únicamente procederá el cambio de piezas si el producto presentó un daño durante el envío. Además se ofrecerá el reembolso del producto si se extravió durante el viaje con alguna de las paquerías con las que trabajamos ya que el costo del envío cuenta con una poliza de seguro para mayor comodidad y tranquilidad de su parte.</p>
                <h5>Garantías</h5>
                <p>Para ejercer su derecho a la garantía, deberá comunicarse al teléfono 229 370 3438 con los siguientes datos.
                  <ol>
                    <li>Número de pedido de la tienda.</li>
                    <li>Fotografías claras del daño de la montura.</li>
                    <li>Fotografía clara de la caja en la que llegó su montura.</li>
                  </ol>
                  Posterior a la confirmación del producto dañado por parte de Talabartería la Guadalupana, se le ofrecerá la opción de enviarle una pieza nueva de reemplazo para la pieza dañada o solicitar el regreso del producto para realizar el cambio del producto directamente en las instalaciones de Talabartería la Guadalupana.
                </p>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  go_privacy() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    $('.main').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="assets/img/a.jpg">
            <div class="caption center-align">
              <h3>Políticas de privacidad</h3>
              <h5 class="light grey-text text-lighten-3">Todos sus datos están protegidos</h5>
            </div>
          </li>
        </ul>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12">
            <div class="card-panel row">
              <div class="col s12 info_data">
                <h5>¿Quiénes somos?</h5>
                <p>
                  ALBERTO CASTRO LOPEZ, mejor conocido como TALABARTERÍA LA GUADALUPANA , con domicilio en calle AV NICOLAS BRAVO NO 86, colonia CENTRO, ciudad COSCOMATEPEC, municipio o delegación COSCOMATEPEC, c.p. 94140, en la entidad de VERACRUZ, país México , y portal de internet www.talabarterialaguadalupana.com , es el responsable del uso y protección de sus datos personales, y al respecto le informamos lo siguiente:<br><br>
                  Los datos personales que recabamos de usted, los utilizaremos para las siguientes finalidades que son necesarias para el servicio que solicita:<br>
                  <ol>
                    <li>Realizar el envío de sus productos a la dirección especificada</li>
                    <li>Comunicar el status de sus envíos</li>
                    <li>Mercadotecnia o publicitaria</li>
                  </ol>
                </p>
                <h5>¿Para qué fines utilizaremos sus datos personales?</h5>
                <p>
                  De manera adicional, utilizaremos su información personal para las siguientes finalidades secundarias que no son necesarias para el servicio solicitado, pero que nos permiten y facilitan brindarle una mejor atención:<br><br>
                  <ol>
                    <li>Ofrecer promociones o descuentos</li>
                  </ol>
                  En caso de que no desee que sus datos personales sean tratados para estos fines secundarios, desde este momento usted nos puede comunicar lo anterior a través del siguiente mecanismo:<br>
                  Solicitar la eliminación de sus datos a través del correo electrónico betocastro18@hotmail.com<br>
                  La negativa para el uso de sus datos personales para estas finalidades no podrá ser un motivo para que le neguemos los servicios y productos que solicita o contrata con nosotros.
                  <h5>¿Dónde puedo consultar el aviso de privacidad integral?</h5>
                  <p>
                  Para conocer mayor información sobre los términos y condiciones en que serán tratados sus datos personales, como los terceros con quienes compartimos su información personal y la forma en que podrá ejercer sus derechos ARCO, puede consultar el aviso de privacidad integral en:<br>www.talabarterialaguadalupana.com
                  </p>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  go_terms() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    $('.main').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="assets/img/a.jpg">
            <div class="caption center-align">
              <h3>Términos y condiciones</h3>
              <h5 class="light grey-text text-lighten-3">Queremos brindarle confianza total</h5>
            </div>
          </li>
        </ul>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12">
            <div class="card-panel row">
              <div class="col s12 info_data">
              Talabartería la Guadalupana es una empresa dedicada a la fabricación de monturas y accesorios con domicilio en Av 1 Nicolás Bravo 86, Coscomatepec de Bravo, Centro, código postal 94140, Coscomatepec de Bravo, Ver. y correo electrónico betocastro18@hotmail.com que es responsable de las transacciones que se realizan para la compra de todos los productos que se ofrecen en esta página con opción de comprar en hasta 12 Mensualidades sin intereses.<br><br>
              Esta página de internet cuenta con un candado de seguridad HTTPS que protege la información de su tarjeta de crédito o débito entre Talabartería la Guadalupana y OpenPay. De igual manera, sus datos financieros NO VIAJAN A LOS SERVIDORES DE TALABARTERÍA LA GUADALUPANA ya que la tarjeta es gestionada por OpenPay. Para darle mayor seguridad, todas las transacciones están protegidas por una capa adicional llamada 3D Secure. Este método le permite a usted autenticarse con su banco a través de un mensaje de texto o la introducción de un token dinámico.<br><br>
              En caso de que tenga algún problema para realizar su pago (por ejemplo: se le hizo un cargo a su tarjeta y la página marca transacción declinada) favor de enviar un correo electrónico con una captura de pantalla del error (incluyendo la URL de la página) a betocastro18@hotmail.com con el título: Problema de cargo aplicado. En un lapso de 24 a 48 horas se realizará la validación correspondiente y se le responderá el resultado del análisis.
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  client_order_details(v) {
    let id = v.attr('data-target'); //get_single_order
    marssoft.asyn.post({ class: 'login', method: 'get_single_order', public: 'true', id: id }, (r) => {
      switch (r.response) {
        case 'true':
          marssoft.in.public.order_detail(r);
          break;
        default:
          Core.toast('Se presentó un problema al encontrar la orden.', 'red');
          break;
      }
    });
  }

  go_print_payment(v){
    let id = v.attr('data-target');
    marssoft.in.public.validate(id, false);
  }

  return_to_order(v){
    marssoft.in.public.client_order_details(v);
  }

  send_message(v){
    let send = v.attr('data-send');
    let clase = (send == '2') ? 'login' : 'messages';
    let data = {
      'mes_ord_id' : v.attr('data-target'),
      'mes_messages' : $('.men_message').val()
    }
    if (data.mes_messages == ''){
      Core.toast('Escriba un mensaje para continuar', 'red');
      return false;
    }
    marssoft.asyn.post({ class: clase, method: 'create_message', public: 'true', data: data }, (r) => {
      switch (r.response) {
        case 'true':
          $('.men_message').val('');
          M.updateTextFields();
          marssoft.in.public.addMessage(r.id, send);
          break;
        default:
          Core.toast('Su mensaje no pudo ser procesado, intente nuevamente.', 'red');
          break;
      }
    });
  }

  addMessage(data, action = '2'){
    let place = (data.mes_type == action) ? '' : 'offset-m4';
    let color = (data.mes_type == action) ? 'bluel' : 'greyl';
    $('.message_content').append(`
      <div class="col s12 m8 ${place}">
        <div class="card-panel ${color}">
          <h6>${data.mes_messages}</h6>
          ${moment(data.mes_created_at).format('DD MMMM YYYY [a las] HH:mm')}
        </div>
      </div>
    `);
    $(".message_content").scrollTop(9999);
  }

  go_sent(v){
    let ord_id = v.attr('data-target');
    marssoft.asyn.post({ class: 'login', method: 'getMessages', public: 'true', id: ord_id }, (r) => {
      $('.main').html(`
        <div class="slider categories">
          <ul class="slides">
            <li>
              <img src="assets/img/a.jpg">
              <div class="caption left-align">
                <h3>Centro de mensajes</h3>
                <h5 class="light grey-text text-lighten-3">Centro de mensajes para la orden No. ${ord_id}</h5>
              </div>
            </li>
          </ul>
        </div>
        <div class="container">
          <div class="row">
            <div class="col s12">
              <div class="card-panel row">
                <div class="col s12 m3">
                  ${Form.btn(false, 'waves_effect white_text primary_background', 'return_to_order btn100', 'Regresar', '', 'data-target="'+ord_id+'"')}
                </div>            

                <div class="col s12 message_content"></div>
                <div class="col s12 message_buttons">
                  <div class="col s12 m10 input-field">
                    ${Form.input('men_message', 'text', 'Escriba el mensaje para el venvedor', '', '', '', 100)}
                  </div>
                  <div class="col s12 m2 div_send_message">
                    ${Form.btn(false, 'waves_effect white_text primary_background', 'send_message btn100', '', 'send', 'data-target="'+ord_id+'" data-send="2"')}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      `);

      $.each(r, (i, e) => {
        marssoft.in.public.addMessage(e);
      });

      $('.slider.categories').slider({
        indicators: false,
        height: 350,
      });
    });

    
    
  }

  order_detail(r) {
    let text = '';
    //array('', 'Pendiente', 'Listo para pagar', 'Listo con detalles', 'Fuera de stock', 'Cancelado', 'Pagado, por enviar', 'Enviado', 'Entregado');
    switch (r.ord_status) {
      case '1':
        text = 'Estamos preparando su pedido';
        break;
      case '2':
        text = '¡Genial! su pedido esta listo para que efectúe el pago';
        break;
      case '3':
        text = '¡Genial! su pedido esta listo, pero revise estos detalles';
        break;
      case '4':
        text = 'Lamentamos informarle que no hay suficiente stock para efecturar su compra';
        break;
      case '5':
        text = 'Pedido cancelado por usted';
        break;
      case '6':
        text = 'Recibimos su pago, estamos preparando su paquete';
        break;
      case '7':
        text = '¡Genial! Su producto viaja con ' + r.ord_shipping_company + ' con número de rastreo ' + r.ord_shipping_code;
        break;
      case '8':
        text = '¡Genial! Su producto ha llegado con usted. Gracias por comprar.';
        break;
    }

    $('.main').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="assets/img/a.jpg">
            <div class="caption left-align">
              <h3>Detalles de su pedido No. ${r.ord_id}</h3>
              <h5 class="light grey-text text-lighten-3">${text}</h5>
            </div>
          </li>
        </ul>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12">
            <div class="card-panel row">
              <div class="col s12 m3">
                ${Form.btn(false, 'waves_effect white_text primary_background', 'go_cart btn100', 'Regresar', 'keyboard_arrow_left')}
              </div>
              <div class="col s12 m4 ${r.ord_openpay_code == '' ? 'hidden' : ''}">
                ${Form.btn(false, 'waves_effect white_text primary_background', 'go_print_payment btn100', 'Ver comprobante', 'keyboard_arrow_right', 'data-target="'+r.ord_openpay_code+'"')}
              </div>
              <div class="col s12 offset-m2 m3">
                ${Form.btn(false, 'waves_effect white_text primary_background', 'go_sent btn100', 'Mensajes', 'send', 'data-target="'+r.ord_id+'"')}
              </div>
              <div class="col s12">
                <table>
                  <tbody class="general">
                    <tr>
                      <th>Fecha del pedido</th>
                      <td class="right-align">${moment(r.ord_created_at).format('DD MMMM YYYY [a las] HH:mm')}</td>
                    </tr>
                    <tr>
                      <th>Status del pedido</th>
                      <td class="right-align">${r.status}</td>
                    </tr>
                    <tr>
                      <th>Subtotal del pedido</th>
                      <td class="right-align">${r.ord_subtotal.formatMoney()}</td>
                    </tr>
                    <tr>
                      <th>Costo del envío*</th>
                      <td class="right-align">${r.ord_shipping_amount.formatMoney()}</td>
                    </tr>
                    <tr>
                      <th>Total del envío*</th>
                      <td class="right-align">${r.ord_total.formatMoney()}</td>
                    </tr>
                    <tr>
                      <th>Plan de financimiento</th>
                      <td class="right-align">Plan a ${r.ord_financing} mes${r.ord_financing == '1' ? '' : 'es'} sin intereses</td>
                    </tr>
                    <tr class="${r.ord_financing == '1' ? 'hidden' : ''}">
                      <th>Usted pagará ${r.ord_financing} pagos mensuales de</th>
                      <td class="right-align">${r.ord_montly_payment.formatMoney()}</td>
                    </tr>
                    <tr class="${r.ord_shipping_company == '' ? 'hidden' : ''}">
                      <th>Empresa de envío</th>
                      <td class="right-align">${r.ord_shipping_company}. Enviado el ${moment(r.ord_send_at).format('DD MMMM YYYY')}</td>
                    </tr>
                    <tr class="${r.ord_shipping_code == '' ? 'hidden' : ''}">
                      <th>Número de rastreo</th>
                      <td class="right-align">${r.ord_shipping_code}</td>
                    </tr>
                  </tbody>
                  <tbody class="finantilal ${r.ord_openpay_code == '' ? 'hidden' : ''}">
                    <tr>
                      <th colspan="2" class="center-align">Datos de su pago realizado</th>
                    </tr>
                    <tr>
                      <th>Fecha de pago</th>
                      <td class="right-align">${moment(r.ord_paid_at).format('DD MMMM YYYY [a las] HH:mm')}</td>
                    </tr>
                    <tr>
                      <th>Forma de pago</th>
                      <td class="right-align">${r.openpay.ope_bank} ${r.openpay.ope_brand} ${r.openpay.ope_credit_type}</td>
                    </tr>
                    <tr>
                      <th>Autorización</th>
                      <td class="right-align">${r.ord_openpay_authorization} / ${r.ord_openpay_code}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col s12 center-align">
                <h6>Lista de productos solicitados</h6>
              </div>
              <div class="col s12 table_place"></div>
            </div>
          </div>
        </div>
      </div>
    `);
    let products = [];
    $.each(r.items, (i, e) => {
      products.push({
        'pro_id': e.ite_pro_id,
        'pro_name': e.product.pro_code + ' - ' + e.product.pro_name,
        'pro_status': e.status,
        'pro_unit': e['ite_price_' + r.ord_financing].formatMoney(),
        'pro_items': e.ite_units_before,
        'pro_delivered': e.ite_units,
        'pro_amount': e.ite_amount.formatMoney(),
      });
    });
    Table.table('products_client_table', 'pro_id', '.table_place', {
      'pro_name': 'Producto',
      'pro_status': 'Status',
      'pro_unit': 'Precio unitario',
      'pro_items': 'U. solicitadas',
      'pro_delivered': 'U. disponibles',
      'pro_amount': 'Subtotal',
    }, products);
    for (let i = 3; i < 6; i++)
      $('.products_client_table > tr > td:nth-child(' + i + ')').addClass('right-align');

    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  confirm_pedido() {
    let vars = ['shi_street', 'shi_external', 'shi_internal', 'shi_suburd', 'shi_postal', 'shi_city', 'shi_state', 'shi_additional', 'shi_social_reason', 'shi_rfc', 'shi_rfc_type', 'shi_cfdi_use'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, ['shi_street', 'shi_external', 'shi_internal', 'shi_suburd', 'shi_postal', 'shi_city', 'shi_state', 'shi_additional'], true)) {
      Core.toast("Por favor complete sus datos de envío antes de continuar", "red");
      $('.tabs').tabs('select', 'ship');
      return false;
    }

    vars = ['ord_financing'];
    data = z.gValues(vars);
    if (!z.mandatory(data, vars, true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    let total = $('.grand_subtotal').html().toSimpleNumber();
    let cont = true;
    switch(data.ord_financing){
      case '3':
        if (total < 300){
          cont = false;
          Core.toast('En compras a 3 meses sin intereses, el mínimo es $300.00', 'red');
        }
      break;
      case '6':
        if (total < 600){
          cont = false;
          Core.toast('En compras a 6 meses sin intereses, el mínimo es $600.00', 'red');
        }
      break;
      case '9':
        if (total < 900){
          cont = false;
          Core.toast('En compras a 9 meses sin intereses, el mínimo es $900.00', 'red');
        }
      break;
      case '12':
        if (total < 1200){
          cont = false;
          Core.toast('En compras a 12 meses sin intereses, el mínimo es $1,200.00', 'red');
        }
      break;
    }
    if (!cont)
      return false;
    marssoft.asyn.post({ class: 'login', method: 'complete_order', public: 'true', data: data }, (r) => {
      switch (r.response) {
        case 'true':
          $('.go_cart').click();
          $('.cart_actual').html('(' + marssoft.in.public.getCartNUmber(r.data) + ')');
          Core.toast('Su orden ha sido enviada con éxito.', 'green');
          break;
        default:
          Core.toast('Su orden no pudo ser procesada, intente nuevamente.', 'red');
          break;
      }
    });
  }

  pay_differed(v) {
    let id = v.val();
    marssoft.in.public.current_month = id;
    let type = '';
    let text = '';
    switch (id) {
      case '1':
        type = 'pro_price_1';
        text = '1 MSI';
        break;
      case '3':
        type = 'pro_price_3';
        text = '3 MSI';
        break;
      case '6':
        type = 'pro_price_6';
        text = '6 MSI';
        break;
      case '9':
        type = 'pro_price_9';
        text = '9 MSI';
        break;
      case '12':
        type = 'pro_price_12';
        text = '12 MSI';
        break;
    }
    id = id.toSimpleNumber();
    let total = 0;
    $.each(marssoft.in.public.cart.products, (i, e) => {
      let unit = e.product[type].toSimpleNumber();
      $('.unitary[data-product="' + e.product.pro_id + '"]').html(e.product[type].formatMoney() + ' ' + text);
      let units = $('.units[data-product="' + e.product.pro_id + '"]').attr('data-units').toSimpleNumber();
      let subtotal = unit * units;
      $('.subtotal[data-product="' + e.product.pro_id + '"]').html(subtotal.formatMoney() + ' ' + text);
      total += subtotal;
    });
    $('.grand_subtotal').html(total.formatMoney());
    $('.grand_total').html(total.formatMoney() + '<br>Pagará ' + (total / id).formatMoney() + ' a ' + id + ' mes' + ((id == 1) ? '' : 'es') + ' sin intereses ');
  }

  delete_item(v) {
    let id = v.attr('data-product');
    marssoft.asyn.post({ class: 'login', method: 'addCart', public: 'true', id: id, action: 'remove' }, (r) => {
      switch (r.response) {
        case 'true':
          $('.cart_actual').html('(' + marssoft.in.public.getCartNUmber(r.id) + ')');
          $('.go_cart').click();
          Core.toast('Producto eliminado del carrito', 'green');
          break;
        case 'out':
          Core.toast('Lamentamos informarle que no hay productos en existencia.', 'blue');
          break;
      }
    });
  }

  remove_item(v) {
    let id = v.attr('data-product');
    marssoft.asyn.post({ class: 'login', method: 'addCart', public: 'true', id: id, action: 'subtract' }, (r) => {
      switch (r.response) {
        case 'true':
          $('.cart_actual').html('(' + marssoft.in.public.getCartNUmber(r.id) + ')');
          $('.go_cart').click();
          Core.toast('Producto restado del carrito', 'green');
          break;
        case 'out':
          Core.toast('Lamentamos informarle que no hay productos en existencia.', 'blue');
          break;
      }
    });
  }

  add_item(v) {
    let id = v.attr('data-product');
    marssoft.asyn.post({ class: 'login', method: 'addCart', public: 'true', id: id }, (r) => {
      switch (r.response) {
        case 'true':
          $('.cart_actual').html('(' + marssoft.in.public.getCartNUmber(r.id) + ')');
          $('.go_cart').click();
          Core.toast('Producto agregado al carrito', 'green');
          break;
        case 'out':
          Core.toast('Lamentamos informarle que no hay productos en existencia.', 'blue');
          break;
      }
    });
  }

  login_now() {
    let vars = ['username', 'password'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, vars, true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'login', method: 'grant_user', data: data }, (r) => {
      switch (r.response) {
        case 'true':
          $('.go_cart').click();
          break;
        default:
          Core.toast("Error al iniciar sesión, verifique e intente nuevamente", "red");
          break;
      }
    });
  }

  code_sent(v) {
    let code = v.val();
    if (code.length == '6') {
      marssoft.asyn.post({ class: 'login', method: 'validate_code', public: 'true', code: code }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast('Se cuenta ha sido validada con éxito', 'green');
            $('.go_cart').click();
            break;
          case 'denied':
            Core.toast('Su código de seguridad no es correcto. Intente nuevamente.', 'red');
            break;
        }
      });
    }
  }

  send_email() {
    marssoft.asyn.post({ class: 'login', method: 'send_code', public: 'true' }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast('Código de seguridad enviado con éxito. Revise su bandeja de entrada', 'green');
          $('.code_sent').removeAttr('disabled');
          $('.send_email').addClass('disabled');
          marssoft.in.public.time = 60;
          marssoft.in.public.interval = setInterval(() => {
            $('.send_email').html('<i class="material-icons left">send</i>Enviar código de 6 dígitos (' + marssoft.in.public.time + ')');
            marssoft.in.public.time--;
            if (marssoft.in.public.time == -1) {
              clearInterval(marssoft.in.public.interval);
              $('.send_email').html('<i class="material-icons left">send</i>Enviar código de 6 dígitos');
              $('.send_email').removeClass('disabled');
            }
          }, 1000);
          break;
        case 'validated':
          Core.toast('Se cuenta ha sido validada con éxito', 'green');
          $('.go_cart').click();
          break;
        default:
          Core.toast('Falló el envío del código de verificación. Intente nuevamente', 'red');
          break;
      }
    });
  }

  save_ships() {
    let vars = ['shi_street', 'shi_external', 'shi_internal', 'shi_suburd', 'shi_postal', 'shi_city', 'shi_state', 'shi_additional', 'shi_social_reason', 'shi_rfc', 'shi_rfc_type', 'shi_cfdi_use'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, ['shi_street', 'shi_external', 'shi_internal', 'shi_suburd', 'shi_postal', 'shi_city', 'shi_state', 'shi_additional'], true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'login', method: 'save_chips', data: data }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast("Información guardada con éxito", "green");
          break;
        default:
          Core.toast("Error al iniciar sesión, verifique e intente nuevamente", "red");
          break;
      }
    });
  }

  create_account(v) {
    let vars = ['per_firstname', 'per_lastname', 'per_surname', 'per_birthday', 'per_gender', 'per_phone', 'per_email', 'use_id'];
    if (v || $('.use_password').val() != '') {
      vars.push('use_password');
      vars.push('confirm_password');
    }
    let data = z.gValues(vars);
    if (!z.mandatory(data, ['per_firstname', 'per_lastname', 'per_surname', 'per_email', 'use_id'], true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    if (v || $('.use_password').val() != '') {
      if (data.use_password != data.confirm_password) {
        Core.toast("Las constraseñas no coinciden", "red");
        return false;
      }
    }
    data = {
      personals: {
        per_firstname: data.per_firstname,
        per_lastname: data.per_lastname,
        per_surname: data.per_surname,
        per_email: data.per_email,
        per_birthday: data.per_birthday,
        per_gender: data.per_gender,
        per_phone: data.per_phone
      },
      users: {
        use_id: data.use_id,
        use_password: data.use_password
      }
    }
    marssoft.asyn.post({ class: 'login', method: ((v) ? 'create_user' : 'update_user'), data: data }, (r) => {
      switch (r.response) {
        case 'true':
          if (v) {
            Core.toast("Usuario creado con éxito", "green");
            $('.go_cart').click();
          } else {
            Core.toast("Información guardada con éxito", "green");
          }
          break;
        case 'user_exists':
          Core.toast("El nombre de usuario ya se encuentra ocupado. Intente con otro", "red");
          $('.use_id').val('');
          break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
          break;
      }
    });
  }

  go_cart() {
    marssoft.asyn.post({ class: 'login', method: 'get_cart', public: 'true' }, (r) => {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      switch (r.response) {
        case 'login_required':
          marssoft.in.public.login_info();
          break;
        case 'verify_email':
          marssoft.in.public.verify_email(r.data);
          break;
        case 'true':
          marssoft.in.public.account(r);
          break;
      }
    });
  }

  account(r) {
    marssoft.in.public.cart = r;
    let product = marssoft.in.public.getCartNUmber(r.cart);
    let msg = 'Aún no tiene productos, pero puede ir a comprar';
    if (product > 1) {
      msg = 'Tiene ' + product + ' productos en su carrito';
    } else if (product == 1) {
      msg = 'Tiene ' + product + ' producto en su carrito';
    }
    let cfdi = [
      ['G03', 'Gastos en general'],
      ['G01', 'Adquisición de Mercancías'],
      ['P01', 'Por Definir'],
    ];
    let products = ``;
    let total = 0;
    $.each(r.products, (i, e) => {
      let subtotal = (e.product.pro_price_1.toSimpleNumber() * e.units);
      total += subtotal;
      products += `
        <div class="col s12">
          <div class="card horizontal">
            <div class="card-image">
              <img src="resources/docs/${e.product.pictures[0].doc_id}.${e.product.pictures[0].doc_extension}">
            </div>
            <div class="card-stacked">
              <div class="card-content">
                <i class="material-icons red-text right pointer delete_item" data-product="${e.product.pro_id}">indeterminate_check_box</i>
                <h5 class="pointer go_product" data-target="${e.product.pro_id}">${e.product.pro_name}</h5>
                <h6 class="pointer go_category" data-target="${e.product.subfamily.sub_id}">${e.product.subfamily.sub_name}</h6>
                <table>
                  <tr>
                    <th>Importe unitario</th>
                    <td class="right-align unitary" data-product="${e.product.pro_id}">${e.product.pro_price_1.formatMoney()} 1 MSI</td>
                  </tr>
                  <tr>
                    <th>Unidades</th>
                    <td class="right-align units" data-product="${e.product.pro_id}" data-units="${e.units}">
                      <i class="material-icons primary_color right pointer add_item" data-product="${e.product.pro_id}">add_circle</i>
                      ${e.units}
                      <i class="material-icons primary_color left pointer remove_item" data-product="${e.product.pro_id}">remove_circle</i>
                    </td>
                  </tr>
                  <tr>
                    <th>Subtotal</th>
                    <td class="right-align subtotal" data-product="${e.product.pro_id}">${subtotal.formatMoney()} 1 MSI</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      `;
    });
    if (products == ``) {
      products = `
        <div class="col s12 center-align nothing_here">No tiene productos en su carrito</div>
      `;
    }
    let prices = [
      [1, 'Pagar a 1 MSI'],
      [3, 'Pagar a 3 MSI'],
      [6, 'Pagar a 6 MSI'],
      [9, 'Pagar a 9 MSI'],
      [12, 'Pagar a 12 MSI'],
    ];
    $('.main').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="assets/img/a.jpg">
            <div class="caption center-align">
              <h3>¡Hola ${r.client.personal.per_firstname}!</h3>
              <h5 class="light grey-text text-lighten-3">${msg}</h5>
            </div>
          </li>
        </ul>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12 offset-m9 m3 workforce">
            ${Form.btn(false, 'waves_effect white_text blue', 'close_client_session btn100', 'Cerrar sesión', '')}
          </div>
          <div class="col s12">
            <div class="card-panel row">
              <div class="col s12">
                <ul class="tabs">
                  <li class="tab col s3"><a href="#cart" class="active">Mi carrito (${product})</a></li>
                  <li class="tab col s3"><a href="#orders">Mis pedidos</a></li>
                  <li class="tab col s3"><a href="#ship">Facturación y envío</a></li>
                  <li class="tab col s3"><a href="#profile">Mis datos</a></li>
                </ul>
              </div>

              <div id="orders" class="col s12 login_form">
                <div class="col s12 offset-m6 m3">
                  ${Form.btn(false, 'waves_effect white_text primary_background', 'client_order_details btn100 disabled', 'Detalles', 'keyboard_arrow_right')}
                </div>
                <div class="col s12 m3">
                  ${Form.btn(false, 'waves_effect white_text green', 'pay_order btn100 disabled', 'Pagar', 'attach_money')}
                </div>
                <div class="col s12 table_place"></div>
              </div>

              <div id="cart" class="col s12 login_form">
                <div class="col s12">
                  <h5>Productos</h5>
                </div>
                ${products}
                <div class="col s12 cart_panel">
                  <div class="card-panel">
                    <table>
                      <tr>
                        <th>Subtotal</th>
                        <td class="right-align grand_subtotal">${total.formatMoney()}</td>
                      </tr>
                      <tr>
                        <th>Envío</th>
                        <td class="right-align">Calculado al confirmar</td>
                      </tr>
                      <tr>
                        <th>Pago a meses sin intereses</th>
                        <td class="right-align">${Form.select('ord_financing', 'Planes de financiamiento', prices, '', '', '')}</td>
                      </tr>
                      <tr>
                        <th>Total a pagar*</th>
                        <td class="right-align grand_total">${total.formatMoney()}<br>Pagará ${total.formatMoney()} a 1 mes sin intereses</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="col s12 input-field cart_panel">
                  ${Form.btn(true, 'waves_effect white_text green', 'confirm_pedido btn100 ', 'Confirmar pedido', 'save', '')}
                </div>
                <div class="col s12 cart_panel">
                  * El costo no incluye envío, este se calculará al confirmar el pedido por uno de nuestros agentes de ventas de Talabartería la Guadalupana.<br><br>
                  Al realizar el pedido, este se revisará contra existencia final para asegurarle la mejor experiencia de compra en Talabartería la Guadalupana.
                </div>
                
              </div>
              <div id="ship" class="col s12 login_form">
                <div class="col s12">
                  <h6>Datos de envío</h6>
                </div>
                <div class="col s12 m8 input-field">
                  ${Form.input('shi_street', 'text', 'Calle', '', '', '', 100)}
                </div>
                <div class="col s12 m2 input-field">
                  ${Form.input('shi_external', 'text', 'Número exterior', '', '', '', 100)}
                </div>
                <div class="col s12 m2 input-field">
                  ${Form.input('shi_internal', 'text', 'Número interior', '', '', '', 100)}
                </div>
                <div class="col s12 m9 input-field">
                  ${Form.input('shi_suburd', 'text', 'Colonia', '', '', '', 100)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('shi_postal', 'text', 'Código postal', '', '', '', 5)}
                </div>
                <div class="col s12 m6 input-field">
                  ${Form.input('shi_city', 'text', 'Ciudad', '', '', '', 100)}
                </div>
                <div class="col s12 m6 input-field">
                  ${Form.input('shi_state', 'text', 'Estado', '', '', '', 100)}
                </div>
                <div class="col s12 input-field">
                  ${Form.area('shi_additional', 'Información adicional que ayude al servicio de paquetería', 200)}
                </div>
                <div class="col s12">
                  <h6>Información fiscal (opcional)</h6>
                </div>
                <div class="col s12 m9 input-field">
                  ${Form.input('shi_social_reason', 'text', 'Razón social', '', '', '', 200)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('shi_rfc', 'text', 'RFC', '', '', '', 13)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.select('shi_rfc_type', 'Tipo de persona', [[1, 'Persona física'], [2, 'Persona moral']])}
                </div>
                <div class="col s12 m9 input-field">
                  ${Form.select('shi_cfdi_use', 'Uso del CFDI', cfdi)}
                </div>
                <div class="col s12 m4 offset-m8 input-field">
                  ${Form.btn(false, 'waves_effect white_text blue', 'save_ships btn100 ', 'Guardar cambios', 'save', '')}
                </div>
              </div>

              <div id="profile" class="col s12 login_form">
                <div class="col s12">
                  <h6>Datos personales</h6>
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.input('per_firstname', 'text', 'Nombre(s)', '', '', '', 100)}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.input('per_lastname', 'text', 'Apellido paterno', '', '', '', 100)}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.input('per_surname', 'text', 'Apellido materno', '', '', '', 100)}
                </div>
                <div class="col s12"></div>
                <div class="col s12 m4 input-field">
                  ${Form.input('per_birthday', 'text', 'Fecha de nacimiento', '')}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.select('per_gender', 'Género', [[1, 'Mujer'], [2, 'Hombre']])}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.input('per_phone', 'number', 'Teléfono a 10 dígitos', '', '', '', 10)}
                </div>
                <div class="col s12 input-field">
                  ${Form.input('per_email', 'email', 'Correo electrónico', '', '', '', 150)}
                </div>
                <div class="col s12">
                  <h6>Datos de inicio de sesión</h6>
                </div>
                <div class="col s12 m6 input-field">
                  ${Form.input('use_id', 'text', 'Nombre de usuario', '', '', '', 30, '', 'autocomplete="off"')}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('use_password', 'password', 'Contraseña', '', '', '', 30, '', 'autocomplete="new-password"')}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('confirm_password', 'password', 'Confirmar contraseña', '', '', '', 30, '', 'autocomplete="new-password"')}
                </div>
                <div class="col s12 m4 offset-m8 input-field">
                  ${Form.btn(false, 'waves_effect white_text blue', 'update_client btn100 ', 'Guardar cambios', 'save', '')}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
    $('.tabs').tabs();
    if (r.cart.length == 0) {
      $('.cart_panel').hide(0);
    }
    $.each(r.orders, (i, e) => {
      r.orders[i].ord_subtotal = r.orders[i].ord_subtotal.formatMoney();
      r.orders[i].ord_shipping_amount = r.orders[i].ord_shipping_amount.formatMoney();
      r.orders[i].ord_total = r.orders[i].ord_total.formatMoney();
      r.orders[i]['ord_created_at'] = moment(r.orders[i]['ord_created_at']).format('DD MMMM YYYY');

    });
    Table.table('client_orders_table', 'ord_id', '.table_place', {
      'ord_id': 'Pedido',
      'ord_created_at': 'Fecha',
      'ord_subtotal': 'Subtotal',
      'ord_shipping_amount': 'Envío',
      'ord_total': 'Total',
      'ord_financing': 'Meses',
      'status': 'Status'
    }, r.orders);
    for (let i = 3; i < 6; i++)
      $('.client_orders_table > tr > td:nth-child(' + i + ')').addClass('right-align');

    if (r.cart.length == 0 && r.orders.length > 0) {
      $('.tabs').tabs('select', 'orders');
    }

    // PROFILE INFORMATION
    $('.use_id').attr('disabled', 'disabled').val(r.client.use_id);
    $('.per_firstname').val(r.client.personal.per_firstname);
    $('.per_lastname').val(r.client.personal.per_lastname);
    $('.per_surname').val(r.client.personal.per_surname);
    marssoft.fecha.Dtpicker('.per_birthday');
    $('.per_birthday').val(r.client.personal.birthday);
    $('.per_gender').val(r.client.personal.per_gender);
    $('.use_profile').val(r.client.use_profile);
    $('.per_phone').val(r.client.personal.per_phone);
    $('.per_email').val(r.client.personal.per_email);
    // SHIP AND BILLING INFORMATION
    $('.shi_street').val(r.ship.shi_street);
    $('.shi_external').val(r.ship.shi_external);
    $('.shi_internal').val(r.ship.shi_internal);
    $('.shi_suburd').val(r.ship.shi_suburd);
    $('.shi_postal').val(r.ship.shi_postal);
    $('.shi_city').val(r.ship.shi_city);
    $('.shi_state').val(r.ship.shi_state);
    $('.shi_additional').val(r.ship.shi_additional);

    $('.shi_social_reason').val(r.ship.shi_social_reason);
    $('.shi_rfc').val(r.ship.shi_rfc);
    $('.shi_rfc_type').val(r.ship.shi_rfc_type);
    $('.shi_cfdi_use').val(r.ship.shi_cfdi_use);
    if (marssoft.in.public.current_month != undefined)
      $('.ord_financing').val(marssoft.in.public.current_month);
    else
      $('.ord_financing').val('1');
    $('.per_gender, .shi_rfc_type, .shi_cfdi_use, .ord_financing').formSelect();
    if (marssoft.in.public.current_month != undefined)
      $('.ord_financing').change();
    $('input').characterCounter();
    M.updateTextFields()
    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  verify_email(r) {
    $('.main').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="assets/img/a.jpg">
            <div class="caption center-align">
              <h3>Confirmación de correo electrónico</h3>
              <h5 class="light grey-text text-lighten-3"><span class="pointer go_category">Lo hacemos para brindarte una seguridad excepcional</h5>
            </div>
          </li>
        </ul>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12">
            <div class="card-panel row">
              <div class="col s12 justify-align">
                <h5>¡Hola ${r.personal.fullname}!</h5>
                Le damos la bienvenida a Talabartería la Guadalupana, el lugar donde podrá realizar las mejoras compras de monturas y accesorios. Para
                brindarle la mejor experiencia de compra, es necesario verificar su cuenta de correo electrónico dado que en el, recibirá las actualizaciones
                de sus pedidos, tickets de compra y más.
              </div>
              <div class="col s12 m8 offset-m2 input-field">
                ${Form.btn(true, 'waves_effect white_text blue', 'send_email btn100 ', 'Enviar código de 6 dígitos', 'send', '')}
              </div>
              <div class="col s12 m8 offset-m2 input-field">
                ${Form.input('code_sent', 'number', 'Código recibido', '', '', '', 6, '', 'disabled="disabled"')}
              </div>
            </div>
          </div>
        </div>
      </div>
    `);

    $('input').characterCounter();
    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  login_info() {
    $('.main').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="assets/img/a.jpg">
            <div class="caption center-align">
              <h3>Bienvenido a Talabartería la Guadalupana</h3>
              <h5 class="light grey-text text-lighten-3"><span class="pointer go_category">Inicie sesión o cree una cuenta</h5>
            </div>
          </li>
        </ul>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12">
            <div class="card-panel row">
              <div class="col s12">
                <ul class="tabs">
                  <li class="tab col s6"><a href="#login" class="active">Iniciar sesión</a></li>
                  <li class="tab col s6"><a href="#signin">Crear una cuenta</a></li>
                </ul>
              </div>
              <div id="login" class="col s12 login_form">
                <div class="col s12 m8 offset-m2 input-field">
                  ${Form.input('username', 'text', 'Nombre de usuario o correo electrónico', '', '', '')}
                </div>
                <div class="col s12 m8 offset-m2 input-field">
                  ${Form.input('password client', 'password', 'Contraseña', '', '', '')}
                </div>
                <div class="col s12 m4 offset-m2 input-field">
                  ${Form.btn(false, 'waves_effect white_text grey', 'recover_account btn100 ', 'Recuperar acceso', 'lock', '')}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.btn(false, 'waves_effect white_text blue', 'login_now btn100 ', 'Iniciar sesión', 'person', '')}
                </div>
              </div>
              <div id="signin" class="col s12 login_form">
                <div class="col s12">
                  <h6>Datos personales</h6>
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.input('per_firstname', 'text', 'Nombre(s)', '', '', '', 100)}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.input('per_lastname', 'text', 'Apellido paterno', '', '', '', 100)}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.input('per_surname', 'text', 'Apellido materno', '', '', '', 100)}
                </div>
                <div class="col s12 hidden"></div>
                <div class="col s12 m4 input-field hidden">
                  ${Form.input('per_birthday', 'text', 'Fecha de nacimiento', '')}
                </div>
                <div class="col s12 m4 input-field hidden">
                  ${Form.select('per_gender', 'Género', [[1, 'Mujer'], [2, 'Hombre']])}
                </div>
                <div class="col s12 m4 input-field hidden">
                  ${Form.input('per_phone', 'number', 'Teléfono a 10 dígitos', '', '', '', 10)}
                </div>
                <div class="col s12 input-field">
                  ${Form.input('per_email', 'email', 'Correo electrónico', '', '', '', 150)}
                </div>
                <div class="col s12">
                  <h6>Datos de inicio de sesión</h6>
                </div>
                <div class="col s12 m6 input-field">
                  ${Form.input('use_id', 'text', 'Nombre de usuario', '', '', '', 30, '', 'autocomplete="off"')}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('use_password', 'password', 'Contraseña', '', '', '', 30, '', 'autocomplete="new-password"')}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('confirm_password', 'password', 'Confirmar contraseña', '', '', '', 30, '', 'autocomplete="new-password"')}
                </div>
                <div class="col s12 m4 offset-m8 input-field">
                  ${Form.btn(false, 'waves_effect white_text blue', 'create_account btn100 ', 'Crear cuenta', 'account_circle', '')}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
    $('.per_gender').val('2').formSelect();
    $('.tabs').tabs();
    $('input').characterCounter();
    marssoft.fecha.Dtpicker('.per_birthday');
    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  getCartNUmber(cart) {
    let number = 0;
    $.each(cart, (i, e) => {
      number += e;
    });
    return number;
  }

  buy_now(v) {
    let max = v.attr('data-max').toSimpleNumber();
    if (max < 1) {
      Core.toast('Producto no disponible por el momento', 'blue');
      return false;
    }
    let product = v.attr('data-product');
    marssoft.asyn.post({ class: 'login', method: 'addCart', public: 'true', id: product }, (r) => {
      switch (r.response) {
        case 'true':
          $('.cart_actual').html('(' + marssoft.in.public.getCartNUmber(r.id) + ')');
          Core.toast('Producto agregado al carrito', 'green');
          break;
        case 'out':
          Core.toast('Lamentamos informarle que no hay productos en existencia.', 'blue');
          break;
      }
    });
  }

  pro_toprice(v) {
    let id = v.val();
    let text = $('select.pro_toprice > option[value="' + id + '"]').html();
    $('.product_price').html(text);
  }

  changeImage(v){
    if (v.hasClass('after_image')){
      if (marssoft.in.public.slider_i < marssoft.in.public.slider.length - 1){
        marssoft.in.public.slider_i++;
      } else
        marssoft.in.public.slider_i = 0;
    } else {
      if (marssoft.in.public.slider_i >= 0){
        marssoft.in.public.slider_i--;
        if (marssoft.in.public.slider_i < 0){
          marssoft.in.public.slider_i = marssoft.in.public.slider.length - 1;
        }
      }
    }
    $('.principal_picture').attr('src', `resources/docs/${marssoft.in.public.slider[marssoft.in.public.slider_i].doc_id}.${marssoft.in.public.slider[marssoft.in.public.slider_i].doc_extension}`);
  }

  change_slider(){
    if (marssoft.in.public.slider_i < marssoft.in.public.slider.length - 1){
      marssoft.in.public.slider_i++;
    } else
      marssoft.in.public.slider_i = 0;
    $('.principal_picture').attr('src', `resources/docs/${marssoft.in.public.slider[marssoft.in.public.slider_i].doc_id}.${marssoft.in.public.slider[marssoft.in.public.slider_i].doc_extension}`);
  }

  open_product(id) {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    marssoft.asyn.post({ class: 'login', method: 'getProduct', public: 'true', id: id }, (r) => {
      let related = r.related;
      r = r.product;
      let prices = [
        [1, r.pro_price_1.formatMoney() + ' 1 MSI'],
      ];

      if (r.pro_price_3 != '' && r.pro_price_3 != '0'){
        prices.push([3, r.pro_price_3.formatMoney() + ' 3 MSI']);
      }
      if (r.pro_price_6 != '' && r.pro_price_6 != '0'){
        prices.push([6, r.pro_price_6.formatMoney() + ' 6 MSI']);
      }
      if (r.pro_price_9 != '' && r.pro_price_9 != '0'){
        prices.push([9, r.pro_price_9.formatMoney() + ' 9 MSI']);
      }
      if (r.pro_price_12 != '' && r.pro_price_12 != '0'){
        prices.push([12, r.pro_price_12.formatMoney() + ' 12 MSI']);
      }

      let pictures = ``;
      $.each(r.pictures, (i, e) => {
        pictures += `
          <a class="carousel-item t"><img src="resources/docs/${e.doc_id}.${e.doc_extension}"></a>
        `;
      });
      marssoft.in.public.slider = r.pictures;
      marssoft.in.public.slider_i = 0;
      if (marssoft.in.public.slider_interval == undefined)
        marssoft.in.public.slider_interval = 0;
      // clearInterval(marssoft.in.public.slider_interval);
      // marssoft.in.public.slider_interval = setInterval(marssoft.in.public.change_slider,5000);
      let products = ``;
      $.each(related, (i, e) => {
        products += marssoft.in.public.product(e);
      });
      let features = ``;
      $.each(r.features, (i, e) => {
        features += `
          <div class="col s12 grey-text">
            <h6 class="black-text">${e.fea_title}</h6>
            ${e.fea_descr}
          </div>
        `
      });
      if (features == ''){
        features = `
          <div class="col s12 grey-text center-align">
            Este producto no tiene características avanzadas
          </div>
        `;
      }

      $('.main').html(`
        <div class="slider categories">
          <ul class="slides">
            <li>
              <img src="assets/img/a.jpg">
              <div class="caption left-align">
                <h3>${r.pro_name}</h3>
                <h5 class="light grey-text text-lighten-3"><a class="pointer go_category underline" href="#categorias/${r.subfamily.sub_id}" data-target="${r.subfamily.sub_id}">${r.subfamily.sub_name}</a> / ${r.pro_exists} disponibles para envío inmediato</h5>
              </div>
            </li>
          </ul>
        </div>
        <div class="container">
          <div class="row">
            <div class="col s12">
              <div class="card-panel row product_card">
                <div class="col s12 m7 primary_photo z-depth-2">
                  <div class="d_before_image">
                    ${Form.btn(false, 'waves-effect waves-green white btn-flat', 'before_image btn100 center-align', '<', '', '')}
                  </div>
                  <div class="d_after_image">
                    ${Form.btn(false, 'waves-effect waves-green white btn-flat', 'after_image btn100 center-align', '>', '', '')}
                  </div>
                  <img class="materialboxed principal_picture" data-caption="${r.pro_name}" src="resources/docs/${r.pictures[0].doc_id}.${r.pictures[0].doc_extension}" width="100%">
                </div>
                <div class="col s12 m5">
                  <div class="col s12 offer-price ${(r.pro_offer_price == '' || r.pro_offer_price == '0') ? 'hidden' : ''}">${r.pro_offer_price.formatMoney()}</div>
                  <div class="col s12 product_price">${r.pro_price_1.formatMoney()} 1 MSI</div>
                  <div class="col s12 input-field">
                    ${Form.select('pro_toprice', 'Ópciones a meses sin intereses', prices, '', '', '')}
                  </div>
                  <div class="col s12 input-field">
                    ${Form.btn(false, 'waves_effect white_text blue', 'buy_now btn100 ', 'Comprar ahora', 'add_shopping_cart', 'data-max="' + r.pro_exists + '" data-product="' + r.pro_id + '"')}
                  </div>
                </div>
                <div class="col s12"></div>
                <div class="col s12 content-text">${r.pro_descr}</div>
                <div class="col s12 center-align"><h5>Características del producto</h5></div>
                ${features}
                <div class="col s12 center-align hidden"><h5>Más imágenes de ${r.pro_name}</h5></div>
                <div class="col s12 carousel-wrapper hidden">
                  <div class="carousel t m">${pictures}</div>
                </div>
            </div>
            <div class="col s12"><h5 class="center-align">Productos relacionados</h5></div>
            <div class="col s12">${products}</div>
          </div>
          
          </div>

        </div>
      `);
      marssoft.in.public.adjust();
      setTimeout(() => {
        marssoft.in.public.adjust();
        setTimeout(() => {
          marssoft.in.public.adjust();
          setTimeout(() => {
            marssoft.in.public.adjust();
            setTimeout(() => {
              marssoft.in.public.adjust();
            }, 500);
          }, 500);
        }, 500);
      }, 500);
      if (r.pro_exists == '0') {
        $('.buy_now').addClass('disabled').html('Producto no disponible');
      }

      $('.pro_toprice').formSelect();
      $('.materialboxed').materialbox();
      $('.slider.categories').slider({
        indicators: false,
        height: 350,
      });
      $('.slider.product').slider({
        indicators: false,
        height: 250,
      });
      $('.carousel').carousel({
        dist: 0,
        numVisible: 2,
      });
    });
  }

  adjust(){
    $('.d_after_image').css('margin-left', ($('.primary_photo').width()-66)+'px');
    $('.d_before_image, .d_after_image').css('margin-top', (($('.primary_photo').height() / 2) - 33)+'px');
  }

  pro_search(){
    clearTimeout(marssoft.in.public.client_timeout);
    marssoft.in.public.client_timeout = setTimeout(() => {
      let looking = $('.pro_search').val();
      if (looking == '')
        $('.product_sortable').show(0);
      else {
        looking = looking.toLowerCase();
        $('.product_sortable').each(function(){
          let has = $(this).attr('data_name').toLowerCase();
          if (has.indexOf(looking) != -1){
            $(this).show();
          } else {
            $(this).hide();
          }
        });
      }
    }, 600);
  }

  open_category(id) {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    marssoft.asyn.post({ class: 'login', method: 'getCategories', public: 'true', id: id }, (r) => {
      let products = ``;
      $.each(r.products, (i, e) => {
        products += marssoft.in.public.product(e);
      });

      $('.main').html(`
        <div class="slider categories">
          <ul class="slides">
            <li>
              <img src="assets/img/a.jpg">
              <div class="caption center-align">
                <h3>${r.category.sub_name}</h3>
                <h5 class="light grey-text text-lighten-3">¡Contamos con más de ${r.products.length} productos para usted!</h5>
              </div>
            </li>
          </ul>
        </div>
        <div class="container">
          <div class="row">
            <div class="col s12 m6 input-field">
              ${Form.input('pro_search', 'text', 'Busque algo en el cuadro', 'search', '', '', 100)}
            </div>
            <div class="col s6 m3 input-field">
              ${Form.select('pro_alpha', 'Orden alfabético', [[1, 'A-Z'], [2, 'Z-A']], '', '', '')}
            </div>
            <div class="col s6 m3 input-field">
              ${Form.select('pro_economic', 'Orden económico', [[1, 'Más costoso primero'], [2, 'Más económico primero']], '', '', '')}
            </div>
          </div>
          <div class="row" id="product_container">
            ${products}
          </div>
        </div>
      `);

      $('.pro_alpha').formSelect();
      $('.pro_economic').formSelect();
      $('.slider.categories').slider({
        indicators: false,
        height: 350,
      });

      $('.slider.product').slider({
        indicators: false,
        height: 250,
      });
    });
  }

  filter(v) {
    let value = v.val();
    let target = '';
    console.log(value);
    if (v.hasClass('pro_alpha')) {
      $('.pro_economic').val('').formSelect();
      target = 'data_name';
    } else {
      $('.pro_alpha').val('').formSelect();
      target = 'data_price';
    }

    let parent = document.getElementById('product_container');

    let toSort = parent.children;
    toSort = Array.prototype.slice.call(toSort, 0);
    console.log(toSort);
    toSort.sort(function (a, b) {
      var aord = $(a).attr(target);
      var bord = $(b).attr(target);
      if (target == 'data_price')
        return value == 2 ? aord - bord : bord - aord;
      else {
        return (value == 1) ? (aord < bord) ? -1 : 1 : (aord > bord) ? -1 : 1;
      }
    });
    parent.innerHTML = "";
    for (let i = 0, l = toSort.length; i < l; i++) {
      parent.appendChild(toSort[i]);
    }
  }

  product(e) {
    let pictures = ``;
    $.each(e.pictures, (j, k) => {
      pictures += `
        <li>
          <img src="resources/docs/${k.doc_id}.${k.doc_extension}">
        </li>
      `;
    });
    let products = `
      <div class="col s12 m4 product_sortable" data_name="${e.pro_name}" data_price="${e.pro_price_1}">
        <div class="card">
          <div class="card-image">
            <div class="slider product">
              <ul class="slides">${pictures}</ul>
            </div> 
          </div>
          <div class="card-content product_description">
            <span class="card-title go_product pointer" data-exist="${e.pro_exists}" data-target="${e.pro_id}"><a href="#productos/${e.pro_id}">${e.pro_name}</a></span>
            <p><a class="go_category" data-target="${e.pro_sub_id}" href="#categorias/${e.pro_sub_id}">${e.subfamily.sub_name}</a></p>
            <p>${e.pro_descr}</p>
          </div>
          <div class="card-action center-align">
            <a class="offer-price ${(e.pro_offer_price == '' || e.pro_offer_price == '0') ? 'hidden' : ''}" href="#">${e.pro_offer_price.formatMoney()}</a><br>
            <a class="normal-price go_product blue-text" href="#" data-target="${e.pro_id}">${e.pro_price_1.formatMoney()} / 1MSI</a>
          </div>
        </div>
      </div>
    `;
    return products;
  }

  home() {
    marssoft.asyn.post({ class: 'login', method: 'getResourcesDashboard', public: 'true' }, (r) => {
      let products = ``;
      $.each(r.products, (i, e) => {
        products += marssoft.in.public.product(e);
      });
      let sliders = ``;
      $.each(r.sliders, (i, e) => {
        switch (e.sli_alignment) {
          case '2':
            e.sli_alignment = 'left-align';
            break;
          case '3':
            e.sli_alignment = 'right-align';
            break;
          default:
            e.sli_alignment = 'center-align';
            break;
        }
        sliders += `
          <li>
            <img src="resources/sliders/${e.sli_id}.${e.sli_extension}">
            <div class="caption ${e.sli_alignment}">
              <h3>${e.sli_title}</h3>
              <h5 class="light grey-text text-lighten-3">${e.sli_descr}</h5>
            </div>
          </li>
        `;
      });
      $('.main').html(`
        <div class="slider base">
          <ul class="slides">${sliders}</ul>
        </div>
        <div class="container">
          <div class="row">
            <div class="col s12 center-align">
              <h5>Conozca nuestros mejores productos</h5>
            </div>

            ${products}

            <div class="col s12 center-align">
              <h5>Cuidamos cada proceso de elaboración</h5>
            </div>

            <div class="col s12 m6">
              <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                  <img class="activator" src="assets/img/12853884615_66e68bb0ab_h.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Proceso 1<i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                  <p>Here is some more information about this product that is only revealed once clicked on.</p>
                </div>
              </div>
            </div>

            <div class="col s12 m6">
              <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                  <img class="activator" src="assets/img/12853884615_66e68bb0ab_h.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Proceso 2<i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                  <p>Here is some more information about this product that is only revealed once clicked on.</p>
                </div>
              </div>
            </div>

            <div class="col s12 center-align">
              <h5>Formas de pago admitidas</h5>
            </div>

            <div class="col s12 center-align">
              <h6>Tarjetas de Crédito</h6>
            </div>

            <div class="col s12 m6 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/TC_VISA@2x.png" width="228">
                </div>
              </div>
            </div>
            <div class="col s12 m6 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/TC_MASTERCARD@2x.png" width="228">
                </div>
              </div>
            </div>

            <div class="col s12 center-align">
              <h6>Tarjetas de Débito</h6>
            </div>

            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_BBVA@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_CITIBANAMEX@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_HSBC@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_SANTANDER@2x.png" width="100%">
                </div>
              </div>
            </div>

            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_AZTECA@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_IXE@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_SCOTIABANK@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_INBURSA@2x.png" width="100%">
                </div>
              </div>
            </div>

            <div class="col s12 center-align">
              <h6>Tarjetas de crédito a meses sin intereses</h6>
            </div>

            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_BBVA@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_CITIBANAMEX@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_INBURSA@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m3 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_SANTANDER@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m4 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_BANORTE@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m4 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_BANREGIO@2x.png" width="100%">
                </div>
              </div>
            </div>
            <div class="col s12 m4 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/banco_HSBC@2x.png" width="100%">
                </div>
              </div>
            </div>

            <div class="col s12 center-align">
              <h6>Servicio de pagos con tarjeta a través de</h6>
            </div>

            <div class="col s12 m4 offset-m4 center-align">
              <div class="card">
                <div class="card-content">
                  <img src="assets/img/logo.png" width="100%">
                </div>
              </div>
            </div>

            <div class="col s12 center-align">
              <h5>Cómo llegar a talabartería la Guadalupana</h5>
            </div>
            <div class="col s12 center-align">
              <div class="card">
                <div class="card-content">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1679.6932824737735!2d-97.04702782395744!3d19.07324350996718!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85c4de3139d4fdc9%3A0x36d51386c612ce3c!2sTalabarteria%20La%20Guadalupana!5e0!3m2!1ses-419!2smx!4v1598503244776!5m2!1ses-419!2smx" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      `);
      $('.slider.base').slider();
      $('.slider.product').slider({
        indicators: false,
        height: 250,
      });
    });



  }

  action_print_area(){
    $('.print_area').printArea({
      extraCss: 'assets/css/materialize.css,assets/css/index.css',
      mode: 'popup',
      popClose: true
    });
  }

  validate(id, result = true){
    window.scrollTo({ top: 0, behavior: 'smooth' });
    $('.main').html(`
      <div class="slider categories">
        <ul class="slides">
          <li>
            <img src="assets/img/a.jpg">
            <div class="caption center-align">
              <h3>Validando compra...</h3>
              <h5 class="light grey-text text-lighten-3">Esto puede llevar unos segundos</h5>
            </div>
          </li>
        </ul>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12 data_access">
            
            </div>
          </div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'login', method: 'validateCharge', public: 'true', id: id }, (r) => {
      switch(r.response){
        case 'failed':
          $('.caption > h3').html('Error');
          $('.caption > h5').html('Su compra no pudo ser procesada');
          $('.data_access').html(`
            <div class="card-panel row">
              <div class="col s12 center-align">
                <i class="material-icons large red-text">remove_circle</i>
              </div>
              <div class="col s12">
                <h5 class="response_code center-align"></h5>
              </div>
              <div class="col s12 justify-align">
                <h6>Si el cargo se realizó a su tarjeta pero el sistema no lo aplicó a su pedido, favor de enviar un correo a fernandoc@grupomarssoft.com con asunto: <b>pago declinado falso positivo</b> y en <b>contenido</b> el siguiente código: ${r.authorization}</h6>
              </div>
            </div>
          `);
          $('.response_code').html('Se presentó un error con la transacción de su pago.');
          // switch(r.description.error_code){
          //   case 1003:
          //     $('.response_code').html('La operación no se pudo completar por que el valor de uno o más de los parámetros no es correcto.');
          //   break;
          //   case 3001:
          //     $('.response_code').html('La tarjeta fue declinada por el banco emisor.');
          //   break;
          //   case 3002:
          //     $('.response_code').html('La tarjeta ha expirado.');
          //   break;
          //   case 3003:
          //     $('.response_code').html('La tarjeta no tiene fondos suficientes.');
          //   break;
          //   case 3004:
          //     $('.response_code').html('La tarjeta ha sido identificada como una tarjeta robada.');
          //   break;
          //   case 3005:
          //     $('.response_code').html('La tarjeta ha sido rechazada por el sistema antifraudes.');
          //   break;
          //   case 3006:
          //     $('.response_code').html('La operación no esta permitida para este cliente o esta transacción.');
          //   break;
          //   case 3007:
          //     $('.response_code').html('Deprecado. La tarjeta fue declinada.');
          //   break;
          //   case 3008:
          //     $('.response_code').html('La tarjeta no es soportada en transacciones en línea.');
          //   break;
          //   case 3009:
          //     $('.response_code').html('La tarjeta fue reportada como perdida.');
          //   break;
          //   case 3010:
          //     $('.response_code').html('El banco ha restringido la tarjeta.');
          //   break;
          //   case 3011:
          //     $('.response_code').html('El banco ha solicitado que la tarjeta sea retenida. Contacte al banco.');
          //   break;
          //   case 3012:
          //     $('.response_code').html('Se requiere solicitar al banco autorización para realizar este pago.');
          //   break;
          //   case 5001:
          //     $('.response_code').html('La orden con este identificador externo (external_order_id) ya existe');
          //   break;
          //   default:
          //     $('.response_code').html('Se presentó un error con la transacción de su pago.');
          //   break;
          // }
        break;
        case 'completed':
          if (result){
            $('.caption > h3').html('Felicidades');
            $('.caption > h5').html('Su compra ha sido aprobada por su banco');
          } else {
            $('.caption > h3').html('Detalle de su compra');
            $('.caption > h5').html('Pagado el '+moment(r.single.ord_paid_at).format('DD MMMM YYYY [a las] hh:mm'));
          }
          let products = ``;
          $.each(r.single.items, (i, e) => {
            let single = e['ite_price_'+r.single.ord_financing].toSimpleNumber();
            e.ite_units = e.ite_units.toSimpleNumber();
            products += `
              <tr>
                <td>${e.product.pro_code}</td>
                <td>${e.product.pro_name}</td>
                <td>${e.ite_units}</td>
                <td class="right-align">${single.formatMoney()}</td>
                <td class="right-align">${e.ite_amount.formatMoney()}</td>
              </tr>
            `;
          });
          $('.data_access').html(`
            <div class="card-panel row">
              <div class="col s12 center-align ${result ? '' : 'hidden'}">
                <i class="material-icons large green-text">check_circle</i>
              </div>
              <div class="col s12 center-align ${result ? '' : 'hidden'}">
                <h4>Pago aplicado con éxito</h4>
              </div>
              <div class="col s12">
                <h5 class="center-align">Detalles de su pedido</h5>
              </div>
              <div class="col s12 m3 offset-m9">
                ${Form.btn(false, 'waves_effect white_text green', 'action_print_area btn100', 'Imprimir', 'print')}
              </div>
              <div class="col s12 print_area">
                <table>
                  <tr>
                    <td colspan="2"><img src="assets/img/logo_b.png" width="80" /><h6>Talabartería la Guadalupana</h6></td>
                  </tr>
                  <tr>
                    <td>Fecha del pedido</td>
                    <th class="right-align">${moment(r.single.ord_created_at).format('DD MMMM YYYY [a las] hh:mm')}</th>
                  </tr>
                  <tr>
                    <td>Aprobación del pedido por comercio</td>
                    <th class="right-align">${moment(r.single.ord_accepted_at).format('DD MMMM YYYY [a las] hh:mm')}</th>
                  </tr>
                  <tr>
                    <td>Fecha del pago</td>
                    <th class="right-align">${moment(r.single.ord_paid_at).format('DD MMMM YYYY [a las] hh:mm')}</th>
                  </tr>
                  <tr>
                    <td>Orden de compra</td>
                    <th class="right-align">${r.single.ord_id}</th>
                  </tr>
                  <tr>
                    <td>Total pagado</td>
                    <th class="right-align">${r.single.ord_total.formatMoney()}</th>
                  </tr>
                  <tr>
                    <td>Forma de pago</td>
                    <th class="right-align">${r.single.openpay.ope_bank} ${r.single.openpay.ope_brand} ${r.single.openpay.ope_credit_type}</th>
                  </tr>
                  <tr>
                    <td>No. autorización openPay</td>
                    <th class="right-align">${r.single.ord_openpay_authorization} / ${r.single.ord_openpay_code}</th>
                  </tr>
                  <tr>
                    <td>Plan de mensualidades</td>
                    <th class="right-align">${r.single.ord_financing} mes${r.single.ord_financing == '1' ? '' : 'es'} sin intereses</th>
                  </tr>
                  <tr class="${r.single.ord_financing == '1' ? 'hidden' : ''}">
                    <td>Usted pagará ${r.single.ord_financing} pagos sin intereses de</td>
                    <th class="right-align">${r.single.ord_montly_payment.formatMoney()}</th>
                  </tr>
                </table>
                <table>
                  <tr>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Unidades</th>
                    <th>Precio unitario</th>
                    <th>Subtotal</th>
                  </tr>
                  ${products}
                </table>
              </div>
            </div>
          `);
        break;
      }
    });
    $('.slider.categories').slider({
      indicators: false,
      height: 350,
    });
  }

  run() {
    $('title').html('Talabarteria la Guadalupana | Tienda en línea');
    marssoft.asyn.post({ class: 'login', method: 'getResources', public: 'true' }, (r) => {
      let categories = '';
      $.each(r.categories, (i, e) => {
        categories += `<li class="go_category" data-target="${e.sub_id}"><a href="#categorias/${e.sub_id}">${e.sub_name}</a></li>`;
      });
      //hide-on-med-and-down
      $('body').html(`
        <div class="navbar-fixed">
          <nav class="client_navbar">
            <div class="nav-wrapper">
              <a href="#!" class="public brand-logo"><img src="assets/img/logo_b.png" height="64" /></a>
              <a class="public store_name hide-on-med-and-down">Talabartería la Guadalupana</a>
              <ul class="right">
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Categorías<i class="material-icons right">arrow_drop_down</i></a></li>
                <li class="go_cart"><a><i class="material-icons left">shopping_cart</i> <span class="cart_actual">(${marssoft.in.public.getCartNUmber(r.cart_items)})</span> </a></li>
                <li class="go_call"><a><i class="material-icons left">phone</i></a></li>
              </ul>
            </div>
          </nav>
        </div>
        <ul id="dropdown1" class="dropdown-content">${categories}</ul>
        <div class="main"></div>
        <footer class="page-footer client_navbar">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Talabartería la Guadalupana</h5>
                <p class="grey-text text-lighten-4">
                  Av 1 Nicolás Bravo 86,<br>
                  Coscomatepec de Bravo, Centro,<br>
                  94140 Coscomatepec de Bravo, Ver.
                </p>
                <ul>
                  <li><a class="go_terms grey-text text-lighten-3" href="#!">Términos y condiciones</a></li>
                  <li><a class="go_privacy grey-text text-lighten-3" href="#!">Aviso de privacidad</a></li>
                  <li><a class="go_shipping grey-text text-lighten-3" href="#!">Políticas de envío</a></li>
                </ul>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Redes sociales</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Facebook</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Twitter</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">instagram</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2020 Tienda en línea
            <a class="grey-text text-lighten-4 right" href="#!">Visítenos en mercado libre</a>
            </div>
          </div>
        </footer>
      `);
      // $('.sidenav').sidenav();
      $(".dropdown-trigger").dropdown({
        constrainWidth: false
      });
      if ($_GET.auth != undefined){
        switch($_GET.auth){
          case 'success':
            marssoft.in.public.validate($_GET.id);
          break;
        }
      } else {
        let hash = window.location.hash.substr(1);
        hash = hash.split('/');
        if (hash.length > 1) {
          switch (hash[0]) {
            case 'productos':
              marssoft.in.public.open_product(hash[1]);
              break;
            case 'categorias':
              marssoft.in.public.open_category(hash[1]);
              break;
            default:
              marssoft.in.public.home();
              break;
          }
        } else
          marssoft.in.public.home();
      }
      
    });
  }
}

class Order extends Util {
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents() {
    u.click('.orders_table > tr', (v) => {
      this.selectTable(v, '.order_details');
    });
    u.click('.order_details', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.order_details();
    });
    u.click('.aprove_all', this.aprove_all);
    u.keyup('.ord_shipping_amount', this.ord_shipping_amount);
    u.keyup('.ite_new_units', this.ite_new_units);
    u.click('.order_out_stock', this.order_out_stock);
    u.click('.partial_aprovement', this.partial_aprovement);
    u.click('.send_reminder', this.send_reminder);
    u.click('.send_shipping_code', this.send_shipping_code);
    u.click('.mark_order_as_received', this.mark_order_as_received);
  }

  mark_order_as_received() {
    let order = marssoft.in.principal.subs.order.data;
    let ship = order.ship;
    marssoft.ui.question('Confirmar recibido', `
      <div class="row">
        <div class="col s12">
          Está a punto de a ${order.user.personal.fullname} que su Orden No. ${order.ord_id} ha llegado a su destino.
        </div>
      </div>
    `, 'Informar al cliente que ha recibido el producto', () => {
      marssoft.asyn.post({ class: 'orders', method: 'orderReceived' }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("El cliente ha recibido el correo de enterga de producto con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => {
      $('.ord_shipping_amount').focus();
    });
  }

  send_shipping_code() {
    let order = marssoft.in.principal.subs.order.data;
    let ship = order.ship;
    marssoft.ui.question('Confirmar envío', `
      <div class="row">
        <div class="col s12">
          Está a punto de enviar los datos de envió de la compra No ${order.ord_id} para ${order.user.personal.fullname}. Al realizar esta acción, el cliente conocerá la empresa y el número de rastreo de su pedido.
        </div>
        <div class="col s12">
          <table>
            <tr>
              <th>Calle</th>
              <td>${ship.shi_street}</td>
            </tr>
            <tr>
              <th>Colonia</th>
              <td>${ship.shi_suburd} No Ext ${ship.shi_external == '' ? 'Sin número externo' : ship.shi_external}, No Int ${ship.shi_internal == '' ? 'Sin número externo' : ship.shi_internal}</td>
            </tr>
            <tr>
              <th>Código postal</th>
              <td>${ship.shi_postal}</td>
            </tr>
            <tr>
              <th>Ciudad, Estado</th>
              <td>${ship.shi_city}, ${ship.shi_state}</td>
            </tr>
            <tr>
              <th>Referencias adicionales</th>
              <td>${ship.shi_additional}</td>
            </tr>
            <tr>
              <th>Teléfono del cliente</th>
              <td>${order.user.personal.per_phone}</td>
            </tr>
            <tr>
              <th>Correo del cliente</th>
              <td>${order.user.personal.per_email}</td>
            </tr>
            <tr>
              <th>Subtotal a pagar por el cliente</th>
              <td class="right-align">${order.ord_subtotal.formatMoney()}</td>
            </tr>
            <tr>
              <th>Costo del envío previamente establecido</th>
              <td class="right-align">${order.ord_shipping_amount.formatMoney()}</td>
            </tr>
            <tr>
              <th>Total que pagó el cliente (${order.ord_financing} mes${order.ord_financing == '1' ? '' : 'es'} sin intereses)</th>
              <td class="right-align new_total">${order.ord_total.formatMoney()}</td>
            </tr>
            <tr>
              <td class="input-field" colspan="2">
                ${Form.input('ord_shipping_company', 'text', 'Empresa de envío', '', '', '', 100)}
              </td>
            </tr>
            <tr>
              <td class="input-field" colspan="2">
                ${Form.input('ord_shipping_code', 'text', 'No. de rastreo', '', '', '', 100)}
              </td>
            </tr>
          </table>
        </div>
      </div>
    `, 'Enviar datos de envío', () => {
      if ($('.ord_shipping_company').val() == '' || $('.ord_shipping_code').val() == '') {
        Core.toast('Los datos de envío son obligatorios', 'red');
        return false;
      }
      marssoft.in.principal.subs.order.data.ord_shipping_company = $('.ord_shipping_company').val();
      marssoft.in.principal.subs.order.data.ord_shipping_code = $('.ord_shipping_code').val();
      $.each(marssoft.in.principal.subs.order.data.items, (i, e) => {
        delete marssoft.in.principal.subs.order.data.items[i].product;
      });
      delete marssoft.in.principal.subs.order.data.ship;
      delete marssoft.in.principal.subs.order.data.user;
      marssoft.asyn.post({ class: 'orders', method: 'sendShippingData', data: marssoft.in.principal.subs.order.data }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("Datos de envío enviados al cliente con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => {
      $('.ord_shipping_amount').focus();
    });
  }

  send_reminder() {
    marssoft.asyn.post({ class: 'orders', method: 'sendReminder' }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast("Recordatorio enviado al cliente con éxito.", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.activeClass();
          });
          break;
        default:
          Core.toast("Error inesperado, intente más tarde.", "red");
          break;
      }
    });
  }

  ite_new_units(v) {
    let new_value = v.val().toSimpleNumber();
    let ite_id = v.attr('data-item');

    let total = 0;
    let month = marssoft.in.principal.subs.order.data.ord_financing;
    $.each(marssoft.in.principal.subs.order.data.items, (i, e) => {
      if (e.ite_id == ite_id) {
        e.ite_units = new_value;
      } else {
        e.ite_units = e.ite_units.toSimpleNumber();
      }
      e['ite_price_' + month] = e['ite_price_' + month].toSimpleNumber();
      let subtotal = e.ite_units * e['ite_price_' + month];
      e.ite_amount = subtotal;
      total += subtotal;
    });
    marssoft.in.principal.subs.order.data.ord_subtotal = total;
    $('.new_subtotal').html(total.formatMoney());
    $('.ord_shipping_amount').keyup();
  }

  ord_shipping_amount(v) {
    let val = v.val().toSimpleNumber();
    let ord_subtotal = marssoft.in.principal.subs.order.data.ord_subtotal.toSimpleNumber();
    let montly = $('.monthly').attr('data-montly').toSimpleNumber();
    let sentval = (val * montly) + 2.50;
    sentval = sentval - val;
    sentval = sentval * 1.16;
    let total = (ord_subtotal + val + sentval).toSimpleNumber();
    marssoft.in.principal.subs.order.data.ord_shipping_amount = (val + sentval).toSimpleNumber();
    marssoft.in.principal.subs.order.data.ord_total = total;
    marssoft.in.principal.subs.order.data.ord_montly_payment = (total / marssoft.in.principal.subs.order.data.ord_financing.toSimpleNumber()).toSimpleNumber();
    $('.new_total').html(total.formatMoney());
  }

  aprove_all() {
    let order = marssoft.in.principal.subs.order.data;
    let ship = order.ship;
    marssoft.ui.question('Confirmar acción', `
      <div class="row">
        <div class="col s12">
          Está a punto de formalizar la compra para ${order.user.personal.fullname}. Al realizar esta acción, el cliente podrá realizar la compra. Es necesario agregar el costo de envío del pedido con base en el destino indicado por el cliente.
        </div>
        <div class="col s12">
          <table>
            <tr>
              <th>Calle</th>
              <td>${ship.shi_street}</td>
            </tr>
            <tr>
              <th>Colonia</th>
              <td>${ship.shi_suburd} No Ext ${ship.shi_external == '' ? 'Sin número externo' : ship.shi_external}, No Int ${ship.shi_internal == '' ? 'Sin número externo' : ship.shi_internal}</td>
            </tr>
            <tr>
              <th>Código postal</th>
              <td>${ship.shi_postal}</td>
            </tr>
            <tr>
              <th>Ciudad, Estado</th>
              <td>${ship.shi_city}, ${ship.shi_state}</td>
            </tr>
            <tr>
              <th>Referencias adicionales</th>
              <td>${ship.shi_additional}</td>
            </tr>
            <tr>
              <th>Teléfono del cliente</th>
              <td>${order.user.personal.per_phone}</td>
            </tr>
            <tr>
              <th>Correo del cliente</th>
              <td>${order.user.personal.per_email}</td>
            </tr>
            <tr>
              <th>Subtotal a pagar por el cliente</th>
              <td class="right-align">${order.ord_subtotal.formatMoney()}</td>
            </tr>
            <tr>
              <td class="input-field" colspan="2">
                ${Form.input('ord_shipping_amount', 'number', 'Costo del envío', '', '', '', 100)}
              </td>
            </tr>
            <tr>
              <th>Total a pagar por el cliente (${order.ord_financing} mes${order.ord_financing == '1' ? '' : 'es'} sin intereses)</th>
              <td class="right-align new_total">${order.ord_total.formatMoney()}</td>
            </tr>
          </table>
        </div>
      </div>
    `, 'Formalizar orden de compra', () => {
      if ($('.ord_shipping_amount').val() == '') {
        Core.toast('El costo del envío es obligatorio, favor de introducirlo', 'red');
        return false;
      }
      $.each(marssoft.in.principal.subs.order.data.items, (i, e) => {
        delete marssoft.in.principal.subs.order.data.items[i].product;
      });
      delete marssoft.in.principal.subs.order.data.ship;
      delete marssoft.in.principal.subs.order.data.user;
      marssoft.asyn.post({ class: 'orders', method: 'formalizeOrder', data: marssoft.in.principal.subs.order.data }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("Orden formalizada con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => {
      $('.ord_shipping_amount').focus();
    });
  }

  order_out_stock() {
    let order = marssoft.in.principal.subs.order.data;
    let ship = order.ship;
    marssoft.ui.question('Confirmar cancelación', `
      <div class="row">
        <div class="col s12">
          Está a punto de cancelar la compra para ${order.user.personal.fullname}. Al realizar esta acción, el cliente NO podrá realizar la compra.
        </div>
      </div>
    `, 'Cancelar orden de compra', () => {
      marssoft.asyn.post({ class: 'orders', method: 'cancelOrden' }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("Orden cancelada con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => {
      $('.ord_shipping_amount').focus();
    });
  }

  partial_aprovement() {
    let order = marssoft.in.principal.subs.order.data;
    let ship = order.ship;
    marssoft.ui.question('Confirmar acción', `
      <div class="row">
        <div class="col s12">
          Está a punto de aprobar una compra parcial para ${order.user.personal.fullname}. Al realizar esta acción, el cliente podrá realizar la compra con base en la disponibilidad actual de su inventario.
        </div>
        <div class="col s12">
          <table>
            <thead>
              <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>El cliente solicita</th>
                <th>Se puede surtir con</th>
              </tr>
            </thead>
            <tbody class="product_list"></tbody>
          </table>
        </div>
        <div class="col s12">
          <table>
            <tr>
              <th>Calle</th>
              <td>${ship.shi_street}</td>
            </tr>
            <tr>
              <th>Colonia</th>
              <td>${ship.shi_suburd} No Ext ${ship.shi_external == '' ? 'Sin número externo' : ship.shi_external}, No Int ${ship.shi_internal == '' ? 'Sin número externo' : ship.shi_internal}</td>
            </tr>
            <tr>
              <th>Código postal</th>
              <td>${ship.shi_postal}</td>
            </tr>
            <tr>
              <th>Ciudad, Estado</th>
              <td>${ship.shi_city}, ${ship.shi_state}</td>
            </tr>
            <tr>
              <th>Referencias adicionales</th>
              <td>${ship.shi_additional}</td>
            </tr>
            <tr>
              <th>Teléfono del cliente</th>
              <td>${order.user.personal.per_phone}</td>
            </tr>
            <tr>
              <th>Correo del cliente</th>
              <td>${order.user.personal.per_email}</td>
            </tr>
            <tr>
              <th>Anterior Subtotal a pagar por el cliente</th>
              <td class="right-align">${order.ord_subtotal.formatMoney()}</td>
            </tr>
            <tr>
              <th>Nuevo Subtotal a pagar por el cliente</th>
              <td class="right-align new_subtotal">${order.ord_subtotal.formatMoney()}</td>
            </tr>
            <tr>
              <td class="input-field" colspan="2">
                ${Form.input('ord_shipping_amount', 'number', 'Costo del envío', '', '', '', 100)}
              </td>
            </tr>
            <tr>
              <th>Total a pagar por el cliente (${order.ord_financing} mes${order.ord_financing == '1' ? '' : 'es'} sin intereses)</th>
              <td class="right-align new_total">${order.ord_total.formatMoney()}</td>
            </tr>
          </table>
        </div>
      </div>
    `, 'Aprobar orden de compra', () => {
      if ($('.ord_shipping_amount').val() == '') {
        Core.toast('El costo del envío es obligatorio, favor de introducirlo', 'red');
        return false;
      }
      marssoft.asyn.post({ class: 'orders', method: 'orderpartialyAproved', data: marssoft.in.principal.subs.order.data }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("Orden completada con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => {
      $.each(order.items, (i, e) => {
        $('.product_list').append(`
          <tr>
            <td>${e.product.pro_code}</td>
            <td>${e.product.pro_name}</td>
            <td>${e.ite_units}</td>
            <td class="input-field">${Form.input('ite_new_units', 'number', 'Unidades', '', '', '', 100, e.ite_units, 'data-item="' + e.ite_id + '"')}</td>
          </tr>
        `);
      });
      M.updateTextFields();
    });
  }

  order_details(v) {
    let id = marssoft.in.principal.DOMAction.attr('data-target');
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.order.run, 'Órdenes'],
      [marssoft.in.principal.subs.order.order_details, 'Detalles de la orden ' + id],
    ]);

    marssoft.asyn.post({ class: 'orders', method: 'single', public: 'true', id: id }, (r) => {
      marssoft.in.principal.subs.order.data = r;
      let buttons = ``;
      switch (r.ord_status) {
        case '1':
          buttons = `
            <div class="col s12 m4">
              ${Form.btn(false, 'waves_effect white_text green', 'aprove_all btn100', 'Aprobar todo', '')}
            </div>
            <div class="col s12 m4">
              ${Form.btn(false, 'waves_effect white_text red', 'order_out_stock btn100', 'Fuera de stock', '')}
            </div>
            <div class="col s12 m4">
              ${Form.btn(false, 'waves_effect white_text orange', 'partial_aprovement btn100', 'Aprobación parcial', '')}
            </div>
          `;
          break;
        case '2':
        case '3':
          buttons = `
          <div class="col s12 offset-m6 m6">
            ${Form.btn(false, 'waves_effect white_text green', 'send_reminder btn100', 'Enviar recordatorio de pago', '')}
          </div>
        `;
          break;
        case '6':
          buttons = `
          <div class="col s12 offset-m8 m4">
            ${Form.btn(false, 'waves_effect white_text green', 'send_shipping_code btn100', 'Enviar datos de envío', '')}
          </div>
        `;
          break;
        case '7':
          buttons = `
          <div class="col s12 offset-m6 m6">
            ${Form.btn(false, 'waves_effect white_text green', 'mark_order_as_received btn100', 'Marcar pedido recibido', '')}
          </div>
        `;
          break;
      }
      let percent = '', pnumber = '';
      switch(r.ord_financing){
        case '1':
          percent = '0.0%';
          pnumber = '1.0290';
        break;
        case '3':
          percent = '5.5%';
          pnumber = '1.084';
        break;
        case '6':
          percent = '7.8%';
          pnumber = '1.107';
        break;
        case '9':
          percent = '10.8%';
          pnumber = '1.137';
        break;
        case '12':
          percent = '13.8%';
          pnumber = '1.167';
        break;
      }
      r.ord_total = r.ord_total.toSimpleNumber();
      r.ord_shipping_amount = r.ord_shipping_amount.toSimpleNumber();
      r.ord_openpay_comission = r.ord_openpay_comission.toSimpleNumber();
      r.ord_openpay_msi = r.ord_openpay_msi.toSimpleNumber();
      r.ord_openpay_taxes = r.ord_openpay_taxes.toSimpleNumber();
      let utilities = r.ord_total - r.ord_shipping_amount - r.ord_openpay_comission - r.ord_openpay_msi - r.ord_openpay_taxes;
      $('.content').html(`
        ${buttons}
        <div class="col s12 ">
          <div class="card">
            <div class="card-content row">
              <div class="col s12">
                <table>
                  <tbody class="general">
                    <tr>
                      <th>Fecha del pedido</th>
                      <td class="right-align">${moment(r.ord_created_at).format('DD MMMM YYYY [a las] HH:mm')}</td>
                    </tr>
                    <tr>
                      <th>Status del pedido</th>
                      <td class="right-align">${r.status}</td>
                    </tr>
                    <tr>
                      <th>Subtotal del pedido</th>
                      <td class="right-align">${r.ord_subtotal.formatMoney()}</td>
                    </tr>
                    <tr>
                      <th>Costo del envío*</th>
                      <td class="right-align">${r.ord_shipping_amount.formatMoney()}</td>
                    </tr>
                    <tr>
                      <th>Total del envío*</th>
                      <td class="right-align">${r.ord_total.formatMoney()}</td>
                    </tr>
                    <tr>
                      <th>Plan de financimiento</th>
                      <td class="right-align monthly" data-montly="${pnumber}">Plan a ${r.ord_financing} mes${r.ord_financing == '1' ? '' : 'es'} sin intereses</td>
                    </tr>
                    <tr class="${r.ord_financing == '1' ? 'hidden' : ''}">
                      <th>El cliente pagará ${r.ord_financing} pagos mensuales de</th>
                      <td class="right-align">${r.ord_montly_payment.formatMoney()}</td>
                    </tr>
                  </tbody>
                  <tbody class="finnantial ${r.openpay.ope_bank == '' ? 'hidden' : ''}">
                    <tr>
                      <th colspan="2" class="center-align">Datos del pago realizado por el cliente</th>
                    </tr>
                    <tr>
                      <th>Fecha en que pagó el cliente</th>
                      <td class="right-align">${moment(r.ord_paid_at).format('DD MMMM YYYY [a las] HH:mm')}</td>
                    </tr>
                    <tr>
                      <th>Forma de pago</th>
                      <td class="right-align">${r.openpay.ope_bank} ${r.openpay.ope_brand} ${r.openpay.ope_credit_type}</td>
                    </tr>
                    <tr>
                      <th class="right-align">Total pagado</th>
                      <td class="right-align">${r.ord_total.formatMoney()}</td>
                    </tr>
                    <tr>
                      <th class="right-align">Menos comisión (2.9% + 2.50)</th>
                      <td class="right-align">${r.ord_openpay_comission.formatMoney()}</td>
                    </tr>
                    <tr>
                      <th class="right-align">Subtotal </th>
                      <td class="right-align">${(r.ord_total.toSimpleNumber() - r.ord_openpay_comission.toSimpleNumber()).formatMoney()}</td>
                    </tr>

                    <tr>
                      <th class="right-align">Menos envío</th>
                      <td class="right-align">${r.ord_shipping_amount.formatMoney()}</td>
                    </tr>
                    
                    <tr>
                      <th class="right-align">Menos comisión por MSI (${percent})</th>
                      <td class="right-align">${r.ord_openpay_msi.formatMoney()}</td>
                    </tr>
                    <tr>
                      <th class="right-align">Menos I.V.A. de comisión y MSI</th>
                      <td class="right-align">${r.ord_openpay_taxes.formatMoney()}</td>
                    </tr>
                    <tr>
                      <th class="right-align">Utilidad de compra</th>
                      <td class="right-align">${utilities.formatMoney()}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col s12 center-align">
                <h6>Lista de productos solicitados</h6>
              </div>
              <div class="col s12 table_place"></div>
              <div class="col s12 center-align">
                <h6>Lista de mensajes</h6>
              </div>
              <div class="col s12 message_content"></div>
              <div class="col s12 message_buttons">
                <div class="col s12 m10 input-field">
                  ${Form.input('men_message', 'text', 'Escriba el mensaje para el venvedor', '', '', '', 100)}
                </div>
                <div class="col s12 m2 div_send_message">
                  ${Form.btn(false, 'waves_effect white_text primary_background', 'send_message btn100', '', 'send', 'data-target="'+id+'" data-send="1"')}
                </div>
              </div>
            </div>
          </div>
        </div>
      `);
      $.each(r.messages, (i, e) => {
        marssoft.in.public.addMessage(e, '1');
      });
      let products = [];
      $.each(r.items, (i, e) => {
        products.push({
          'pro_id': e.ite_pro_id,
          'pro_name': e.product.pro_code + ' - ' + e.product.pro_name,
          'pro_status': e.status,
          'pro_unit': e['ite_price_' + r.ord_financing].formatMoney(),
          'pro_items': e.ite_units,
          'pro_amount': e.ite_amount.formatMoney(),
        });
      });
      Table.table('products_client_table', 'pro_id', '.table_place', {
        'pro_name': 'Pedido',
        'pro_status': 'Status',
        'pro_unit': 'Precio unitario',
        'pro_items': 'Unidades',
        'pro_amount': 'Subtotal',
      }, products);
      for (let i = 3; i < 6; i++)
        $('.products_client_table > tr > td:nth-child(' + i + ')').addClass('right-align');
    });
  }

  run() {
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.order.run, 'Órdenes']
    ]);
    $('.content').html(`
      <div class="col s12 offset-m9 m3">
        ${Form.btn(false, 'waves_effect white_text blue', 'order_details btn100 disabled', 'Detalles', '')}
      </div>
      <div class="col s12 ">
        <div class="card">
          <div class="card-content table_place"></div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'orders', method: 'get' }, (r) => {
      $.each(r, (i, e) => {
        r[i].ord_created_at = moment(r[i].ord_created_at).format('DD MMMM YYYY')
        r[i].ord_subtotal = r[i].ord_subtotal.formatMoney();
        r[i].ord_shipping_amount = r[i].ord_shipping_amount.formatMoney();
        r[i].ord_total = r[i].ord_total.formatMoney();
      })
      Table.table('orders_table', 'ord_id', '.table_place', {
        'ord_id': 'No.',
        'client.personal.fullname': 'Cliente',
        'ord_created_at': 'Fecha',
        'ord_subtotal': 'Importe',
        'ord_shipping_amount': 'Envío',
        'ord_total': 'Total',
        'ord_financing': 'Meses',
        'status': 'Status',
      }, r, true, [[0, 'desc']]);
      // for (let i = 4; i < 9; i++)
      //   $('.orders_table > tr > td:nth-child(' + i + ')').addClass('right-align');
    });
  }
}

class Product extends Util {
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents() {
    u.click('.new_product', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_product();
    });
    u.click('.edit_product', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_product();
    });
    u.click('.product_create', (v) => {
      this.product_create(true);
    });
    u.click('.product_edit', (v) => {
      this.product_create(false);
    });
    u.click('.products_table > tr', (v) => {
      this.selectTable(v, '.delete_product, .edit_product');
    });
    u.click('.delete_product', this.delete_product);
    u.click('.add_features', this.add_features);
    u.click('.remove_target', this.remove_target);
  }

  delete_product(v) {
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción', 'Está seguro de eliminar a la subfamilia <b>' + $('tr.active > td:nth-child(2)').html() + '</b>? Esta eliminación será permanente e irreversible', 'Eliminar', () => {
      marssoft.asyn.post({ class: 'products', method: 'delete', id: id }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("Producto eliminado con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => { });
  }

  product_create(v) {
    let vars = ['pro_code', 'pro_sub_id', 'pro_bra_id', 'pro_name', 'pro_descr', 'pro_price_1', 'pro_price_3', 'pro_price_6', 'pro_price_9', 'pro_price_12', 'pro_exists', 'pro_offer_price'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, ['pro_code', 'pro_sub_id', 'pro_name', 'pro_descr', 'pro_price_1', 'pro_exists', 'pro_offer_price'], true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    data.images = Util.getFile('.sec_imagen');
    if (Object.keys(data.images).length < 1) {
      Core.toast("Es necesario agregar al menos 1 fotografía para el producto.", "red");
      return false;
    }
    data.features = [];
    $('.feature').each(function(){
      let tmp = {
        'fea_id' : $(this).attr('data-id'),
        'fea_title' : $(this).children('.div_fea_title').children('input').val(),
        'fea_descr' : $(this).children('.div_fea_descr').children('textarea').val(),
      };
      if (tmp.fea_title != '' && tmp.fea_descr != ''){
        data.features.push(tmp);
      }
    });
    console.log(data);
    marssoft.asyn.post({ class: 'products', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast("Producto guardado con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.product.run;
            marssoft.in.principal.activeClass();
          });
          break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
          break;
      }
    });
  }

  add_features(){
    marssoft.in.principal.subs.product.add_feature();
  }

  remove_target(v){
    v.parent().parent().remove();
  }

  add_feature(id = '0', title = '', descr = ''){
    $('.features').append(`
      <div class="col s12 feature" data-id="${id}">
        <div class="col s12 m1 input-field">
          ${Form.btn(false, 'waves_effect white_text red', 'remove_target btn100 ', 'x', '')}
        </div>
        <div class="col s12 m4 input-field div_fea_title">
          ${Form.input('fea_title', 'text', 'Nombre', '', '', '', 100, title)}
        </div>
        <div class="col s12 m7 input-field div_fea_descr">
          ${Form.area('fea_descr', 'Descripción', 500, '', descr)}
        </div>
      </div>
    `);
    M.updateTextFields();
  }

  management_product() {
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_product'));
    let action = ['', '', ''];
    let product = '';
    if (is_new) {
      action[0] = 'Guardar cambios';
      action[1] = 'product_create';
      action[2] = 'Nuevo producto';
    } else {
      product = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Guardar cambios';
      action[1] = 'product_edit';
      action[2] = 'Editar producto';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.product.management_product;
    marssoft.in.principal.animate(() => {
      let families = [], brands = [];
      marssoft.asyn.post({ class: 'products', method: 'getResources'}, (r) => {
        $.each(r.categories, (i, e) => {
          families.push([e.sub_id, e.sub_name]);
        });
        $.each(r.brands, (i, e) => {
          brands.push([e.sub_id, e.sub_name]);
        });
        marssoft.in.principal.buildBreadcrum([
          [marssoft.in.principal.subs.product.run, 'Productos'],
          [marssoft.in.principal.subs.product.management_product, action[2]],
        ]);
        $('.content').html(`
          <div class="col s12">
            <div class="card-panel">
              <div class="row">
                <div class="col s12">
                  <h6>Datos del producto</h6>
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.input('pro_code', 'text', 'Código del producto', '', '', '', 100)}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.select('pro_sub_id', 'Seleccione categoría', families, '', '', 'searchable="true"')}
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.select('pro_bra_id', 'Seleccione marca', brands, '', '', 'searchable="true"')}
                </div>
                <div class="col s12 m9 input-field">
                  ${Form.input('pro_name', 'text', 'Nombre del producto', '', '', '', 100)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('pro_price_1', 'number', 'Importe normal', '', '', '', 100)}
                </div>
                <div class="col s12 input-field">
                  ${Form.area('pro_descr', 'Descripción del producto', 500)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('pro_price_3', 'number', 'Importe 3MSI', '', '', '', 100)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('pro_price_6', 'number', 'Importe 6MSI', '', '', '', 100)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('pro_price_9', 'number', 'Importe 9MSI', '', '', '', 100)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('pro_price_12', 'number', 'Importe 12MSI', '', '', '', 100)}
                </div>
                <div class="col s12 input-field">
                  ${Form.input('pro_exists', 'number', 'Productos disponibles para venta (existencias)', '', '', '', 100)}
                </div>

                <div class="col s12 m9">
                  <h6>Características del producto</h6>
                </div>

                <div class="col s12 m3 input-field btn_action">
                  ${Form.btn(false, 'waves_effect white_text primary_background', 'add_features btn100 ', 'añadir', 'add')}
                </div>

                <div class="col s12 features"></div>
                <div class="col s12">
                  <h6>Ofertas del producto</h6>
                </div>
                <div class="col s12 m4 input-field">
                  ${Form.input('pro_offer_price', 'number', 'Precio de oferta (opcional)', '', '', '', 100)}
                </div>

                <div class="col s12">
                  <h6>Imágenes del producto</h6>
                </div>
                <div class="col s12 input-field">
                  ${Form.file('sec_imagen', 'Cargar imágenes del producto', 'Se admiten archivos: jpg y png', true)}
                </div>
              </div>
            </div>
          </div>
          <div class="col s12 offset-m8 m4 input-field btn_action">
            ${Form.btn(false, 'waves_effect white_text primary_background', action[1] + ' btn100 ', action[0], 'save')}
          </div>
        `);
        if (!is_new) {
          marssoft.asyn.post({ class: 'products', method: 'single', id: product }, (r) => {
            Form.fnimage('sec_imagen', 5242880, ['png', 'jpg'], () => { });
            if (r.pro_offer_price != '' && r.pro_offer_price != '0') {
              let tmp = r.pro_offer_price;
              r.pro_offer_price = r.pro_price_1;
              r.pro_price_1 = tmp;
            }
            $('.pro_code').val(r.pro_code);
            $('.pro_name').val(r.pro_name);
            $('.pro_descr').val(r.pro_descr);
            $('.pro_price_1').val(r.pro_price_1);
            $('.pro_price_3').val(r.pro_price_3);
            $('.pro_price_6').val(r.pro_price_6);
            $('.pro_price_9').val(r.pro_price_9);
            $('.pro_price_12').val(r.pro_price_12);
            $('.pro_offer_price').val(r.pro_offer_price);
            $('.pro_exists').val(r.pro_exists);
            $('.pro_sub_id').val(r.pro_sub_id);
            $('.pro_bra_id').val(r.pro_bra_id);
            $('.pro_sub_id').formSelect();
            $('.pro_bra_id').formSelect();
            M.textareaAutoResize($('.pro_descr'));
            $.each(r.pictures, (i, e) => {
              Util.fillImage('.sec_imagen', 'resources/docs/' + e.doc_id + '.' + e.doc_extension, e.doc_id);
            });
            $.each(r.features, (i, e) => {
              marssoft.in.principal.subs.product.add_feature(e.fea_id, e.fea_title, e.fea_descr);
            });
            M.updateTextFields();
            M.textareaAutoResize($('textarea'));
          });
        } else {
          Form.fnimage('sec_imagen', 5242880, ['png', 'jpg'], () => { });
          $('.pro_sub_id').formSelect();
          $('.pro_bra_id').formSelect();
        }
      });
    });
  }

  run() {
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.product.run, 'Productos']
    ]);
    $('.content').html(`
      <div class="col s12 offset-m6 m2">
        ${Form.btn(false, 'waves_effect white_text red', 'delete_product btn100 disabled', 'Borrar', 'remove')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_product btn100 disabled', 'Editar', 'edit')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'new_product btn100 ', 'Nuevo', 'add')}
      </div>
      <div class="col s12 ">
        <div class="card">
          <div class="card-content table_place"></div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'products', method: 'get' }, (r) => {
      $.each(r, (i, e) => {
        r[i].pro_price_1 = r[i].pro_price_1.formatMoney();
        r[i].pro_offer_price = (r[i].pro_offer_price == '0' || r[i].pro_offer_price == '') ? 'No hay' : r[i].pro_offer_price.formatMoney();
        // r[i].pro_price_6 = r[i].pro_price_6.formatMoney();
        // r[i].pro_price_9 = r[i].pro_price_9.formatMoney();
        r[i].pro_price_12 = r[i].pro_price_12.formatMoney();
      })
      Table.table('products_table', 'pro_id', '.table_place', {
        'pro_code': 'Código',
        'pro_name': 'Nombre',
        'subfamily.sub_name': 'Categoría',
        'pro_offer_price': 'Precio Oferta',
        'pro_price_1': 'Precio 1MSI',
        // 'pro_price_3': 'Precio 3MSI',
        // 'pro_price_6': 'Precio 6MSI',
        // 'pro_price_9': 'Precio 9MSI',
        'pro_price_12': 'Precio 12MSI',
        'pro_exists': 'Existencias',
      }, r);
      for (let i = 4; i < 9; i++)
        $('.products_table > tr > td:nth-child(' + i + ')').addClass('right-align');
    });
  }
}

class Family extends Util {
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents() {
    u.click('.new_family', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_family();
    });
    u.click('.edit_family', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_family();
    });
    u.click('.family_create', (v) => {
      this.family_create(true);
    });
    u.click('.family_edit', (v) => {
      this.family_create(false);
    });
    u.click('.families_table > tr', (v) => {
      this.selectTable(v, '.delete_family, .edit_family');
    });
    u.click('.delete_family', this.delete_family);
  }

  delete_family(v) {
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción', 'Está seguro de eliminar a la familia <b>' + $('tr.active > td:nth-child(2)').html() + '</b>? Esta eliminación será permanente e irreversible', 'Eliminar', () => {
      marssoft.asyn.post({ class: 'families', method: 'delete', id: id }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("familia eliminada con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => { });
  }

  family_create(v) {
    let vars = ['fam_name'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, vars, true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'families', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast("familia creada con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.family.run;
            marssoft.in.principal.activeClass();
          });
          break;
        default:
          Core.toast("Error inesperado, intente más tarde.", "red");
          break;
      }
    });
  }

  management_family() {
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_family'));
    let action = ['', '', ''];
    let family = '';
    if (is_new) {
      action[0] = 'Crear familia';
      action[1] = 'family_create';
      action[2] = 'Nueva familia';
    } else {
      family = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Editar familia';
      action[1] = 'family_edit';
      action[2] = 'Editar familia';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.family.management_family;
    marssoft.in.principal.animate(() => {
      marssoft.in.principal.buildBreadcrum([
        [marssoft.in.principal.subs.family.run, 'Familias'],
        [marssoft.in.principal.subs.family.management_family, action[2]],
      ]);
      $('.content').html(`
        <div class="col s12">
          <div class="card-panel">
            <div class="row">
              <div class="col s12">
                <h6>Datos de la familia</h6>
              </div>
              <div class="col s12 input-field">
                ${Form.input('fam_name', 'text', 'Nombre de la familia', '', '', '', 100)}
              </div>
            </div>
          </div>
        </div>
        <div class="col s12 offset-m8 m4 input-field btn_action">
          ${Form.btn(false, 'waves_effect white_text primary_background', action[1] + ' btn100 ', action[0], 'save')}
        </div>
      `);
      if (!is_new) {
        marssoft.asyn.post({ class: 'families', method: 'single', id: family }, (r) => {
          $('.fam_name').val(r.fam_name);
          M.updateTextFields();
        });
      }
    });
  }

  run() {
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.family.run, 'Familias']
    ]);
    $('.content').html(`
      <div class="col s12 offset-m6 m2">
        ${Form.btn(false, 'waves_effect white_text red', 'delete_family btn100 disabled', 'Borrar', 'remove')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_family btn100 disabled', 'Editar', 'edit')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'new_family btn100 ', 'Nuevo', 'add')}
      </div>
      <div class="col s12 ">
        <div class="card">
          <div class="card-content table_place"></div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'families', method: 'get' }, (r) => {
      Table.table('families_table', 'fam_id', '.table_place', {
        'fam_id': 'Id',
        'fam_name': 'Nombre de la familia'
      }, r);
    });
  }
}

class Subfamily extends Util {
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents() {
    u.click('.new_subfamily', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_subfamily();
    });
    u.click('.edit_subfamily', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_subfamily();
    });
    u.click('.subfamily_create', (v) => {
      this.subfamily_create(true);
    });
    u.click('.subfamily_edit', (v) => {
      this.subfamily_create(false);
    });
    u.click('.subfamilies_table > tr', (v) => {
      this.selectTable(v, '.delete_subfamily, .edit_subfamily');
    });
    u.click('.delete_subfamily', this.delete_subfamily);
  }

  delete_subfamily(v) {
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción', 'Está seguro de eliminar a la subfamilia <b>' + $('tr.active > td:nth-child(2)').html() + '</b>? Esta eliminación será permanente e irreversible', 'Eliminar', () => {
      marssoft.asyn.post({ class: 'subfamilies', method: 'delete', id: id }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("subfamilia eliminada con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => { });
  }

  subfamily_create(v) {
    let vars = ['sub_name', 'sub_fam_id'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, vars, true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'subfamilies', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast("subfamilia creada con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.subfamily.run;
            marssoft.in.principal.activeClass();
          });
          break;
        case 'family_not_found':
          Core.toast("La familia padre no existe, intente de nuevo por favor.", "red");
          break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
          break;
      }
    });
  }

  management_subfamily() {
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_subfamily'));
    let action = ['', '', ''];
    let subfamily = '';
    if (is_new) {
      action[0] = 'Crear subfamilia';
      action[1] = 'subfamily_create';
      action[2] = 'Nueva familia';
    } else {
      subfamily = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Editar subfamilia';
      action[1] = 'subfamily_edit';
      action[2] = 'Editar subfamilia';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.subfamily.management_subfamily;
    marssoft.in.principal.animate(() => {
      let families = []
      marssoft.asyn.post({ class: 'families', method: 'get' }, (r) => {
        $.each(r, (i, e) => {
          families.push([e.fam_id, e.fam_name]);
        });
        marssoft.in.principal.buildBreadcrum([
          [marssoft.in.principal.subs.subfamily.run, 'Subfamilias'],
          [marssoft.in.principal.subs.subfamily.management_subfamily, action[2]],
        ]);
        $('.content').html(`
          <div class="col s12">
            <div class="card-panel">
              <div class="row">
                <div class="col s12">
                  <h6>Datos de la subfamilia</h6>
                </div>
                <div class="col s12 m6 input-field">
                  ${Form.select('sub_fam_id', 'Seleccione una familia padre', families, '', '', 'searchable="true"')}
                </div>
                <div class="col s12 m6 input-field">
                  ${Form.input('sub_name', 'text', 'Nombre de la subfamilia', '', '', '', 100)}
                </div>
              </div>
            </div>
          </div>
          <div class="col s12 offset-m8 m4 input-field btn_action">
            ${Form.btn(false, 'waves_effect white_text primary_background', action[1] + ' btn100 ', action[0], 'save')}
          </div>
        `);
        if (!is_new) {
          marssoft.asyn.post({ class: 'subfamilies', method: 'single', id: subfamily }, (r) => {
            $('.sub_name').val(r.sub_name);
            $('.sub_fam_id').val(r.sub_fam_id);
            $('.sub_fam_id').formSelect();
            M.updateTextFields();
          });
        } else {
          $('.sub_fam_id').formSelect();
        }
      });
    });
  }

  run() {
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.subfamily.run, 'Subfamilias']
    ]);
    $('.content').html(`
      <div class="col s12 offset-m6 m2">
        ${Form.btn(false, 'waves_effect white_text red', 'delete_subfamily btn100 disabled', 'Borrar', 'remove')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_subfamily btn100 disabled', 'Editar', 'edit')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'new_subfamily btn100 ', 'Nuevo', 'add')}
      </div>
      <div class="col s12 ">
        <div class="card">
          <div class="card-content table_place"></div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'subfamilies', method: 'get' }, (r) => {
      Table.table('subfamilies_table', 'sub_id', '.table_place', {
        'family.fam_name': 'Familia',
        'sub_name': 'Subfamilia'
      }, r);
    });
  }
}

class Supplier extends Util {
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents() {
    u.click('.new_supplier', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_supplier();
    });
    u.click('.edit_supplier', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_supplier();
    });
    u.click('.supplier_create', (v) => {
      this.supplier_create(true);
    });
    u.click('.supplier_edit', (v) => {
      this.supplier_create(false);
    });
    u.click('.suppliers_table > tr', (v) => {
      this.selectTable(v, '.delete_supplier, .edit_supplier');
    });
    u.click('.delete_supplier', this.delete_supplier);
  }

  delete_supplier(v) {
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción', 'Está seguro de eliminar al provedor <b>' + $('tr.active > td:nth-child(1)').html() + '</b>? Esta eliminación será permanente e irreversible', 'Eliminar', () => {
      marssoft.asyn.post({ class: 'suppliers', method: 'delete', id: id }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("provedor eliminado con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => { });
  }

  supplier_create(v) {
    let vars = ['sup_supplier_name', 'sup_contact_name', 'sup_social_reason', 'sup_address', 'sup_rfc', 'sup_bank_info', 'sup_phone_home_1', 'sup_phone_home_2', 'sup_phone_cell', 'sup_fax', 'sup_email'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, ['sup_supplier_name', 'sup_rfc', 'sup_phone_home_1', 'sup_email', 'sup_contact_name'], true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'suppliers', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast("Usuario creado con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.supplier.run;
            marssoft.in.principal.activeClass();
          });
          break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
          break;
      }
    });
  }

  management_supplier() {
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_supplier'));
    let action = ['', '', ''];
    let supplier = '';
    if (is_new) {
      action[0] = 'Crear proveedor';
      action[1] = 'supplier_create';
      action[2] = 'Nuevo proveedor';
    } else {
      supplier = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Editar proveedor';
      action[1] = 'supplier_edit';
      action[2] = 'Editar proveedor';
    }
    let turn = [], services = [];
    marssoft.asyn.post({ class: 'suppliers', method: 'loadResources' }, (r) => {
      $.each(r.turns, (i, e) => {
        turn.push([e.sub_id, e.sub_name]);
      });
      $.each(r.services, (i, e) => {
        services.push([e.sub_id, e.sub_name]);
      });
      console.log(turn);
      marssoft.in.principal.activeClass = marssoft.in.principal.subs.supplier.management_supplier;
      marssoft.in.principal.animate(() => {
        marssoft.in.principal.buildBreadcrum([
          [marssoft.in.principal.subs.supplier.run, 'Proveedores'],
          [marssoft.in.principal.subs.supplier.management_supplier, action[2]],
        ]);
        $('.content').html(`
          <div class="col s12">
            <div class="card-panel">
              <div class="row">
                <div class="col s12">
                  <h6>Datos del proveedor</h6>
                </div>
                <div class="col s12 input-field">
                  ${Form.input('sup_supplier_name', 'text', 'Nombre del proveedor', '', '', '', 200)}
                </div>
                <div class="col s12"></div>
                <div class="col s12 m4 input-field">
                  ${Form.input('sup_contact_name', 'text', 'Nombre del contacto', '', '', '', 200)}
                </div>
                <div class="col s12 m8 input-field">
                  ${Form.input('sup_social_reason', 'text', 'Razón social', '', '', '', 200)}
                </div>
                <div class="col s12"></div>
                <div class="col s12 input-field">
                  ${Form.input('sup_address', 'text', 'Dirección', '', '', '', 200)}
                </div>
                <div class="col s12"></div>
                <div class="col s12 m4 input-field">
                  ${Form.input('sup_rfc', 'text', 'RFC', '', '', '', 15)}
                </div>
                <div class="col s12 m8 input-field">
                  ${Form.input('sup_bank_info', 'text', 'Información bancaria', '', '', '', 200)}
                </div>
                <div class="col s12"></div>
                <div class="col s12 m3 input-field">
                  ${Form.input('sup_phone_home_1', 'text', 'Teléfono principal', '', '', '', 16)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('sup_phone_home_2', 'text', 'Teléfono alterno', '', '', '', 16)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('sup_phone_cell', 'text', 'Teléfono celular', '', '', '', 16)}
                </div>
                <div class="col s12 m3 input-field">
                  ${Form.input('sup_fax', 'text', 'Fax', '', '', '', 16)}
                </div>
                <div class="col s12"></div>
                <div class="col s12 input-field">
                  ${Form.input('sup_email', 'text', 'Correo electrónico', '', '', '', 200)}
                </div>
              </div>
            </div>
          </div>
          <div class="col s12 offset-m8 m4 input-field btn_action">
            ${Form.btn(false, 'waves_effect white_text primary_background', action[1] + ' btn100 ', action[0], 'save')}
          </div>
        `);
        if (!is_new) {
          marssoft.asyn.post({ class: 'suppliers', method: 'single', id: supplier }, (r) => {
            $('.sup_supplier_name').val(r.sup_supplier_name);
            $('.sup_contact_name').val(r.sup_contact_name);
            $('.sup_social_reason').val(r.sup_social_reason);
            $('.sup_address').val(r.sup_address);
            $('.sup_rfc').val(r.sup_rfc);
            $('.sup_bank_info').val(r.sup_bank_info);
            $('.sup_phone_home_1').val(r.sup_phone_home_1);
            $('.sup_phone_home_2').val(r.sup_phone_home_2);
            $('.sup_phone_cell').val(r.sup_phone_cell);
            $('.sup_fax').val(r.sup_fax);
            $('.sup_email').val(r.sup_email);
            $('.sup_turn').formSelect();
            $('.sup_service').formSelect();
            $('input').characterCounter();
            M.updateTextFields();
          });
        } else {
          $('input').characterCounter();
          $('.sup_turn').formSelect();
          $('.sup_service').formSelect();
        }
      });
    });
  }

  run() {
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.supplier.run, 'Proveedores']
    ]);
    $('.content').html(`
      <div class="col s12 offset-m6 m2">
        ${Form.btn(false, 'waves_effect white_text red', 'delete_supplier btn100 disabled', 'Borrar', 'remove')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_supplier btn100 disabled', 'Editar', 'edit')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'new_supplier btn100 ', 'Nuevo', 'add')}
      </div>
      <div class="col s12 ">
        <div class="card">
          <div class="card-content table_place"></div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'suppliers', method: 'get' }, (r) => {
      Table.table('suppliers_table', 'sup_id', '.table_place', {
        'sup_supplier_name': 'Nombre',
        'sup_contact_name': 'Contacto',
        'sup_phone_home_1': 'Teléfono',
        'sup_email': 'Correo electrónico'
      }, r);
    });
  }
}

class Client extends Util {
  constructor() {
    super();
    this.clickEvents();
  }

  clickEvents() {
    u.click('.new_client', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_client();
    });
    u.click('.edit_client', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_client();
    });
    u.click('.new_service', (v) => {
      marssoft.in.principal.DOMAction3 = v;
      this.management_service();
    });
    u.click('.edit_service', (v) => {
      marssoft.in.principal.DOMAction3 = v;
      this.management_service();
    });
    u.click('.client_create', (v) => {
      this.client_create(true);
    });
    u.click('.client_edit', (v) => {
      this.client_create(false);
    });
    u.click('.service_create', (v) => {
      this.service_create(true);
    });
    u.click('.service_edit', (v) => {
      this.service_create(false);
    });
    u.click('.clients_table > tr', (v) => {
      this.selectTable(v, '.delete_client, .edit_client');
    });
    u.click('.delete_client', this.delete_client);
  }

  delete_client(v) {
    let id = v.attr('data-target');
    marssoft.ui.question('Confirmar acción', 'Está seguro de eliminar al cliente <b>' + $('tr.active > td:nth-child(2)').html() + '</b>? Esta eliminación será permanente e irreversible', 'Eliminar', () => {
      marssoft.asyn.post({ class: 'clients', method: 'delete', id: id }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("cliente eliminado con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => { });
  }

  client_create(v) {
    let vars = ['cli_first_name', 'cli_last_name', 'cli_sur_name', 'cli_gender', 'cli_birthday', 'cli_address', 'cli_postal_code', 'cli_colony', 'cli_city', 'cli_state', 'cli_country', 'cli_social_reason', 'cli_rfc', 'cli_phone', 'cli_email'];
    let data = z.gValues(vars);
    if (!z.mandatory(data, ['cli_first_name', 'cli_last_name', 'cli_sur_name', 'cli_social_reason', 'cli_rfc', 'cli_phone', 'cli_email'], true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    marssoft.asyn.post({ class: 'clients', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast("cliente creado con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.client.run;
            marssoft.in.principal.activeClass();
          });
          break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
          break;
      }
    });
  }

  management_client() {
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_client'));
    let action = ['', '', ''];
    let client = '';
    if (is_new) {
      action[0] = 'Crear cliente';
      action[1] = 'client_create';
      action[2] = 'Nuevo cliente';
    } else {
      client = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Editar cliente';
      action[1] = 'client_edit';
      action[2] = 'Editar cliente';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.client.management_client;
    marssoft.in.principal.animate(() => {
      marssoft.in.principal.buildBreadcrum([
        [marssoft.in.principal.subs.client.run, 'Clientes'],
        [marssoft.in.principal.subs.client.management_client, action[2]],
      ]);
      $('.content').html(`
        <div class="col s12">
          <div class="card-panel">
            <div class="row">
              <div class="col s12">
                <h6>Datos del cliente</h6>
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_first_name', 'text', 'Nombre(s)', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_last_name', 'text', 'Apellido paterno', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_sur_name', 'text', 'Apellido materno', '', '', '', 100)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m3 input-field">
                ${Form.select('cli_gender', 'Género', [[1, 'Mujer'], [2, 'Hombre']])}
              </div>
              <div class="col s12 m3 input-field">
                ${Form.input('cli_birthday', 'text', 'Fecha de nacimiento', '', '', '', 200)}
              </div>
              <div class="col s12 m6 input-field">
                ${Form.input('cli_address', 'text', 'Dirección', '', '', '', 200)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_postal_code', 'number', 'Código postal', '', '', '', 5)}
              </div>
              <div class="col s12 m8 input-field">
                ${Form.input('cli_colony', 'text', 'Colonia', '', '', '', 200)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_city', 'text', 'Ciudad', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_state', 'text', 'Estado', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_country', 'text', 'País', '', '', '', 50)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m8 input-field">
                ${Form.input('cli_social_reason', 'text', 'Razón social', '', '', '', 200)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_rfc', 'text', 'RFC', '', '', '', 15)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m4 input-field">
                ${Form.input('cli_phone', 'number', 'Teléfono', '', '', '', 10)}
              </div>
              <div class="col s12 m8 input-field">
                ${Form.input('cli_email', 'text', 'Correo electrónico', '', '', '', 200)}
              </div>
            </div>
          </div>
        </div>
        <div class="col s12 offset-m8 m4 input-field btn_action">
          ${Form.btn(false, 'waves_effect white_text primary_background', action[1] + ' btn100 ', action[0], 'save')}
        </div>
      `);
      if (!is_new) {
        marssoft.asyn.post({ class: 'clients', method: 'single', id: client }, (r) => {
          $('.cli_first_name').val(r.cli_first_name);
          $('.cli_last_name').val(r.cli_last_name);
          $('.cli_sur_name').val(r.cli_sur_name);
          $('.cli_address').val(r.cli_address);
          $('.cli_postal_code').val(r.cli_postal_code);
          $('.cli_colony').val(r.cli_colony);
          $('.cli_city').val(r.cli_city);
          $('.cli_state').val(r.cli_state);
          $('.cli_country').val(r.cli_country);
          $('.cli_social_reason').val(r.cli_social_reason);
          $('.cli_rfc').val(r.cli_rfc);
          $('.cli_email').val(r.cli_email);
          $('.cli_birthday').val(r.cli_birthday);
          $('.cli_gender').val(r.cli_gender);
          $('.cli_phone').val(r.cli_phone);
          marssoft.fecha.Dtpicker('.cli_birthday');
          $('.cli_gender').formSelect();
          $('input').characterCounter();
          M.updateTextFields();
        });
      } else {
        $('input').characterCounter();
        marssoft.fecha.Dtpicker('.cli_birthday');
        $('.cli_gender').formSelect();
      }
    });
  }

  run() {
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.client.run, 'Clientes']
    ]);
    $('.content').html(`
      <div class="col s12 m3 offset-m3">
        ${Form.btn(false, 'waves_effect white_text red', 'delete_client btn100 disabled', 'Borrar', 'remove')}
      </div>
      <div class="col s12 m3">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_client btn100 disabled', 'Editar', 'edit')}
      </div>
      <div class="col s12 m3">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'new_client btn100 ', 'Nuevo', 'add')}
      </div>
      <div class="col s12">
        <div class="card">
          <div class="card-content table_place"></div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'clients', method: 'get' }, (r) => {
      Table.table('clients_table', 'cli_id', '.table_place', {
        'fullname': 'Nombre',
        'cli_social_reason': 'Razón social',
        'cli_rfc': 'RFC',
        'cli_email': 'Correo',
        'cli_phone': 'Teléfono'
      }, r);
    });
  }
}

class User extends Util {
  constructor() {
    super();
    this.clickEvents();
  }
  clickEvents() {
    u.click('.new_user', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_user();
    });
    u.click('.edit_user', (v) => {
      marssoft.in.principal.DOMAction = v;
      this.management_user();
    });
    u.click('.user_create', (v) => {
      this.user_create(true);
    });
    u.click('.user_edit', (v) => {
      this.user_create(false);
    });
    u.click('.users_table > tr', (v) => {
      this.selectTable(v, '.delete_user, .edit_user');
    });
    u.click('.delete_user', this.delete_user);
  }
  delete_user(v) {
    let user = v.attr('data-target');
    marssoft.ui.question('Confirmar acción', 'Está seguro de eliminar al usuario <b>' + $('tr.active > td:nth-child(2)').html() + '</b>? Esta eliminación será permanente e irreversible', 'Eliminar', () => {
      marssoft.asyn.post({ class: 'users', method: 'delete', user: user }, (r) => {
        switch (r.response) {
          case 'true':
            Core.toast("Usuario eliminado con éxito.", "green");
            marssoft.in.principal.animate(() => {
              marssoft.in.principal.activeClass();
            });
            break;
          default:
            Core.toast("Error inesperado, intente más tarde.", "red");
            break;
        }
      });
    }, () => { });
  }
  user_create(v) {
    let vars = ['per_firstname', 'per_lastname', 'per_surname', 'per_birthday', 'per_gender', 'per_phone', 'per_email', 'use_id', 'use_profile'];
    if (v || $('.use_password').val() != '') {
      vars.push('use_password');
      vars.push('confirm_password');
    }
    let data = z.gValues(vars);
    if (!z.mandatory(data, vars, true)) {
      Core.toast("Los datos en rojo son obligatorios", "red");
      return false;
    }
    if (v || $('.use_password').val() != '') {
      if (data.use_password != data.confirm_password) {
        Core.toast("Las constraseñas no coinciden", "red");
        return false;
      }
    }
    data = {
      personals: {
        per_firstname: data.per_firstname,
        per_lastname: data.per_lastname,
        per_surname: data.per_surname,
        per_email: data.per_email,
        per_birthday: data.per_birthday,
        per_gender: data.per_gender,
        per_phone: data.per_phone
      },
      users: {
        use_id: data.use_id,
        use_profile: data.use_profile,
        use_password: data.use_password
      }
    }
    marssoft.asyn.post({ class: 'users', method: ((v) ? 'create' : 'update'), data: data }, (r) => {
      switch (r.response) {
        case 'true':
          Core.toast("Usuario creado con éxito", "green");
          marssoft.in.principal.animate(() => {
            marssoft.in.principal.DOMAction = '';
            marssoft.in.principal.activeClass = marssoft.in.principal.subs.user.run;
            marssoft.in.principal.activeClass();
          });
          break;
        case 'user_exists':
          Core.toast("El nombre de usuario ya se encuentra ocupado. Intente con otro", "red");
          $('.use_id').val('');
          break;
        case 'Unexpected_error':
          Core.toast("Error inesperado, intente más tarde.", "red");
          break;
      }
    });
  }
  management_user() {
    let is_new = (marssoft.in.principal.DOMAction.hasClass('new_user'));
    let action = ['', '', ''];
    let user = '';
    if (is_new) {
      action[0] = 'Crear usuario';
      action[1] = 'user_create';
      action[2] = 'Nuevo usuario';
    } else {
      user = marssoft.in.principal.DOMAction.attr('data-target');
      action[0] = 'Editar usuario';
      action[1] = 'user_edit';
      action[2] = 'Editar usuario';
    }
    marssoft.in.principal.activeClass = marssoft.in.principal.subs.user.management_user;
    marssoft.in.principal.animate(() => {
      marssoft.in.principal.buildBreadcrum([
        [marssoft.in.principal.subs.user.run, 'Usuarios'],
        [marssoft.in.principal.subs.user.management_user, action[2]],
      ]);
      $('.content').html(`
        <div class="col s12">
          <div class="card-panel">
            <div class="row">
              <div class="col s12">
                <h6>Datos personales</h6>
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('per_firstname', 'text', 'Nombre(s)', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('per_lastname', 'text', 'Apellido paterno', '', '', '', 100)}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('per_surname', 'text', 'Apellido materno', '', '', '', 100)}
              </div>
              <div class="col s12"></div>
              <div class="col s12 m4 input-field">
                ${Form.input('per_birthday', 'text', 'Fecha de nacimiento', '')}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.select('per_gender', 'Género', [[1, 'Mujer'], [2, 'Hombre']])}
              </div>
              <div class="col s12 m4 input-field">
                ${Form.input('per_phone', 'number', 'Teléfono a 10 dígitos', '', '', '', 10)}
              </div>
              <div class="col s12 input-field">
                ${Form.input('per_email', 'email', 'Correo electrónico', '', '', '', 150)}
              </div>
            </div>
          </div>
        </div>
        <div class="col s12">
          <div class="card-panel">
            <div class="row">
              <div class="col s12">
                <h6>Datos de inicio de sesión</h6>
              </div>
              <div class="col s12 m6 input-field">
                ${Form.input('use_id', 'text', 'Nombre de usuario', '', '', '', 30, '', 'autocomplete="off"')}
              </div>
              <div class="col s12 m3 input-field">
                ${Form.input('use_password', 'password', 'Contraseña', '', '', '', 30, '', 'autocomplete="new-password"')}
              </div>
              <div class="col s12 m3 input-field">
                ${Form.input('confirm_password', 'password', 'Confirmar contraseña', '', '', '', 30, '', 'autocomplete="new-password"')}
              </div>
              <div class="col s12"></div>
              <div class="col s12 input-field">
                ${Form.select('use_profile', 'Nivel de acceso', [[1, 'Administrador'], [2, 'Supervisor']])}
              </div>
            </div>
          </div>
        </div>
        <div class="col s12 m4 offset-m8 input-field btn_action">
          ${Form.btn(false, 'waves_effect white_text primary_background', action[1] + ' btn100 ', action[0], 'save')}
        </div>
      `);
      if (!is_new) {
        marssoft.asyn.post({ class: 'users', method: 'single', id: user }, (r) => {
          $('.use_id').attr('disabled', 'disabled').val(r.use_id);
          $('.per_firstname').val(r.personal.per_firstname);
          $('.per_lastname').val(r.personal.per_lastname);
          $('.per_surname').val(r.personal.per_surname);
          marssoft.fecha.Dtpicker('.per_birthday');
          $('.per_birthday').val(r.personal.birthday);
          $('.per_gender').val(r.personal.per_gender);
          $('.use_profile').val(r.use_profile);
          $('.per_phone').val(r.personal.per_phone);
          $('.per_email').val(r.personal.per_email);
          $('input').characterCounter();

          $('.per_gender, .use_profile').formSelect();
          M.updateTextFields();
          console.log(r);
        });
      } else {
        $('input').characterCounter();
        marssoft.fecha.Dtpicker('.per_birthday');
        $('.per_gender, .use_profile').formSelect();
      }
    });
  }
  run() {
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.user.run, 'Usuarios']
    ]);
    $('.content').html(`
      <div class="col s12 offset-m6 m2">
        ${Form.btn(false, 'waves_effect white_text red', 'delete_user btn100 disabled', 'Borrar', 'remove')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_user btn100 disabled', 'Editar', 'edit')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'new_user btn100 ', 'Nuevo', 'add')}
      </div>
      <div class="col s12 ">
        <div class="card">
          <div class="card-content table_place"></div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'users', method: 'get', exclude: ['3'] }, (r) => {
      Table.table('users_table', 'use_id', '.table_place', {
        'use_id': 'Usuario',
        'personal.fullname': 'Nombre',
        'profile': 'Privilegios',
        'personal.per_email': 'Correo',
        'personal.per_phone': 'Teléfono'
      }, r);
    });
  }
  run_2() {
    marssoft.in.principal.buildBreadcrum([
      [marssoft.in.principal.subs.user.run_2, 'Clientes de la tienda']
    ]);
    $('.content').html(`
      <div class="col s12 offset-m8 m2">
        ${Form.btn(false, 'waves_effect white_text red', 'delete_user btn100 disabled', 'Borrar', 'remove')}
      </div>
      <div class="col s12 m2 hidden">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'edit_user btn100 disabled', 'Editar', 'edit')}
      </div>
      <div class="col s12 m2">
        ${Form.btn(false, 'waves_effect white_text primary_background', 'new_user btn100 ', 'Nuevo', 'add')}
      </div>
      <div class="col s12 ">
        <div class="card">
          <div class="card-content table_place"></div>
        </div>
      </div>
    `);
    marssoft.asyn.post({ class: 'users', method: 'get', exclude: ['0, 1, 2'] }, (r) => {
      Table.table('users_table', 'use_id', '.table_place', {
        'use_id': 'Usuario',
        'personal.fullname': 'Nombre',
        'personal.per_email': 'Correo',
        'personal.per_phone': 'Teléfono',
        'use_last_login': 'Último acceso'
      }, r);
    });
  }
}

class Login {
  constructor() {
    this.clickEvents();
  }
  clickEvents() {
    u.click('.login_action', this.login_action);
  }
  login_action(v) {
    let vars = ['username', 'password'];
    let data = z.gValues(vars);
    z.mandatory(data, vars, true);
    if (!z.mandatory(data, vars)) {
      Core.toast("Escriba su nombre de usuario y contraseña para continuar", "red");
      return false;
    }
    v.addClass('disabled').html('Iniciando sesión...');
    marssoft.asyn.post({ class: 'login', method: 'grant', data: data }, (r) => {
      v.removeClass('disabled').html('Iniciar sesión');
      switch (r.response) {
        case 'denied':
          Core.toast("Acceso denegado, intente nuevamente", "red");
          break;
        case 'locked':
          Core.toast("Cuenta bloqueada por exceso de fallos, podrá iniciar sesión " + moment(r.time_at).fromNow(), "red");
          break;
        case 'true':
          Core.toast("Hola " + $.blurhouse.decode($.cookie('per_firstname')) + ", iniciando sesión...", "green");
          marssoft.out.login.exit(() => {
            marssoft.in.principal.run();
          });
          break;
      }
    });

  }
  run() {
    $('title').html(marssoft.data.title + ' - Inicio de sesión');
    $('.app').html(`
      <div class="login scale-transition scale-out">
        <div class="card z-depth-5">
          <div class="system_img center-align"><img src="assets/img/logo_b.png" height="90"></div>
          <div class="system_title primary_color">${marssoft.data.title}</div>
          <div class="card-content row">
            <div class="input-field col s12">
              <input class="username" id="username" type="text" class="validate">
              <label for="username">Nombre de usuario</label>
            </div>
            <div class="input-field col s12">
              <input class="password" id="password" type="password" class="validate">
              <label for="password">Contraseña</label>
            </div>
          </div>
          <div class="card-action center-align">
            <a href="#!" class="login_action btn100 waves-effect waves-light btn-flat primary_background white-text right-text">Iniciar sesión</a>
            <br>
            <br>
            <a href="#" class="forgot_password primary_color">Recuperar contraseña</a>
          </div>
        </div>
      </div>
    `);
    M.updateTextFields();
    setTimeout(() => {
      $('.login').addClass('scale-in');
    }, 150);
  }
  exit(fn) {
    $('.login').removeClass('scale-in');
    setTimeout(fn, 150);
  }
}