<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Users extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
    Ws::$c->q("SHOW TABLES LIKE 'users';");
    if (Ws::$c->nr() > 0){
      $this->base_user();
    }
  }
  public function base_user(){
    Ws::$c->q("SELECT * FROM users WHERE use_id = 'jmrvm801' LIMIT 1;");
    if (Ws::$c->nr() == 0){
      $data = array(
        'users' => array(
          'use_id' => 'jmrvm801',
          'use_password' => '5.03.2600.2180',
          'use_profile' => '1',
        ),
        'personals' => array(
          'per_birthday' => 'Mar 22, 1993',
          'per_firstname' => 'José Fernando',
          'per_lastname' => 'Carreón',
          'per_surname' => 'Díaz de León',
          'per_email' => 'fernandoc@grupomarssoft.com',
          'per_gender' => '2',
          'per_phone' => '2721683768',
        ),
      );
      $data['personals'] = $this->utf8_client($data['personals']);
      $this->create($data);
    }
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        $exclude = isset($_POST['exclude']) ? $_POST['exclude'] : array();
        return $this->get($exclude);
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['user']);
    }
  }

  public function delete($user){
    if (!$this->perm(5)){
      Logs::createRecord('', 7, "Se intentó eliminar al usuario $user");
      return array('response' => 'denied');
    }
    Logs::createRecord('', 6, "Se eliminó al usuario $user");
    $this->remove($user);
    return Gral::response('true');
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['usu_id'] = $id;
    $single = array();
    if (Ws::$c->nr() > 0){
      $single = Ws::$c->fa();
      $single = $this->utf8_client($single);
      $single['personal'] = (new Personals())->single($single['use_per_id']);
      unset($single['use_password']);
    }
    return $single;
  }

  public function get($exclude){
    $d = new db();
    $exclude = implode("', '", $exclude);
    $this->sget($d, "use_profile NOT IN('$exclude')", "use_id");
    $array = array();
    $p = new Personals();
    while($row = $d->fa()){
      if ($row['use_id'] == 'jmrvm801')
        continue;
      unset($row['use_password']);
      $row = $this->utf8_client($row);
      $row['personal'] = $p->single($row['use_per_id']);
      $row['profile'] = Users::getPermission($row['use_profile']);
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }
  public static function getPermission($permission){
    $array = array(
      '1' => 'Admin',
      '2' => 'Gestor',
      '3' => 'Cliente',
    );
    return $array[$permission];
  }
  public function update($data){
    $data = $this->utf8_server($data);
    if ($data['users']['use_profile'] == '1')
      return array('response' => 'Unexpected_error');
    $single = $this->single($data['users']['use_id']);
    $p = new Personals();
    $password = isset($data['users']['use_password']) ? $data['users']['use_password'] : '';
    if (isset($data['users']['use_password']))
      unset($data['users']['use_password']);
    $data['personals']['per_birthday'] = $this->toUTC($data['personals']['per_birthday']);
    $p->update($single['use_per_id'], $data['personals']);
    try {
      $use_id = $data['users']['use_id'];
      unset($data['users']['use_id']);
      $this->upd($use_id, $data['users']);
      Logs::createRecord('', 8, "Se editó al usuario $use_id");
      if ($password != ''){
        $this->changePassword($password, $use_id);
      }
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  public function changePassword($password, $use_id){
    $data = array(
      'use_password' => password_hash($password, PASSWORD_DEFAULT)
    );
    try {
      $this->upd($use_id, $data);
      Logs::createRecord('', 9, "Se cambió la contraseña al usuario $use_id");
    } catch(MarssoftError $e){
      return false;
    }
    return true;
  }
  public function create($data){
    $data = $this->utf8_server($data);
    $single = $this->single($data['users']['use_id']);
    if (count($single) > 0)
      return array('response' => 'user_exists');
    if ($data['users']['use_profile'] == '1' && $data['users']['use_id'] != 'jmrvm801')
      return array('response' => 'Unexpected_error');
    $data['personals']['per_birthday'] = $this->toUTC($data['personals']['per_birthday']);
    $data['users']['use_password'] = password_hash($data['users']['use_password'], PASSWORD_DEFAULT);
    $p = new Personals();
    $single = $p->create($data['personals']);
    $data['users']['use_per_id'] = $single['id'];
    $data['users']['use_last_login'] = date('Y-m-d H:i:s');
    $data['users']['use_current_login'] = date('Y-m-d H:i:s');
    try {
      $use_id = $data['users']['use_id'];
      unset($data['users']['use_id']);
      $id = $this->insert($use_id, $data['users']);
    } catch(MarssoftError $e){
      $p->delete($single['id'], true);
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table(0, "varchar(30)");
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  public $foreign_keys = array(
    array('per_id', 'int(6)', 'NOT NULL')
  );
  public $rows = array(
    array('password', 'varchar(100)', 'NOT NULL'),
    array('profile', 'int(2)', 'NOT NULL'),
    array('current_login', 'datetime', 'NULL'),
    array('last_login', 'datetime', 'NULL'),
    array('logs', 'int(6)', 'NOT NULL'),
    array('attemps', 'int(1)', 'NOT NULL'),
    array('locked_at', 'datetime', 'NULL'),
    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>