<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Products extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
      case 'getResources':
        return $this->getResources();
    }
  }

  public function getResources(){
    $subfamilies = new Subfamilies();
    return array(
      'categories' => $subfamilies->getSubfamiliesByFamId('1'),
      'brands' => $subfamilies->getSubfamiliesByFamId('2'),
    );
  }

  public function relatedProducts($sub_id, $restrict){
    Ws::$c->q("SELECT pro_id FROM products WHERE pro_sub_id = '$sub_id' AND pro_deleted = '0' AND pro_id != '$restrict' ORDER BY RAND() LIMIT 3");
    $ids = array();
    while($row = Ws::$c->fa()){
      array_push($ids, $row['pro_id']);
    }
    for($i = 0; $i < count($ids); $i++){
      $ids[$i] = $this->single($ids[$i], true);
    }
    return $ids;
  }

  public function isAvailable($id, $number){
    Ws::$c->q("SELECT pro_exists FROM products WHERE pro_id = '$id' LIMIT 1;");
    return ($number <= Ws::$c->r());
  }

  public function getProductsByCategory($id){
    Ws::$c->q("SELECT pro_id FROM products WHERE pro_sub_id = '$id' AND pro_deleted = '0' ORDER BY pro_name");
    $ids = array();
    while($row = Ws::$c->fa()){
      array_push($ids, $row['pro_id']);
    }
    for($i = 0; $i < count($ids); $i++){
      $ids[$i] = $this->single($ids[$i], true);
    }
    return $ids;
  }

  public function getRamdomProducts($limit = 3, $exists = true){
    Ws::$c->q("SELECT pro_id FROM products WHERE pro_exists > '0' AND pro_deleted = '0' ORDER BY RAND() LIMIT ".$limit);
    $ids = array();
    while($row = Ws::$c->fa()){
      array_push($ids, $row['pro_id']);
    }
    for($i = 0; $i < count($ids); $i++){
      $ids[$i] = $this->single($ids[$i], true);
    }
    return $ids;
  }


  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  public function single($id, $price = false){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['pro_id'] = $id;
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    if ($single['pro_offer_price'] != '' && $single['pro_offer_price'] != '0'){
      $original = $single['pro_price_1'];
      $single['pro_price_1'] = $single['pro_offer_price'];
      $single['pro_offer_price'] = $original;
    }
    if ($price){
      $base = array('pro_price_1', 'pro_price_3', 'pro_price_6', 'pro_price_9', 'pro_price_12', 'pro_offer_price');
      for($i = 0; $i < count($base); $i++){
        $item = $single[$base[$i]];
        if ($item == '' || $item == '0')
          continue;
        $single[$base[$i]] = $single[$base[$i]] + (($single[$base[$i]] * 0.0290) + 2.50) * 1.16;
      }
    }
    
    $single['pictures'] = (new Documents())->getDocuments('products', $id);
    $single['subfamily'] = (new Subfamilies())->single($single['pro_sub_id']);
    $single['features'] = (new Features())->get($single['pro_id']);
    return $single;
  }

  public function get(){
    $d = new db();
    $this->sget($d, "", "pro_name");
    $array = array();
    $subfamily = new Subfamilies();
    while($row = $d->fa()){
      $row['subfamily'] = $subfamily->single($row['pro_sub_id']);
      $row = $this->utf8_client($row);
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }
  public function update($data){
    $data = $this->utf8_server($data);
    $features = isset($data['features']) ? $data['features'] : array();
    unset($data['features']);
    $docs = $data['images'];
    if (is_array($docs)){
      if (!is_array($docs[0])){
        $docs = array($docs);
      }
    }
    unset($data['images']);
    $document = new Documents();
    $d = new db();
    try {
      $this->upd($_SESSION["pro_id"], $data);
      $ids = array();
      for($i = 0; $i < count($docs); $i++){
        $doc = $docs[$i];
        if ($doc['doc_type'] == 'Imagen'){
          array_push($ids, $doc['document']);
          continue;
        }
      }
      
      $ids = implode("', '", $ids);
      $d->q("SELECT doc_id FROM documents WHERE doc_type = 'products' AND doc_type_id = '$_SESSION[pro_id]' AND doc_id NOT IN('$ids')");
      while($row = $d->fa()){
        $document->delete($row['doc_id']);
      }
      
      for($i = 0; $i < count($docs); $i++){
        $doc = $docs[$i];
        if ($doc['doc_type'] == 'Imagen'){
          continue;
        }
        $array = array(
          'doc_name' => 'photo',
          'doc_extension' => strtolower($doc['doc_type']),
          'doc_size' => '10000',
          'data' => $doc['document'],
        );
        $sid = $document->create($array)['id'];
        $document->formalizeFiles($sid, 'products', $_SESSION["pro_id"]);
      }
      $feature = new Features();
      for($i = 0; $i < count($features); $i++){
        $features[$i]['fea_pro_id'] = $_SESSION["pro_id"];
        $fea_id = $features[$i]['fea_id'];
        unset($features[$i]['fea_id']);
        if ($fea_id == '0'){
          $feature->create($features[$i]);
        } else {
          $feature->update($fea_id, $features[$i]);
        }
      }
    } catch(MarssoftError $e){
      $d->cl();
      return Gral::error('false', $e->getOptions());
    }
    $d->cl();
    return Gral::response('true');
  }

  public function create($data){
    $data = $this->utf8_server($data);
    $features = isset($data['features']) ? $data['features'] : array();
    unset($data['features']);
    $docs = $data['images'];
    unset($data['images']);
    try {
      $id = $this->insert("NULL", $data);
      $document = new Documents();
      for($i = 0; $i < count($docs); $i++){
        $doc = $docs[$i];
        $array = array(
          'doc_name' => 'photo',
          'doc_extension' => strtolower($doc['doc_type']),
          'doc_size' => '10000',
          'data' => $doc['document'],
        );
        $sid = $document->create($array)['id'];
        $document->formalizeFiles($sid, 'products', $id);
      }
      $feature = new Features();
      for($i = 0; $i < count($features); $i++){
        $features[$i]['fea_pro_id'] = $id;
        unset($features[$i]['fea_id']);
        $feature->create($features[$i]);
      }
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id); 
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('sub_id', 'int(6)', 'NOT NULL'),
    array('bra_id', 'int(6)', 'NOT NULL'),
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('code', 'varchar(25)', 'NOT NULL'),
    array('name', 'varchar(100)', 'NOT NULL'),
    array('descr', 'TEXT', 'NOT NULL'),
    array('price_1', 'varchar(9)', 'NOT NULL'),
    array('price_3', 'varchar(9)', 'NOT NULL'),
    array('price_6', 'varchar(9)', 'NOT NULL'),
    array('price_9', 'varchar(9)', 'NOT NULL'),
    array('price_12', 'varchar(9)', 'NOT NULL'),
    array('offer_price', 'varchar(9)', 'NOT NULL'),

    array('exists', 'int(4)', 'NOT NULL'),

    array('deleted', 'varchar(100)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>