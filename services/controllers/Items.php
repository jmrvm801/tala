<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Items extends QueryBuilder implements SqlManagement{
  public $status = array('', 'Pendiente', 'Aceptado', 'Fuera de stock');

  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['id'], $_POST['data']);
      case 'get':
        return $this->get($_POST['id']);
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }


  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    return $single;
  }

  public function get($id){
    $d = new db();
    $this->sget($d, "ite_ord_id = '$id'", "ite_id");
    $array = array();
    while($row = $d->fa()){
      $row = $this->utf8_client($row);
      $row['product'] = (new Products())->single($row['ite_pro_id']);
      $row['status'] = $this->status[$row['ite_status']];
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }
  public function update($id, $data){
    $data = $this->utf8_server($data);
    try {
      $this->upd($id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id); 
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('ord_id', 'int(6)', 'NOT NULL'),
    array('pro_id', 'int(6)', 'NOT NULL')
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('price_1', 'varchar(10)', 'NOT NULL'),
    array('price_3', 'varchar(10)', 'NOT NULL'),
    array('price_6', 'varchar(10)', 'NOT NULL'),
    array('price_9', 'varchar(10)', 'NOT NULL'),
    array('price_12', 'varchar(10)', 'NOT NULL'),
    array('units', 'int(4)', 'NOT NULL'),
    array('units_before', 'int(4)', 'NOT NULL'),
    array('amount', 'varchar(10)', 'NOT NULL'),
    array('status', 'int(1)', 'NOT NULL'),

    array('deleted', 'varchar(100)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>