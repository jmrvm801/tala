<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Ships extends QueryBuilder implements SqlManagement{
  public $status = array('', 'Persona física', 'Persona moral');

  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['id'], $_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }


  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  public function single($id){
    Ws::$c->q("SELECT shi_id FROM ships WHERE shi_use_id = '$id' LIMIT 1;");
    if (Ws::$c->nr() == 0){
      return array(
        'shi_use_id' => '',
        'shi_street' => '',
        'shi_external' => '',
        'shi_internal' => '',
        'shi_suburd' => '',
        'shi_postal' => '',
        'shi_city' => '',
        'shi_state' => '',
        'shi_additional' => '',
        'shi_social_reason' => '',
        'shi_rfc' => '',
        'shi_rfc_type' => '',
        'shi_cfdi_use' => '',
      );
    }
    $id = Ws::$c->r();
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    return $single;
  }

  public function get(){
    $d = new db();
    $this->sget($d, "", "pro_name");
    $array = array();
    $subfamily = new Subfamilies();
    while($row = $d->fa()){
      $row['subfamily'] = $subfamily->single($row['pro_sub_id']);
      $row = $this->utf8_client($row);
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }
  public function update($id, $data){
    $data = $this->utf8_server($data);
    try {
      $this->upd($id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id); 
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('use_id', 'varchar(30)', 'NOT NULL')
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('street', 'varchar(100)', 'NOT NULL'),
    array('external', 'varchar(100)', 'NOT NULL'),
    array('internal', 'varchar(100)', 'NOT NULL'),
    array('suburd', 'varchar(100)', 'NOT NULL'),
    array('postal', 'varchar(5)', 'NOT NULL'),
    array('city', 'varchar(100)', 'NOT NULL'),
    array('state', 'varchar(100)', 'NOT NULL'),
    array('additional', 'varchar(100)', 'NOT NULL'),
    array('social_reason', 'varchar(200)', 'NOT NULL'),
    array('rfc', 'varchar(13)', 'NOT NULL'),
    array('rfc_type', 'varchar(1)', 'NOT NULL'), /* 1 = persona física, 2 = personal moral */
    array('cfdi_use', 'varchar(10)', 'NOT NULL'), /* 1 = persona física, 2 = personal moral */

    array('deleted', 'varchar(100)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>