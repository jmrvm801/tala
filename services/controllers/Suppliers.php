<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Suppliers extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
      case 'loadResources':
        return $this->loadResources();
    }
  }

  public function loadResources(){
    $subfamilies = new Subfamilies();
    return array(
      'turns' => $subfamilies->getSubfamiliesByFamId(2),
      'services' => $subfamilies->getSubfamiliesByFamId(1),
    );
  }

  public function delete($id){
    $single = $this->single($id);
    Logs::createRecord('', 15, "Se eliminó al proveedor $single[sup_supplier_name]");
    $this->remove($id);
    return Gral::response('true');
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['sup_id'] = $id;
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    return $single;
  }

  public function get(){
    $this->sget(Ws::$c, "", "sup_supplier_name");
    $array = array();
    while($row = Ws::$c->fa()){
      $row = $this->utf8_client($row);
      array_push($array, $row);
    }
    return $array;
  }

  public function update($data){
    $data = $this->utf8_server($data);
    Logs::createRecord('', 14, "Se editó al proveedor $_SESSION[sup_id]");
    try {
      $this->upd($_SESSION['sup_id'], $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }
  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
      Logs::createRecord('', 13, "Se creó al proveedor $data[sup_supplier_name]");
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table(0);
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }

  public $foreign_keys = array();
  public $rows = array(
    array('supplier_name', 'varchar(200)', 'NULL'),
    array('contact_name', 'varchar(200)', 'NULL'),
    array('social_reason', 'varchar(200)', 'NULL'),
    array('address', 'varchar(200)', 'NULL'),
    array('rfc', 'varchar(15)', 'NULL'),
    array('bank_info', 'varchar(200)', 'NULL'),
    array('phone_home_1', 'varchar(16)', 'NULL'),
    array('phone_home_2', 'varchar(16)', 'NULL'),
    array('phone_cell', 'varchar(16)', 'NULL'),
    array('fax', 'varchar(16)', 'NULL'),
    array('email', 'varchar(200)', 'NULL'),
    
    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>