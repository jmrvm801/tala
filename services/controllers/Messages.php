<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Messages extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'create_message':
        return $this->create_message($_POST['data']);
      case 'get':
        return $this->get($_POST['id']);
    }
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    return $single;
  }

  public function get($id){
    $d = new db();
    $this->sget($d, "mes_ord_id = '$id'", "mes_id");
    $array = array();
    while($row = $d->fa()){
      $row = $this->utf8_client($row);
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }

  public function create_message($data){
    $data['mes_type'] = '2';
    $r = $this->create($data);
    return Gral::response('true', $this->single($r['id']));
  }

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id); 
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('ord_id', 'int(6)', 'NOT NULL')
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('messages', 'TEXT', 'NOT NULL'),
    array('type', 'INT(1)', 'NOT NULL'), /* 1 = cliente, 2 = vendedor */

    array('deleted', 'varchar(100)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>