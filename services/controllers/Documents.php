<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Documents extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }

  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
    }
  }

  public function getDocuments($type, $type_id){
    $docs = array();
    Ws::$c->q("SELECT * FROM documents WHERE doc_type = '$type' AND doc_type_id = '$type_id' AND doc_deleted = '0';");
    while($row = Ws::$c->fa()){
      array_push($docs, $row);
    }
    return $docs;
  }

  public function formalizeFiles($file, $type, $id){
    Ws::$c->q("UPDATE documents SET doc_type = '$type', doc_type_id = '$id', doc_status = '1' WHERE doc_id = '$file' LIMIT 1;");
  }

  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    return $single;
  }

  public function prohibited_extensions($ext){
    $files = array("php", "phtml", "php3", "php4", "js", "shtml", "pl", "py", "exe", "msi", "css", "html", "jsp", "htaccess", "html", "bat", "dll", "ini", "asp", "htm", "sh", "cgi", "dat");
    return array_search($ext, $files) !== false;
  }

  public function create($data){
    $data = $this->utf8_server($data);
    if ($this->prohibited_extensions($data['doc_extension'])){
      return array('response' => 'prohibited');
    }
    $document = array(
      'document' => $data['data'],
      'doc_type' => $data['doc_extension']      
    );
    $data['doc_status'] = '0';
    unset($data['data']);
    unset($data['mat_id']);
    try {
      $id = $this->insert("NULL", $data);
      $this->setFile($document, $id, 'docs');
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array();
  /**
   * Set row keys
   */
  public $rows = array(
    array('name', 'varchar(250)', 'NOT NULL'),
    array('extension', 'varchar(10)', 'NOT NULL'),
    array('type', 'varchar(10)', 'NOT NULL'),
    array('type_id', 'int(6)', 'NOT NULL'),
    
    array('size', 'int(11)', 'NOT NULL'),
    array('status', 'int(1)', 'NOT NULL'), /* 0 = unassigned | 1 = assigned */
    array('deleted', 'int(1)', 'NOT NULL'),

    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>