<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Personals extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'helloworld':
        return $this->helloworld();
      break;
    }
  }

  public function helloworld(){
    return array('shi');
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    $single['fullname'] = $single['per_firstname']." ".$single['per_lastname']." ".$single['per_surname'];
    $single['birthday'] = Ws::$g->numberDateToString($single['per_birthday']);
    return $single;
  }

  
  public function update($per_id, $data){
    $data = $this->utf8_server($data);
    try {
      $this->upd($per_id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id);
  }

  public function delete($id, $physical = false){
    if ($physical)
      $this->removePhysical($id);
    else
      $this->remove($id);
    return Gral::response('true');
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }

  /**
   * row data
   */
  public $foreign_keys = array();
  /**
   * row data
   */
  public $rows = array(
    array('social_reason', 'varchar(200)', 'NOT NULL'),
    array('firstname', 'varchar(100)', 'NOT NULL'),
    array('lastname', 'varchar(100)', 'NOT NULL'),
    array('surname', 'varchar(100)', 'NOT NULL'),
    array('email', 'varchar(150)', 'NOT NULL'),
    array('birthday', 'date', 'NOT NULL'),
    array('gender', 'int(1)', 'NOT NULL'),
    array('phone', 'varchar(20)', 'NOT NULL'),
    array('email_verified', 'int(1)', 'NOT NULL'),
    array('email_code', 'int(6)', 'NOT NULL'),
    array('deleted', 'int(1)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>