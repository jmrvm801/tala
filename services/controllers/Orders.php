<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Orders extends QueryBuilder implements SqlManagement{
  //                     0        1                2                    3                  4               5                 6               7           8
  public $status = array('', 'Pendiente', 'Listo para pagar', 'Listo con detalles', 'Fuera de stock', 'Cancelado', 'Pagado, por enviar', 'Enviado', 'Entregado');

  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['id'], $_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
      case 'formalizeOrder':
        return $this->formalizeOrder($_POST['data']);
      case 'cancelOrden':
        return $this->cancelOrden();
      case 'orderpartialyAproved':
        return $this->orderpartialyAproved($_POST['data']);
      case 'sendReminder':
        return $this->sendReminder();
      case 'sendShippingData':
        return $this->sendShippingData($_POST['data']);
      case 'orderReceived':
        return $this->orderReceived();
    }
  }

  public function orderReceived(){
    $array = array(
      'ord_status' => '8',
      'ord_delivered_at' => date('Y-m-d H:i:s')
    );
    $this->update($_SESSION['ord_id'], $array);
    $order = $this->single($_SESSION['ord_id']);
    $single = (new Users())->single($order['ord_use_id']);
    Ws::$g->sendmail($single['personal']['per_email'], 'Orden recibida para el pedido No. '.$_SESSION['ord_id'], 'Le informamos que la empresa de envíos nos ha indicado que el producto ha llegado. Muchas gracias por comprar con nosotros.');
    return Gral::response('true');
  }

  public function sendShippingData($data){
    $array = array(
      'ord_shipping_company' => $data['ord_shipping_company'],
      'ord_shipping_code' => $data['ord_shipping_code'],
      'ord_status' => '7',
      'ord_send_at' => date('Y-m-d H:i:s')
    );
    $this->update($_SESSION['ord_id'], $array);
    $order = $this->single($_SESSION['ord_id']);
    $single = (new Users())->single($order['ord_use_id']);
    Ws::$g->sendmail($single['personal']['per_email'], 'Su pedido No. '.$_SESSION['ord_id'].' va en camino', 'Le informamos que su pedido va en camino en la empresa '.$data['ord_shipping_company'].' con número de rastreo '.$data['ord_shipping_code'].'. Muchas gracias por comprar con nosotros.');
    return Gral::response('true');
  }

  public function sendReminder(){
    $order = $this->single($_SESSION['ord_id']);
    $single = (new Users())->single($order['ord_use_id']);
    Ws::$g->sendmail($single['personal']['per_email'], 'Recordatorio de pago para el pedido No. '.$_SESSION['ord_id'], 'Le comentamos que tiene un pedido pendiente por pagar y continuar con el proceso de compra de su orden. Si tiene una duda por favor comuníquese por teléfono o correo electrónico a Talabartería la Guadalupana.');
    return Gral::response('true');
  }

  public function orderpartialyAproved($data){
    $items = $data['items'];
    unset($data['items']);
    $array = array(
      'ord_status' => '3',
      'ord_accepted_at' => date('Y-m-d H:i:s'),
      'ord_subtotal' => $data['ord_subtotal'],
      'ord_shipping_amount' => $data['ord_shipping_amount'],
      'ord_total' => $data['ord_total'],
      'ord_montly_payment' => $data['ord_montly_payment'],
    );
    $this->update($_SESSION['ord_id'], $array);
    $cls_items = new Items();
    for($i = 0; $i < count($items); $i++){
      $item = array(
        'ite_status' => '2',
        'ite_units' => $items[$i]['ite_units'],
        'ite_amount' => $items[$i]['ite_amount'],
      );
      $cls_items->update($items[$i]['ite_id'], $item);
    }
    $order = $this->single($_SESSION['ord_id']);
    $single = (new Users())->single($order['ord_use_id']);
    Ws::$g->sendmail($single['personal']['per_email'], 'Su pedido No. '.$_SESSION['ord_id'].' fue aceptado con detalles', 'Su pedido tuvo un inconveniente con la disponibilidad de inventario. Sin embargo, están disponibles ciertos productos para que pueda continuar con la compra.');
    return Gral::response('true');
  }

  public function cancelOrden(){
    $array = array(
      'ord_status' => '5',
      'ord_rejected_at' => date('Y-m-d H:i:s'),
      'ord_canceled_at' => date('Y-m-d H:i:s'),
    );
    $this->update($_SESSION['ord_id'], $array);
    $order = $this->single($_SESSION['ord_id']);
    $single = (new Users())->single($order['ord_use_id']);
    Ws::$g->sendmail($single['personal']['per_email'], 'El pedido No. '.$_SESSION['ord_id'].' fue cancelado', 'Lamentamos informarle que a pesar de que en la página se anunciaba la disponibilidad de los productos que solicitó, estos no se encuentran disonibles por el momento.<br><br>Reciba nuestra más sincera disculpa.');
    return Gral::response('true');
  }

  public function formalizeOrder($data){
    $items = $data['items'];
    unset($data['items']);
    
    $array = array(
      'ord_status' => '2',
      'ord_accepted_at' => date('Y-m-d H:i:s'),
      'ord_subtotal' => $data['ord_subtotal'],
      'ord_shipping_amount' => $data['ord_shipping_amount'],
      'ord_total' => number_format($data['ord_total'], 2, '.', ''),
      'ord_montly_payment' => $data['ord_montly_payment'],
    );
    $this->update($_SESSION['ord_id'], $array);
    $cls_items = new Items();
    for($i = 0; $i < count($items); $i++){
      $item = array(
        'ite_status' => '2',
        'ite_units' => $items[$i]['ite_units']
      );
      $cls_items->update($items[$i]['ite_id'], $item);
    }
    $order = $this->single($_SESSION['ord_id']);
    $single = (new Users())->single($order['ord_use_id']);
    Ws::$g->sendmail($single['personal']['per_email'], 'Felicidades, su pedido No. '.$_SESSION['ord_id'].' está listo', 'Le informamos que su pedido ha sido confirmado por nuestros agentes de ventas. El costo del envío asignado es de '.$data['ord_shipping_amount'].'. Le informamos que ahora puede realizar el pago de su pedido con tarjetas de crédito participantes incluyendo meses sin intereses.<br>Gracias por comprar con nosotros');
    return Gral::response('true');
  }

  public function getOrdersByUser($id, $simple = false){
    $d = new db();
    $this->sget($d, "ord_use_id = '$id'", "ord_id");
    $array = array();
    while($row = $d->fa()){
      $row = $this->utf8_client($row);
      if (!$simple)
        $row['items'] = (new Items())->get($row['ord_id']);
      $row['status'] = $this->status[$row['ord_status']];
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }


  public function delete($id){
    $this->remove($id);
    return Gral::response('true');
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    $_SESSION['ord_id'] = $id;
    $single['items'] = (new Items())->get($single['ord_id']);
    $single['status'] = $this->status[$single['ord_status']];
    $single['ship'] = (new Ships())->single($single['ord_use_id']);
    $single['user'] = (new Users())->single($single['ord_use_id']);
    $single['messages'] = (new Messages())->get($single['ord_id']);
    if ($single['ord_openpay_code'] != ''){
      Ws::$c->q("SELECT * FROM openpay_successed WHERE ope_authorization = '$single[ord_openpay_code]' LIMIT 1;");
      $single['openpay'] = Ws::$c->fa();
    } else {
      $single['openpay'] = array(
        'ope_bank' => '',
        'ope_brand' => '',
        'ope_credit_type' => '',
      );
    }
    return $single;
  }

  public function get(){
    $d = new db();
    $this->sget($d, "", "ord_id DESC");
    $array = array();
    while($row = $d->fa()){
      $row = $this->utf8_client($row);
      $row['client'] = (new Users())->single($row['ord_use_id']);
      $row['status'] = $this->status[$row['ord_status']];
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }
  public function update($id, $data){
    $data = $this->utf8_server($data);
    try {
      $this->upd($id, $data);
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  public function create($data){
    $data = $this->utf8_server($data);
    $items = $data['items'];
    unset($data['items']);
    try {
      $id = $this->insert("NULL", $data);
      $item = new Items();
      for($i = 0; $i < count($items); $i++){
        $items[$i]['ite_ord_id'] = $id;
        $item->create($items[$i]);
      }
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id); 
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('use_id', 'varchar(30)', 'NOT NULL')
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('subtotal', 'varchar(20)', 'NOT NULL'),
    array('shipping_amount', 'varchar(10)', 'NOT NULL'),
    array('total', 'varchar(20)', 'NOT NULL'),
    array('montly_payment', 'varchar(20)', 'NOT NULL'),
    array('financing', 'int(1)', 'NOT NULL'),
    array('status', 'int(1)', 'NOT NULL'),
    array('shipping_code', 'varchar(100)', 'NULL'),
    array('shipping_company', 'varchar(100)', 'NULL'),

    array('openpay_code', 'varchar(100)', 'NULL'),
    array('openpay_msi', 'varchar(100)', 'NULL'),
    array('openpay_comission', 'varchar(100)', 'NULL'),
    array('openpay_taxes', 'varchar(100)', 'NULL'),
    array('openpay_authorization', 'varchar(100)', 'NULL'),

    array('accepted_at', 'datetime', 'NULL'),
    array('rejected_at', 'datetime', 'NULL'),
    array('paid_at', 'datetime', 'NULL'),
    array('canceled_at', 'datetime', 'NULL'),
    array('send_at', 'datetime', 'NULL'),
    array('delivered_at', 'datetime', 'NULL'),

    array('deleted', 'varchar(100)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>