<?php
include_once("QueryBuilder.php");
include_once("SqlManagement.php");
class Subfamilies extends QueryBuilder implements SqlManagement{
  public function __construct(){
    parent::__construct($this);
  }
  public function run($method = 'default'){
    switch($method){
      case 'create':
        return $this->create($_POST['data']);
      case 'update':
        return $this->update($_POST['data']);
      case 'get':
        return $this->get();
      case 'single':
        return $this->single($_POST['id']);
      case 'delete':
        return $this->delete($_POST['id']);
      case 'getSubfamiliesByFamId':
        return $this->getSubfamiliesByFamId($_POST['id']);
    }
  }

  public function getSubfamiliesByFamId($id){
    Ws::$c->q("SELECT * FROM subfamilies WHERE sub_deleted = '0' AND sub_fam_id = '$id' ORDER BY sub_id ");
    $subfamilies = array();
    while($subfamily = Ws::$c->fa()){
      $subfamily = $this->utf8_client($subfamily);
      array_push($subfamilies, $subfamily);
    }
    return $subfamilies;
  }

  public function delete($id){
    Logs::createRecord('', 21, "Se eliminó la subfamilia $id");
    $this->remove($id);
    return Gral::response('true');
  }

  public function single($id){
    $this->bsingle(Ws::$c, $id);
    $_SESSION['sub_id'] = $id;
    $single = Ws::$c->fa();
    $single = $this->utf8_client($single);
    return $single;
  }

  public function get(){
    $d = new db();
    $this->sget($d, "", "sub_name");
    $array = array();
    $family = new Families();
    while($row = $d->fa()){
      $row['family'] = $family->single($row['sub_fam_id']);
      $row = $this->utf8_client($row);
      array_push($array, $row);
    }
    $d->cl();
    return $array;
  }
  public function update($data){
    $data = $this->utf8_server($data);
    try {
      $this->upd($_SESSION["sub_id"], $data);
      Logs::createRecord('', 20, "Se editó la subfamilia $data[sub_name]");
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true');
  }

  public function create($data){
    $data = $this->utf8_server($data);
    try {
      $id = $this->insert("NULL", $data);
      Logs::createRecord('', 19, "Se creó la subfamilia $data[sub_name]");
    } catch(MarssoftError $e){
      return Gral::error('false', $e->getOptions());
    }
    return Gral::response('true', $id); 
  }

  public function sql_rules(){
    $this->create_table();
    $this->create_fields($this->foreign_keys, 0, true);
    $this->create_fields($this->rows, 0);
  }
  /**
   * Set foreign keys
   */
  public $foreign_keys = array(
    array('fam_id', 'int(6)', 'NOT NULL')
  );
  /**
   * Set row keys
   */
  public $rows = array(
    array('name', 'varchar(100)', 'NOT NULL'),
    array('deleted', 'varchar(100)', 'NOT NULL'),
    array('created_at', 'datetime', 'NOT NULL'),
    array('updated_at', 'datetime', 'NOT NULL')
  );
}

?>