<?php

require('../openpay/Openpay.php');

class OpenPayCreator{
	public function runnable(){
    // Sandbox
    // return Openpay::getInstance('mcgd7w1k2znwkmczfjqq','sk_a7992ee29f9d4c4b851e12fcd92d87c4');
    //Production
		return Openpay::getInstance('mlqyplwkyovxwiitj7y2','sk_b481c627873f4b23bb014ba029f17db9');
	}
}

class Login
{
  public function __construct(){
  }

  public function run($method = 'default')
  {
    switch ($method) {
      case 'grant':
        return $this->{$method}($_POST['data']);
      case 'logout':
        return $this->{$method}();
      case 'logged':
        return array('response' => $this->{$method}());
      case 'getResources':
        return $this->getResources();
      case 'getResourcesDashboard':
        return $this->getResourcesDashboard();
      case 'getCategories':
        return $this->getCategories($_POST['id']);
      case 'getProduct':
        return $this->getProduct($_POST['id']);
      case 'addCart':
        return $this->addCart($_POST['id']);
      case 'get_cart':
        return $this->get_cart();
      case 'create_user':
        return $this->create_user($_POST['data']);
      case 'update_user':
        return $this->update_user($_POST['data']);
      case 'send_code':
        return $this->send_code();
      case 'validate_code':
        return $this->validate_code($_POST['code']);
      case 'grant_user':
        return $this->grant_user($_POST['data']);
      case 'save_chips':
        return $this->save_chips($_POST['data']);
      case 'complete_order':
        return $this->complete_order($_POST['data']);
      case 'get_single_order':
        return $this->get_single_order($_POST['id']);
      case 'onlinePayment':
        return $this->onlinePayment($_POST['data']);
      case 'validateCharge':
        return $this->validateCharge($_POST['id']);
      case 'create_message':
        return $this->create_message($_POST['data']);
      case 'getMessages':
        return $this->getMessages($_POST['id']);
    }
  }

  public function getMessages($id){
    return (new Messages())->get($id);
  }

  public function create_message($data){
    $data['mes_type'] = '1';
    $message = new Messages();
    $r = $message->create($data);
    return Gral::response('true', $message->single($r['id']));
  }

  public function validateCharge($id){
    Ws::$c->q("SELECT * FROM openpay_successed WHERE ope_authorization = '$id' LIMIT 1;");
    if (Ws::$c->nr() == 0)
      return array('response' => 'not_found');
    $row = Ws::$c->fa();
    Openpay::setProductionMode(true);
    $openpay = (new OpenPayCreator())->runnable();
    // print_r($openpay);
    $charge = $openpay->charges->get($row['ope_authorization']);
    // print_r($charge
    if ($charge->{'status'} == 'completed'){
      // print_r($charge);
      $order = new Orders();
      $single = $order->single($row['ope_pay_id']);
      $response = json_decode($row['ope_json_response'], true);
      $msi = 0;
      switch($single['ord_financing']){
        case '3':
          $msi = $single['ord_total'] * 0.0550;
        break;
        case '6':
          $msi = $single['ord_total'] * 0.0780;
        break;
        case '9':
          $msi = $single['ord_total'] * 0.1080;
        break;
        case '12':
          $msi = $single['ord_total'] * 0.1380;
        break;
      }
      $response['comission'] = number_format($response['comission'], 2, '.', '');
      $taxes = number_format((($msi + $response['comission']) * 1.16) - ($msi + $response['comission']), 2, '.', '');
      $msi = number_format($msi, 2, '.', '');
      $single_order = $order->single($row['ope_pay_id']);
      if ($single_order['ord_status'] != '7' && $single_order['ord_status'] != '8'){
        $array = array(
          'ord_paid_at' => date('Y-m-d H:i:s'),
          'ord_status' => '6',
          'ord_openpay_code' => $id,
          'ord_openpay_msi' => $msi,
          'ord_openpay_comission' => $response['comission'],
          'ord_openpay_taxes' => $taxes,
          'ord_openpay_authorization' => $charge->{'authorization'},
        );
        $order->update($row['ope_pay_id'], $array);
        Ws::$g->sendmail('betocastro18@hotmail.com', 'Se ha pagado una orden de compra con No. '.$response['id'], 'Se le informa que se ha realizado un pago en línea desde la tienda en línea. Por favor verifique en la plataforma de OpenPay el pago realizado por un monto de $response[paid] pesos.');
      }
      Ws::$c->q("UPDATE openpay_successed SET ope_json_response = '".json_encode($response)."', ope_paid = '$response[paid]', ope_comission = '$response[comission]', ope_tax = '$response[tax]' WHERE ope_id = '$row[ope_id]' LIMIT 1; ");
      return array(
        'response' => 'completed',
        'single' => $order->single($row['ope_pay_id'])
      );
    } else {
      $response = array(
        'response' => 'failed',
        'authorization' => $charge->{'authorization'},
        'description' => $charge->{'serializableData'},
      );
      return $response;
    }
  }

  public function onlinePayment($data, $secure = false){
    $openpay = (new OpenPayCreator())->runnable();
    $order = (new Orders())->single($data['pay_id']);
    $customer = array(
      'name' => $order['user']['personal']['per_firstname'],
      'last_name' => $order['user']['personal']['per_lastname'].' '.$order['user']['personal']['per_surname'],
      'phone_number' => $order['user']['personal']['per_phone'].' ',
      'email' => $order['user']['personal']['per_email'],
    );
    $chargeData = array(
      'method' => 'card',
      'currency' => 'MXN',
      'source_id' => $data['source_id'],
      'amount' => (float)round($order['ord_total'], 0).'.00', //round((float)number_format((float) $order['ord_total'], 2, '.', ''), 2),
      'description' => 'Talabartería la guadalupana, orden No. '.$order['ord_id'],
      'device_session_id' => $data['device_session_id'],
      'customer' => $customer,
      'use_3d_secure' => 'true',
      // 'redirect_url' => 'https://client.armesis.gov.mx/tala/?auth=success',
      'redirect_url' => 'https://talabarterialaguadalupana.com/?auth=success',
    );
    if ($order['ord_financing'] > '1'){
      $chargeData['payment_plan'] = array(
        'payments' => $order['ord_financing']
      );
    }
    // return $chargeData;
    Openpay::setProductionMode(true);
    try {
      $tmp = $openpay->charges->create($chargeData);
      $comission = (($tmp->{'serializableData'}['amount'] * 0.029) + 2.50);
      $response = array(
        'response' => 'success',
        'status' => $tmp->{'status'},
        'authorization' => $tmp->{'id'},
        'credit_type' => $tmp->{'card'}->{'type'},
        'brand' => $tmp->{'card'}->{'brand'},
        'bank' => $tmp->{'card'}->{'bank_name'},
        'paid' => $tmp->{'serializableData'}['amount'],
        'comission' => $comission,
        'chargeData' => $chargeData,
        'tax' => ($comission * 1.16) - $comission
      );
      if ($response['status'] == 'charge_pending'){
        $_SESSION['response'] = $response;
        Ws::$c->q("INSERT INTO openpay_successed VALUES(NULL, '$data[pay_id]', '$data[source_id]', '".$order['ord_use_id']."', '".json_encode($chargeData)."', '".json_encode($response)."', '$response[authorization]', '$response[credit_type]', '$response[brand]', '$response[bank]', '0', '0', '0', '".date('Y-m-d H:i:s')."')");
        $response['redirect'] = $tmp->{'serializableData'};
      } else{
        Ws::$c->q("INSERT INTO openpay_successed VALUES(NULL, '$data[pay_id]', '$data[source_id]', '".$order['ord_use_id']."', '".json_encode($chargeData)."', '".json_encode($response)."', '$response[authorization]', '$response[credit_type]', '$response[brand]', '$response[bank]', '$response[paid]', '$response[comission]', '$response[tax]', '".date('Y-m-d H:i:s')."')");
      }
      return $response;
    } catch(Exception $e){
      $tmp = $e;
      $_SESSION['session_expire'] = $tmp;
      $response = array(
        'response' => 'failed',
        'description' => $tmp->getDescription(),
        'error_code' => $tmp->getErrorCode(),
        'request_id' => $tmp->getRequestId(),
        'charge_data' => $chargeData
      );
      $response['description'] = str_replace("'", "", $response['description']);
      Ws::$c->q("INSERT INTO openpay_failes VALUES(NULL, '$data[pay_id]', '$data[source_id]', '".$order['ord_use_id']."', '".json_encode($chargeData)."', '".json_encode($response)."', '".date('Y-m-d H:i:s')."')");
      if ($response['error_code'] == 3005){
        return $this->onlinePayment($data, true);
      } else {
        return $response;
      }
    }
    // Ws::e($chargeData);
  }

  public function get_single_order($id){
    $single = (new Orders())->single($id);
    $single['response'] = 'true';
    if ($single['ord_use_id'] == $_SESSION['use_id'])
      return $single;
    else
      return Gral::response('not_found');
  }

  public function complete_order($data){
    $cart = $this->get_cart();
    $subtotal = 0;
    $items = array();
    for($i = 0; $i < count($cart['products']); $i++){
      $product = $cart['products'][$i];
      $sub = number_format($product['units'] * $product['product']['pro_price_'.$data['ord_financing']], 2, '.', '');
      array_push($items, array(
        'ite_ord_id' => '',
        'ite_pro_id' => $product['product']['pro_id'],
        'ite_price_1' => number_format($product['product']['pro_price_1'], 2, '.', ''),
        'ite_price_3' => number_format($product['product']['pro_price_3'], 2, '.', ''),
        'ite_price_6' => number_format($product['product']['pro_price_6'], 2, '.', ''),
        'ite_price_9' => number_format($product['product']['pro_price_9'], 2, '.', ''),
        'ite_price_12' => number_format($product['product']['pro_price_12'], 2, '.', ''),
        'ite_units' => $product['units'],
        'ite_units_before' => $product['units'],
        'ite_amount' => $sub,
        'ite_status' => '1',
      ));
      $subtotal += $sub;
    }
    $data = array(
      'ord_use_id' => $_SESSION['use_id'],
      'ord_subtotal' => $subtotal,
      'ord_shipping_amount' => '0',
      'ord_total' => $subtotal,
      'ord_montly_payment' => number_format($subtotal / $data['ord_financing'], 2, '.', ''),
      'ord_financing' => $data['ord_financing'],
      'ord_status' => '1',
      'items' => $items
    );
    $single = (new Users())->single($_SESSION['use_id']);
    $response = (new Orders())->create($data);
    if ($response['response'] == 'true'){
      Ws::$g->sendmail($single['personal']['per_email'], 'Confirmación de pedido No. '.$response['id'], 'Gracias por confiar en Talabartería la Guadalupana. Le informamos que hemos recibido su pedido y está siendo procesado en las instalaciones de Talabartería la Guadalupana. Una vez que nuestros asesores de ventas hayan confirmado el pedido, podrá realizar el pago en internet con la forma de pago que haya establecido.<br><br>Nuevamente le agradecemos por confiar en Talabartería la Guadalupana.');
      Ws::$g->sendmail('betocastro18@hotmail.com', 'Se ha generado una orden de compra con No. '.$response['id'], 'Se le informa que se ha realizado un pedido desde la tienda en línea. Por favor verifique que lo solicitado por el cliente se encuentre en existencia para que el cliente pueda realizar el pago correspondiente.<br><br>También es importante adjuntarle el costo del envío de acuerdo a los datos del destinatario proporcionados.');
      $_SESSION['cart'] = array();
      $response['data'] = $_SESSION['cart'];
    }
    return $response;
  }

  public function save_chips($data){
    Ws::$c->q("SELECT shi_id FROM ships WHERE shi_use_id = '$_SESSION[use_id]' LIMIT 1;");
    $ships = new Ships();
    if (Ws::$c->nr() == 0){
      $data['shi_use_id'] = $_SESSION['use_id'];
      $ships->create($data);
    } else {
      $id = Ws::$c->r();
      $ships->update($id, $data);
    }
    return Gral::response('true');
  }

  public function grant_user($data){
    return $this->grant($data);
  }

  public function validate_code($code){
    if (!isset($_SESSION['use_id']))
      return Gral::response('error');
    if ($_SESSION['use_profile'] != '3')
      return Gral::response('error');
    $user = new Users();
    $single = $user->single($_SESSION['use_id']);
    if ($single['personal']['per_email_verified'] == '1')
      return Gral::response('validated', '', $single);
    if ($single['personal']['per_email_code'] == $code){
      $data = array(
        'per_email_verified' => '1',
        'per_email_code' => '0',
      );
      (new Personals())->update($single['use_per_id'], $data);
      return Gral::response('true');
    } else {
      return Gral::response('denied');
    }
  }

  public function send_code(){
    if (!isset($_SESSION['use_id']))
      return Gral::response('error');
    if ($_SESSION['use_profile'] != '3')
      return Gral::response('error');
    $user = new Users();
    $single = $user->single($_SESSION['use_id']);
    if ($single['personal']['per_email_verified'] == '1')
      return Gral::response('validated', '', $single);
    $code = rand(100000,999999);
    $data = array(
      'per_email_code' => $code,
    );
    (new Personals())->update($single['use_per_id'], $data);
    Ws::$g->sendmail($single['personal']['per_email'], 'Su clave de verificación', 'Gracias por confiar en Talabartería la Guadalupana. A continuación le hacemos llegar un código de verificación para validar su cuenta de correo.<br><br><h2>'.$code.'</h2>Ingréselo en el campo solicitado.');
    return Gral::response('true');
  }

  public function update_user($data){
    $data['users']['use_profile'] = '3';
    return (new Users())->update($data);
  }

  public function create_user($data){
    $data['users']['use_profile'] = '3';
    $response = (new Users())->create($data);
    if ($response['response'] == 'true'){
      $data = array(
        'username' => $data['users']['use_id'],
        'password' => $data['users']['use_password'],
      );
      return $this->grant($data);
    } else {
      return $response;
    }
  }

  public function get_cart(){
    if (!isset($_SESSION['use_id']))
      return Gral::response('login_required');
    if ($_SESSION['use_profile'] != '3')
      return Gral::response('login_required');
    $user = new Users();
    $single = $user->single($_SESSION['use_id']);
    if ($single['personal']['per_email_verified'] == '0')
      return Gral::response('verify_email', '', $single);
    $products = array();
    $product = new Products();
    $i = 0;
    foreach($_SESSION['cart'] as $pro_id => $units){
      $products[$i] = array(
        'product' => $product->single($pro_id, true),
        'units' => $units
      );
      $i++;
    }
    $data = array(
      'response' => 'true',
      'client' => $single,
      'products' => $products,
      'cart' => $_SESSION['cart'],
      'ship' => (new Ships())->single($_SESSION['use_id']),
      'orders' => (new Orders())->getOrdersByUser($_SESSION['use_id'], true)
    );
    return $data;
  }

  public function addCart($id){
    $array = $_SESSION['cart'];
    $products = new Products();

    if (!isset($array[$id])){
      if (!$products->isAvailable($id, 1))
        return Gral::response('out');
      $array[$id] = 1;
    } else{
      if (isset($_POST['action'])){
        switch($_POST['action']){
          case 'subtract':
            if (!$products->isAvailable($id, $array[$id] - 1))
              return Gral::response('out');
            $array[$id] -= 1;
            if ($array[$id] == 0){
              unset($array[$id]);
            }
          break;
          case 'remove':
            unset($array[$id]);
          break;
        }
      } else {
        if (!$products->isAvailable($id, $array[$id] + 1))
          return Gral::response('out');
        $array[$id] += 1;
      }
      
    }
    $_SESSION['cart'] = $array;
    return Gral::response('true', $_SESSION['cart']);
  }

  public function getProduct($id){
    $products = new Products();
    $single = $products->single($id, true);
    return array(
      'product' => $single,
      'related' => $products->relatedProducts($single['pro_sub_id'], $id)
    );
  }

  public function getCategories($id){
    $products = new Products();
    $subs = new Subfamilies();
    return array(
      'category' => $subs->single($id),
      'products' => $products->getProductsByCategory($id),
    );
  }

  public function getResourcesDashboard(){
    return array(
      'products' => (new Products())->getRamdomProducts(),
      'sliders' => (new Sliders())->getActiveSliders()
    );
  }

  public function getResources(){
    if (!isset($_SESSION['cart']))
      $_SESSION['cart'] = array();
    $subs = new Subfamilies();
    return array(
      'categories' => $subs->getSubfamiliesByFamId(1),
      'cart_items' => $_SESSION['cart']
    );
  }

  public function logout(){
    Logs::createRecord('', 4);
    session_destroy();
    $array = array('use_id', 'per_firstname', 'per_lastname', 'per_surname', 'use_profile', 'per_gender', 'logged');
    for ($i = 0; $i < count($array); $i++)
      Ws::$g->cook($array[$i], '', 1);
    return array('response' => 'true');
  }

  public function grant($datam){
    $logs = new Logs();
    Ws::$c->q("SELECT use_id, use_password, use_locked_at, per_gender, use_attemps, per_firstname, per_lastname, per_surname, use_profile, use_logs, use_current_login, use_last_login FROM users LEFT JOIN personals ON use_per_id = per_id WHERE use_id = '$datam[username]' AND use_deleted = '0' LIMIT 1;");
    if (Ws::$c->nr() == 1) {
      $data = Ws::$c->fa();
      $data['use_locked_at'] = ($data['use_locked_at'] == '') ? date('Y-m-d H:i:s') : $data['use_locked_at'];
      if (strtotime(date('Y-m-d H:i:s')) < strtotime($data['use_locked_at']))
        return array('response' => 'locked', 'time_at' => $data['use_locked_at']);
      if (!password_verify($datam['password'], $data['use_password'])){
        Logs::createRecord($datam['username'], 2);
        $data['use_attemps'] += 1;
        Ws::$c->q("UPDATE users SET use_attemps = '$data[use_attemps]' WHERE use_id = '$datam[username]' LIMIT 1;");
        if ($data['use_attemps'] == 3){
          Logs::createRecord($datam['username'], 3);
          $date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' + 15 minutes'));
          Ws::$c->q("UPDATE users SET use_locked_at = '$date' WHERE use_id = '$datam[username]' LIMIT 1;");
          return array('response' => 'locked', 'time_at' => $date);
        } else
          return array('response' => 'denied');
      }
      Logs::createRecord($datam['username'], 1);
      $data['use_last_login'] = $data['use_current_login'];
      $data['use_current_login'] = date('Y-m-d H:i:s');
      $data['use_logs']++;
      Ws::$c->q("UPDATE users SET use_attemps = '0', use_last_login = '$data[use_last_login]', use_current_login = '$data[use_current_login]', use_logs = '$data[use_logs]', use_locked_at = NULL WHERE use_id = '$datam[username]' LIMIT 1;");
      $array = array('use_id', 'per_firstname', 'per_lastname', 'per_surname', 'use_profile', 'per_gender');
      $data['per_firstname'] = base64_encode($data['per_firstname']);
      $data['per_lastname'] = base64_encode($data['per_lastname']);
      $data['per_surname'] = base64_encode($data['per_surname']);
      for ($i = 0; $i < count($array); $i++) {
        Ws::$g->cook($array[$i], $data[$array[$i]], 7200);
        $_SESSION[$array[$i]] = $data[$array[$i]];
      }
      $_SESSION['logged'] = 1;
      return array('response' => 'true');
    } else
      return array('response' => 'denied');
  }

  public function logged(){
    if (isset($_SESSION['logged'])) {
      return ($_SESSION['logged'] == 1);
    } else
      return false;
  }
}
