<?php
class Gral{
	public $abrv = array('Ene' => '01', 'Feb' => '02', 'Mar' => '03', 'Abr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Ago' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dic' => '12');
	public $nmx = array('01' => 'Ene', '02' => 'Feb', '03' => 'Mar', '04' => 'Abr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Ago', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dic');
	function __construct(){
		session_start();
		if ($this->activateXssProtection()){
			foreach ($_POST as $clave => $valor)
				$_POST[$clave] = $this->xss($_POST[$clave]);
		}
	}

	public function activateXssProtection(){
		if (isset($_POST['class']) && isset($_POST['method'])){
			if ($_POST['class'] == 'contents' && $_POST['method'] == 'create')
				return false;
			if ($_POST['class'] == 'contents' && $_POST['method'] == 'update')
				return false;
			return true;
		} else
			return true;
	}

	public static function response($status, $id = "", $data = ""){
		return array(
			'response' => $status,
			'id' => $id,
			'data' => $data
		);
	}

	public static function error($status, $data = "", $code = ""){
		return array(
			'response' => $status,
			'response_error' => $status,
			'data' => $data,
			'code' => $code,
		);
	}

	public function stringDateToNumber($str, $separator = ''){
		if ($str == ' ' || $str == '')
				return $str;
		$str = explode(' ', $str);
		$str[1] = str_replace(',','',$str[1]);
		$str[0] = $this->abrv[$str[0]];
		return $str[2].$separator.$str[0].$separator.$str[1];
	}

	/**
   * Compare to dates
   * 
   * @param Date $start date
   * @param Date $end date
   */

	public function comparateDates($start, $end){
		$start = date('Ymd', strtotime($start));
		$end = date('Ymd', strtotime($end));
		if ($end > $start){
			return 1;
		} else if ($end < $start){
			return 2;
		} else{
			return 3;
		}
	}

	public function br2nl($str){
		$breaks = array("<br />","<br>","<br/>");  
    return str_ireplace($breaks, "\r\n", $str);
	}

	public function numberDateToString($str){
		if ($str == ' ' || $str == '')
				return $str;
		$str = str_split($str);
		$year = $str[0].''.$str[1].''.$str[2].''.$str[3];
		$month = $this->nmx[$str[5].''.$str[6]];
		$day = $str[8].''.$str[9];
		return $month.' '.$day.', '.$year;
	}
	
	function cook($name,$value,$future=10800){
		setcookie($name, $value, time()+$future, "/", "", 0);
	}

	function normaliza ($cadena){
		$originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
		$modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
		$cadena = utf8_decode($cadena);
		$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
		$cadena = strtolower($cadena);
		return utf8_encode($cadena);
	}

	function json($v,$data){
		return ($v) ? json_encode($data) : json_decode($data);
	}

	function srfecha($dia, $ret, $fecha = ''){
		$fecha = ($fecha == '') ? date('YmdHis') : $fecha;
		$fecha = strtotime($dia.' day', strtotime($fecha));
		return date($ret, $fecha);
	}

	function jsonO($v,$data){
		return ($v) ? json_encode($data, JSON_FORCE_OBJECT) : json_decode($data);
	}

	function utf8($v,$data){
		if (!is_array($data))
			$data = ($v) ? utf8_encode($data) : utf8_decode($data);
		else
			foreach ($data as $i => $v)
				$data[$i] = ($v) ? utf8_encode($data[$i]) : utf8_decode($data[$i]);
		return $data;
	}

	function kill($msn){
		die($msn);
	}

	function xss($v){
		$_e = array("'", "<",">","&");
		$_r = array("#039;", "&lt;", "&gt;","&amp;");
		if (is_array($v)){
			foreach($v as $key => $value)
				$v[$key] = $this->xss($v[$key]);
		} else {
			$v = str_replace($_e, $_r, $v);
		}
		return $v;
	}

	function jsonx($v){
		$v = $this->xss($v);
		$v = $this->json(false, $v);
		return $this->xss($v);
	}

	function isn($v){
		if (!is_array($v))
			return is_numeric($v);
		else {
			$vs = true;
			for ($i = 0; $i < count($v); $i++)
				if (!is_numeric($v[$i]))
					$vs = false;
			return $vs;
		}
	}

	function sendmail($mail, $title, $descr){
		$data = '
		<style> @font-face { font-family: "Roboto"; font-style: normal; font-weight: 400; src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2) format("woff2"); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;} </style>
		<table class="table" style="color: #FFF;font-family: Roboto;width: 1000px;line-height: 1.5em;" border="0" cellspacing="0" cellpadding="0">
			<tr style="background: #563c3e;">
				<td style="font-size: 28px;text-align: left;padding:5px 100px;">Talabartería la Guadalupana</td>
				<td style="padding:5px 100px;text-align: right;"><img src="https://talabarterialaguadalupana.com/assets/img/logo_b.png" alt="Talabarterís" width="80" /></td>
			</tr>
			<tr style="background: #563c3e;">
				<td colspan="2" style="height: 40px;"></td>
			</tr>
			<tr style="background: #563c3e;">
				<td colspan="2" style="text-align: center;font-size: 40px;padding:50px;">'.$title.'</td>
			</tr>
			<tr style="background: #563c3e;">
				<td colspan="2" style="height: 70px;"></td>
			</tr>
		</table>
		<table class="table" style="font-family: Roboto;width: 1000px;line-height: 1.5em;" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td style="padding: 130px 100px;line-height: 1.5em;font-size: 18px;text-align: justify;">
					'.$descr.'
				</td>
			</tr>
		</table>
		<table class="table" style="font-family: Roboto;width: 1000px;line-height: 1.5em;" border="0" cellspacing="0" cellpadding="0">
			<tr style="background: #F0F0F0">
				<td style="padding: 45px 100px 15px;line-height: 1.5em;font-size: 30px;text-align: center;color:#444;">
					Talabartería la Guadalupana
				</td>
			</tr>
			<tr style="background: #F0F0F0">
				<td style="padding: 0px 100px 30px;line-height: 1.5em;font-size: 18px;text-align: center;">
					Av 1 Nicolás Bravo 37,<br>
					Coscomatepec de Bravo, Centro,<br>
					94140 Coscomatepec de Bravo, Ver.<br><br>
					229 370 3438<br>
					www.talabarteríalaguadalupana.com
				</td>
			</tr>
		</table>
		';

		$headers = "MIME-Version: 1.0\r\n"; 
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
		$headers .= 'From: Talabartería la Guadalupana <noreply@talabarterialaguadalupana.com>\r\n'; 
		$email = @mail($mail, $this->utf8(false,$title), $this->utf8(false,$data), $this->utf8(false,$headers));
		// if ($email)
		// 	Ws::e('Correo enviado');
		// else
		// 	Ws::e('Correo no enviado');
		
		return $email;
	}
}